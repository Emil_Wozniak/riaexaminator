package pl.examinator.service.util.proxy;

import calculator.model.results.CurvePoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.examinator.model.result.StorableCurvePoint;

import java.util.List;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ResultProxyTest extends ResultProxy {

    private List<StorableCurvePoint> curve;
    private String strigifyResult;
    private List<CurvePoint> curvePoints;
    private String streamToString;

    @BeforeEach
    void setUp() {
        curve = List.of(
            new StorableCurvePoint(null, "file", "test", 1, "1", 30, true, "30ng", 0.0, null),
            new StorableCurvePoint(null, "file", "test", 1, "1", 30, true, "30ng", 0.0, null),
            new StorableCurvePoint(null, "file", "test", 1, "1", 30, true, "30ng", 0.0, null),
            new StorableCurvePoint(null, "file", "test", 1, "1", 30, true, "30ng", 0.0, null),
            new StorableCurvePoint(null, "file", "test", 1, "1", 30, true, "30ng", 0.0, null)
        );
        streamToString = curve.stream()
            .map(point ->
                point.getIdentifier() + ";" +
                    point.getPattern() + ";" +
                    point.getProbeNumber() + ";" +
                    point.getPosition() + ";" +
                    point.getCpm() + ";" +
                    point.getFlagged() + ";" +
                    point.getNg())
            .collect(joining("\n", "", ""));
        strigifyResult = "file;test;1;30;1;true;30ng\n" +
            "file;test;1;30;1;true;30ng\n" +
            "file;test;1;30;1;true;30ng\n" +
            "file;test;1;30;1;true;30ng\n" +
            "file;test;1;30;1;true;30ng";
        curvePoints = curve
            .stream()
            .map(point -> new CurvePoint(
                0.0,
                point.getIdentifier(),
                point.getPattern(),
                point.getProbeNumber(),
                point.getPosition(),
                point.getCpm(),
                point.getFlagged(),
                point.getNg()))
            .collect(toList());
    }

    @AfterEach
    void tearDown() {
        curve = null;
        curvePoints = null;
        strigifyResult = null;
    }

    @Test
    void stringify() {
        assertEquals(strigifyResult, streamToString);
    }

    @Test
    void convertToStore() {
        streamToString = stringify(curvePoints);
        assertEquals(strigifyResult, streamToString);
    }
}
