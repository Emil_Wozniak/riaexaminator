/*
 * Copyright (c) 2020.  Emil Woźniak emil.wozniak.591986@gmail.com. This file is part of examinator
 *  examinator can be copied and/or distributed without the express permission of Emil Woźniak
 *
 */

package pl.examinator.service.util;

import calculator.Counter;

import java.util.List;

/**
 * Contains sample values which should be results on tests in this package.
 */
public final class AnalyzeTestConstants {
    public final static Double[] CORTISOL_SOLUTION = new Double[]{1.25, 1.25, 2.5, 2.5, 5.0, 5.0, 10.0, 10.0, 20.0, 20.0, 40.0, 40.0, 80.0, 80.0};
    public final static String[] STANDARD_NAMES = new String[]{"T", "T", "NSB", "NSB", "NSB", "ZERO", "ZERO", "ZERO"};
    public final static List<Boolean> CORRECT_FLAGS = List.of(false, false, true, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
    public final static List<Integer> CPMS = List.of(1976, 1982, 49, 32, 36, 458, 459, 447, 412, 390, 378, 352, 322, 316, 225, 249, 165, 174, 102, 116, 74, 65, 512, 536, 219, 224, 164, 191, 226, 230, 293, 279, 325, 347, 386, 388, 372, 379, 293, 330, 252, 237, 225, 247, 211, 220, 313, 295, 338, 34);
    public final static List<Integer> CURVE_CPMS = List.of(1976, 1982, 49, 32, 36, 458, 459, 447, 412, 390, 378, 352, 322, 316, 225, 249, 165, 174, 102, 116, 74, 65);
    public final static List<Integer> CORRECT_STANDARD = List.of(412, 390, 378, 352, 322, 316, 225, 249, 165, 174, 102, 116, 74);
    public final static List<Double> CORRECT_BINDING_PERCENT = List.of(90.0, 85.0, 82.0, 76.0, 68.0, 67.0, 51.0, 45.0, 32.0, 30.0, 18.0, 15.0, 8.0);
    public final static List<Double> CORRECT_REAL_ZERO = List.of(0.954, 0.753, 0.659, 0.501, 0.327, 0.308, 0.017, -0.087, -0.327, -0.368, -0.659, -0.753, -1.061);
    public final static List<Double> CORRECT_DOSE = List.of(0.1, 0.1, 0.4, 0.4, 0.7, 0.7, 1.0, 1.0, 1.3, 1.3, 1.6, 1.6, 1.9, 1.9);
    public final static List<Double> FAILURE_LIST = List.of(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    public final static Double CORRECT_PARAM_B = -0.8788;
    public final static Double CORRECT_CORRELATION = 0.984276;
    public final static Double CORRECT_PARAM_A = 0.9073714285714286;
    public final static List<Double> CORRECT_METER_READ = List.of(0.86, 1.51, 1.93, 2.97, 4.5, 4.86, 13.77, 10.54, 28.26, 25.15, 80.18, 60.29, 178.96);
    public final static Double ERROR_SAFE_VALUE = 0.0;
    public final static Integer TEST_CURVE_SIZE = CURVE_CPMS.size() - 1;
}
