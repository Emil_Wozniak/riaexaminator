/*
 * Copyright (c) 2020. Free to copy and use.
 */

package pl.examinator;

public class DataSets {

    public static final String FILE = "file";
    public static final String SETTINGS = "settings";
    public static final String FILE_NAME = "test.txt";
    public static final String FILE_PDF = "test.pdf";
    public static final String PDF_DATA = "<<pdf data>>";
    public static final String FILES_ROUTE = "/api/files";

    public static final String TEXT =
        "C:\\mbw\\results\\A16_244.txt\n" +
            "RUN INFORMATION:\n" +
            "================\n" +
            "Counting protocol no: 16                                  Fri 18-Jan-2019  5:22\n" +
            "Name: COPY_OF_H-3_KORTYZOL_5_MIN\n" +
            "CPM normalization protocol no:  2\n" +
            "\n" +
            "\n" +
            "*** DETECTORS NOT NORMALIZED\n" +
            "\n" +
            "\n" +
            "COLUMNS:\n" +
            "========\n" +
            " \tSAMPLE\tPOS\tCCPM1\tCCPM1%\t\n" +
            " \tUnk_1\tA01\t1976\t2.0\t\n" +
            " \tUnk_2\tA02\t1982\t2.0\t\n" +
            " \tUnk_3\tA03\t49\t12.8\t\n" +
            " \tUnk_4\tA04\t32\t15.7\t\n" +
            " \tUnk_5\tA05\t36\t14.9\t\n" +
            " \tUnk_6\tA06\t458\t4.2\t\n" +
            " \tUnk_7\tB01\t459\t4.2\t\n" +
            " \tUnk_8\tB02\t447\t4.2\t\n" +
            " \tUnk_9\tB03\t412\t4.4\t\n" +
            " \tUnk_10\tB04\t390\t4.5\t\n" +
            " \tUnk_11\tB05\t378\t4.6\t\n" +
            " \tUnk_12\tB06\t352\t4.8\t\n" +
            " \tUnk_13\tC01\t322\t5.0\t\n" +
            " \tUnk_14\tC02\t316\t5.0\t\n" +
            " \tUnk_15\tC03\t225\t6.0\t\n" +
            " \tUnk_16\tC04\t249\t5.7\t\n" +
            " \tUnk_17\tC05\t165\t7.0\t\n" +
            " \tUnk_18\tC06\t174\t6.8\t\n" +
            " \tUnk_19\tD01\t102\t8.8\t\n" +
            " \tUnk_20\tD02\t116\t8.3\t\n" +
            " \tUnk_21\tD03\t74\t10.4\t\n" +
            " \tUnk_22\tD04\t65\t11.1\t\n" +
            " \tUnk_23\tD05\t512\t4.0\t\n" +
            " \tUnk_24\tD06\t536\t3.9\t\n" +
            " \tUnk_25\tA01\t219\t6.0\t\n" +
            " \tUnk_26\tA02\t224\t6.0\t\n" +
            " \tUnk_27\tA03\t164\t7.0\t\n" +
            " \tUnk_28\tA04\t191\t6.5\t\n" +
            " \tUnk_29\tA05\t226\t6.0\t\n" +
            " \tUnk_30\tA06\t230\t5.9\t\n" +
            " \tUnk_31\tB01\t293\t5.2\t\n" +
            " \tUnk_32\tB02\t279\t5.4\t\n" +
            " \tUnk_33\tB03\t325\t5.0\t\n" +
            " \tUnk_34\tB04\t347\t4.8\t\n" +
            " \tUnk_35\tB05\t386\t4.6\t\n" +
            " \tUnk_36\tB06\t388\t4.5\t\n" +
            " \tUnk_37\tC01\t372\t4.6\t\n" +
            " \tUnk_38\tC02\t379\t4.6\t\n" +
            " \tUnk_39\tC03\t293\t5.2\t\n" +
            " \tUnk_40\tC04\t330\t4.9\t\n" +
            " \tUnk_41\tC05\t252\t5.6\t\n" +
            " \tUnk_42\tC06\t237\t5.8\t\n" +
            " \tUnk_43\tD01\t225\t6.0\t\n" +
            " \tUnk_44\tD02\t247\t5.7\t\n" +
            " \tUnk_45\tD03\t211\t6.2\t\n" +
            " \tUnk_46\tD04\t220\t6.0\t\n" +
            " \tUnk_47\tD05\t313\t5.1\t\n" +
            " \tUnk_48\tD06\t295\t5.2\t\n" +
            " \tUnk_49\tA01\t338\t4.9\t\n" +
            " \tUnk_50\tA02\t341\t4.8\t";
}
