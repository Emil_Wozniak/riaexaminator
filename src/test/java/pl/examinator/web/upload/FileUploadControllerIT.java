/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

package pl.examinator.web.upload;

import pl.examinator.ExaminatorApp;

import org.springframework.boot.test.context.SpringBootTest;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;

/**
 * Integration tests for the {@link FileUploadController} REST controller.
 */
@SpringBootTest(classes = ExaminatorApp.class)
public class FileUploadControllerIT {

}
