/*
 * Copyright (c) 2020. Emil Woźniak emil.wozniak.591986@gmail.com
 *
 * This file is part of examinator
 *
 * examinator can be copied and/or distributed without the express
 * permission of Emil Woźniak
 */
package pl.examinator.web.errors;

import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.examinator.model.file.FileSettings;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.examinator.DataSets.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
class ExceptionTranslatorTest {

    private static FileSettings standardSettings;
    private static ObjectMapper objectMapper;
    private static MockMultipartFile fileTxtEmpty;
    private static MockMultipartFile fileTxtCorrect;
    private static MockMultipartFile fileWrongFormat;
    private static MockMultipartFile settings;
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    static void beforeAll() throws JsonProcessingException {
        objectMapper = new ObjectMapper();
        standardSettings = new FileSettings("", "", true, "", "");
        settings = new MockMultipartFile(SETTINGS, "settings", APPLICATION_JSON_VALUE,
            objectMapper.writeValueAsString(standardSettings).getBytes(UTF_8));
        fileTxtEmpty = new MockMultipartFile(FILE, FILE_NAME, TEXT_PLAIN_VALUE, "".getBytes(UTF_8));
        fileTxtCorrect = new MockMultipartFile(FILE, FILE_NAME, TEXT_PLAIN_VALUE, TEXT.getBytes(UTF_8));
        fileWrongFormat = new MockMultipartFile(FILE, FILE_PDF, APPLICATION_PDF_VALUE, PDF_DATA.getBytes(UTF_8));
    }

    @AfterAll
    static void afterAll() {
        objectMapper = null;
        settings = null;
        fileTxtEmpty = null;
        fileTxtCorrect = null;
        fileWrongFormat = null;
    }

    @BeforeEach
    void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @Test
    @DisplayName("get a new Spring Session")
    public void getANewSpringSession() throws Exception {
        mockMvc.perform(get(FILES_ROUTE)).andExpect(status().is(202));
    }

    @Test
    @DisplayName("handle method argument not valid")
    void handleMethodArgumentNotValid() throws Exception {
        mockMvc.perform(put(FILES_ROUTE, fileTxtEmpty, standardSettings))
            .andExpect(status().is4xxClientError())
            .andExpect(status().is(405));
    }

    @Test
    @DisplayName("handle HttpMediaType NotSupported and BadRequest")
    void handleHttpMediaTypeNotSupported_AndBadRequest() throws Exception {
        mockMvc
            .perform(multipart(FILES_ROUTE)
                .file(settings)
                .file(fileWrongFormat))
            .andDo(print())
            .andExpect(status()
                .is(409));
    }

    @Test
    @DisplayName("handle HttpMediaType NotSupported and GoodRequest")
    void handleHttpMediaTypeNotSupported_AndGoodRequest() throws Exception {
        mockMvc
            .perform(multipart(FILES_ROUTE)
                .file(settings)
                .file(fileTxtCorrect))
            .andExpect(status()
                .is(202));
    }
}
