package pl.examinator.security.jwt;

import io.github.jhipster.config.JHipsterProperties;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static io.jsonwebtoken.io.Decoders.BASE64;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;
import static pl.examinator.security.AuthoritiesConstants.USER;
import static pl.examinator.security.jwt.JWTFilter.AUTHORIZATION_HEADER;

public class JWTFilterTest {

    private TokenProvider tokenProvider;
    private JWTFilter jwtFilter;
    private String requestURI;
    private UsernamePasswordAuthenticationToken authentication;
    private String jwt;
    private List<SimpleGrantedAuthority> authorities;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockFilterChain filterChain;

    @BeforeEach
    public void setup() {
        requestURI = "/api/test";
        JHipsterProperties jHipsterProperties = new JHipsterProperties();
        tokenProvider = new TokenProvider(jHipsterProperties);
        String testKey = "fd54a45s65fds737b9aafcb3412e07ed99b267f33413274720ddbb7f6c5e64e9f14075f2d7ed041592f0b7657baf8";
        ReflectionTestUtils.setField(tokenProvider, "key", Keys.hmacShaKeyFor(BASE64.decode(testKey)));
        ReflectionTestUtils.setField(tokenProvider, "tokenValidityInMilliseconds", 60000);
        jwtFilter = new JWTFilter(tokenProvider);
        SecurityContextHolder.getContext().setAuthentication(null);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        filterChain = new MockFilterChain();
        authorities = singletonList(new SimpleGrantedAuthority(USER));
    }

    @AfterEach
    void tearDown() {
        authentication = null;
        requestURI = null;
        jwt = null;
        request = null;
        response = null;
        filterChain = null;
    }

    @Test
    @DisplayName("correct JWTFilter")
    public void testJWTFilter() throws Exception {
        authentication = new UsernamePasswordAuthenticationToken("test-user", "test-password", authorities);
        jwt = tokenProvider.createToken(authentication, false);
        request.addHeader(AUTHORIZATION_HEADER, "Bearer " + jwt);
        request.setRequestURI(requestURI);
        jwtFilter.doFilter(request, response, filterChain);
        assertThat(response.getStatus()).isEqualTo(OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication().getName()).isEqualTo("test-user");
        assertThat(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString()).isEqualTo(jwt);
    }

    @Test
    @DisplayName("JWTFilter invalid token")
    public void testJWTFilterInvalidToken() throws Exception {
        jwt = "wrong_jwt";
        request.addHeader(AUTHORIZATION_HEADER, "Bearer " + jwt);
        request.setRequestURI(requestURI);
        jwtFilter.doFilter(request, response, filterChain);
        assertThat(response.getStatus()).isEqualTo(OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    @DisplayName("JWTFilter missing authorization")
    public void testJWTFilterMissingAuthorization() throws Exception {
        request.setRequestURI(requestURI);
        jwtFilter.doFilter(request, response, filterChain);
        assertThat(response.getStatus()).isEqualTo(OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    @DisplayName("JWTFilter missing token")
    public void testJWTFilterMissingToken() throws Exception {
        request.addHeader(AUTHORIZATION_HEADER, "Bearer ");
        request.setRequestURI(requestURI);
        jwtFilter.doFilter(request, response, filterChain);
        assertThat(response.getStatus()).isEqualTo(OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    @DisplayName("JWTFilter wrong scheme")
    public void testJWTFilterWrongScheme() throws Exception {
        authentication = new UsernamePasswordAuthenticationToken("test-user", "test-password", authorities);
        jwt = tokenProvider.createToken(authentication, false);
        request.addHeader(AUTHORIZATION_HEADER, "Basic " + jwt);
        request.setRequestURI(requestURI);
        jwtFilter.doFilter(request, response, filterChain);
        assertThat(response.getStatus()).isEqualTo(OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }
}
