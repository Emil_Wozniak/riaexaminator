/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */
package pl.examinator.repo.results

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.lang.NonNull
import org.springframework.stereotype.Repository
import pl.examinator.model.result.LightResult
import pl.examinator.repo.UpdateRepo

@Repository
interface ResultsRepository : JpaRepository<LightResult, Long>, UpdateRepo<LightResult> {

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update LightResult entity SET entity =?1 where entity.id =?2")
    fun update(entity: LightResult, id: Long?): Int

    @NonNull
    @Query(
        """
            select result 
            from LightResult result 
            """
    )
    override fun findAll(): List<LightResult?>

    @Query(
        """
            select result 
                from LightResult result 
                join fetch result.graphs 
                join fetch result.controlCurve 
                where result.id =:id
        """

    )
    override fun getById(id: Long): LightResult
}