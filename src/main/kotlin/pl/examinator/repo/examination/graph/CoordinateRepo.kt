package pl.examinator.repo.examination.graph

import org.springframework.data.jpa.repository.JpaRepository
import pl.examinator.model.result.StorableCoordinate
import pl.examinator.repo.UpdateRepo
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import pl.examinator.model.result.StorableGraph

@Repository
interface CoordinateRepo : JpaRepository<StorableCoordinate?, Long?>, UpdateRepo<StorableCoordinate> {
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update Coordinates entity SET entity =?1 where entity.id =?2")
    fun update(entity: StorableCoordinate, id: Long?): Int
}