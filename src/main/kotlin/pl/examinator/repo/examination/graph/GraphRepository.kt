package pl.examinator.repo.examination.graph

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import pl.examinator.model.result.StorableGraph
import pl.examinator.repo.UpdateRepo

@Repository
interface GraphRepository : JpaRepository<StorableGraph?, Long?>, UpdateRepo<StorableGraph> {
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update BaseGraph entity SET entity =?1 where entity.id =?2")
    fun update(entity: StorableGraph, id: Long?): Int

    @Query(
        "select graph FROM BaseGraph graph " +
                "left join fetch graph.coordinates " +
                "where graph.id=:id "
    )
    override fun getById(id: Long): StorableGraph
}