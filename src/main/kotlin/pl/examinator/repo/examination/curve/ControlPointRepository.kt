package pl.examinator.repo.examination.curve

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.lang.NonNull
import org.springframework.stereotype.Repository
import pl.examinator.model.result.StorableCurvePoint
import pl.examinator.repo.UpdateRepo

@Repository
interface ControlPointRepository : JpaRepository<StorableCurvePoint?, Long?>, UpdateRepo<StorableCurvePoint> {
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update ControlCurve entity SET entity =?1 where entity.id =?2")
    fun update(entity: StorableCurvePoint, id: Long?): Int

    @NonNull
    @Query(
        """
            select result 
            from ControlCurve result 
        """

    )
    override fun findAll(): List<StorableCurvePoint?>

    @Query(
        """
            select result 
            from ControlCurve result
            where result.id =:id 
    """
    )
    override fun getById(id: Long): StorableCurvePoint
}