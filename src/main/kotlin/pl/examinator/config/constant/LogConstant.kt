/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.config.constant

const val DEV_PROD_LOG = "You have misconfigured your application! It should not run with both the 'dev' and 'prod' profiles at the same time."
const val DEV_CLOUD_LOG = "You have misconfigured your application! It should not run with both the 'dev' and 'cloud' profiles at the same time."
const val DEV = "dev"
const val TEST = "test"
const val PROD = "prod"
const val CLOUD = "cloud"


