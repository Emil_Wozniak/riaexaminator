/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package pl.examinator.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import pl.examinator.model.file.FileSettings
import pl.examinator.model.result.LightResult
import pl.examinator.repo.examination.graph.GraphRepository
import pl.examinator.repo.results.ResultsRepository
import pl.examinator.service.util.api.ApiStoreChecker

/**
 * Service Implementation for managing [ExaminationFile].
 */
@Service
class UploadFileService(
    @Lazy private val repository: ResultsRepository,
    @Lazy private val graphRepository: GraphRepository,
    @Lazy private val resultsProxy: ResultsProxy
) : ApiStoreChecker {
    companion object {
        val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)
    }

    /**
     * Get one examinationFile by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional
    fun getOne(id: Long): LightResult? {
        log.debug("Request to get ExaminationFile : $id")
        val one = getOne(id, repository)
        return fetchWithEagerCoordinates(one)
    }

    @Transactional
    fun save(results: LightResult): LightResult =
        repository.save(results)
            .run {
                val curvePoints = controlCurve.map { it.addResults(this) }.toSet()
                graphs.map { it.addResults(this) }.toSet()
                graphs.forEach {
                    it.coordinates.forEach { coordinate ->
                        coordinate.baseGraph = it
                    }
                }
                LightResult(id, patternTitle, curvePoints, points, graphs, errors, title)
                    .let {
                        repository.save(it)
                        it
                    }
            }

    fun update(entity: LightResult, id: Long): Int =
        update(entity, id, repository)

    /**
     * Get all the examinationFiles.
     *
     * @return the list of entities.
     */
    @Transactional
    fun findAll(): List<LightResult> {
        log.debug("Request to get all ExaminationFiles")
        return findAll(repository)
            .map { fetchWithEagerCoordinates(it) }
            .toList()
    }

    private fun fetchWithEagerCoordinates(result: LightResult): LightResult =
        result.run {
            val graph = graphs.map { graphRepository.getById(it.id!!) }.toSet()
            LightResult(id, patternTitle, controlCurve, points, graph, errors, title)
        }

    /**
     * Delete the examinationFile by id.
     *
     * @param id the id of the entity.
     * @return true if delete was successful or false if failed
     */
    fun delete(id: Long): Boolean = delete(id, repository)

    fun analyze(settings: FileSettings, file: MultipartFile?): LightResult =
        resultsProxy.proxyResults(settings, file).let {
            save(it)
            it
        }
}