package pl.examinator.service

import calculator.FileAnalyzer
import calculator.model.ExaminationResults
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import pl.examinator.model.file.FileSettings
import pl.examinator.model.result.*
import pl.examinator.service.util.proxy.DatabaseProxy
import pl.examinator.service.util.proxy.ResultProxy
import java.nio.charset.StandardCharsets.UTF_8

@Component
class ResultsProxy(
    private val analyzer: FileAnalyzer
) {
    var proxy: DatabaseProxy = ResultProxy()

    fun proxyResults(settings: FileSettings, file: MultipartFile?): LightResult =
        getProxySettings(settings).let {
            analyzer.analyze(file!!, it).run {
                val coordinates = storableCoordinates(this)
                val baseGraph = setOf(storableGraph(this, coordinates))
                val curvePoints = storableCurve(this)
                val errors = storableAnalyseError(this)
                LightResult(
                    null,
                    patternTitle,
                    curvePoints,
                    getPoints(),
                    baseGraph,
                    errors,
                    title
                )
            }
        }

    private fun ExaminationResults.getPoints() = proxy
        .convertToStore(resultPoints)
        .toByteArray(UTF_8)

    private fun getProxySettings(settings: FileSettings): calculator.model.FileSettings =
        settings.run {
            calculator.model.FileSettings(target, defaults, pattern, startValue)
        }

    private fun storableAnalyseError(analyze: ExaminationResults): MutableSet<ExaminationError> =
        analyze
            .errors
            .asSequence()
            .filterNotNull()
            .map { (key, value) -> ExaminationError(key = key, value = value) }
            .toMutableSet()

    private fun storableCurve(analyze: ExaminationResults): Set<StorableCurvePoint> =
        analyze.controlCurve
            .asSequence()
            .filterNotNull()
            .map { (meterRead, identifier, pattern, probeNumber, position, cpm, flagged, ng) ->
                StorableCurvePoint(
                    null,
                    identifier,
                    pattern,
                    probeNumber,
                    position,
                    cpm,
                    flagged!!,
                    ng,
                    meterRead,
                    null
                )
            }
            .toSet()

    private fun storableGraph(analyze: ExaminationResults, coordinates: List<StorableCoordinate>): StorableGraph =
        analyze.run {
            val (_, r, zeroBindingPercent) = graph!!
            StorableGraph(null, coordinates, r, zeroBindingPercent)
        }

    private fun storableCoordinates(analyze: ExaminationResults): List<StorableCoordinate> =
        analyze.graph!!.coordinates
            .map { (x, y) -> StorableCoordinate(x = x, y = y) }
            .toList()
}