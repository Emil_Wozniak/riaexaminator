/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.service.mapper

import calculator.model.ExaminationResults
import calculator.model.results.BaseGraph
import calculator.model.results.Coordinates
import calculator.model.results.CurvePoint
import org.springframework.stereotype.Service
import pl.examinator.model.result.LightResult
import pl.examinator.model.result.StorableCoordinate
import pl.examinator.model.result.StorableCurvePoint
import pl.examinator.model.result.StorableGraph
import pl.examinator.service.util.proxy.ResultProxy
import java.nio.charset.StandardCharsets.UTF_8
import java.util.*
import java.util.stream.Collectors

@Service
open class ResultsMapper : ApiMapper<ExaminationResults?, LightResult?> {
    private val proxy = ResultProxy()
    override fun convertToDto(entity: ExaminationResults?): LightResult =
        entity!!.run {
            val baseGraph = parseGraph(graph)
            val controlCurve = parseToStorableCurve(this)
            LightResult(
                null,
                title = title,
                patternTitle = patternTitle,
                controlCurve = controlCurve,
                points = proxy.convertToStore(entity.resultPoints).toByteArray(UTF_8),
                graphs = setOf(baseGraph),
                errors = mutableSetOf()
            )
        }

    private fun parseToStorableCurve(entity: ExaminationResults): Set<StorableCurvePoint> =
        entity.controlCurve
            .map {
                val (_, identifier, pattern, probeNumber, position, cpm, flagged) = it!!
                StorableCurvePoint(
                    identifier = identifier,
                    pattern = pattern,
                    probeNumber = probeNumber,
                    position = position,
                    cpm = cpm,
                    flagged = flagged!!
                )
            }
            .toSet()

    private fun parseGraph(graph: BaseGraph?): StorableGraph {
        return StorableGraph(
            null,
            parseCoordinates(graph),
            graph!!.r,
            graph.zeroBindingPercent
        )
    }

    private fun parseCoordinates(graph: BaseGraph?): List<StorableCoordinate> =
        graph!!
            .coordinates
            .stream()
            .map { (x, y) -> StorableCoordinate(x, y) }
            .collect(Collectors.toList())

    override fun convertToEntity(entity: LightResult?): ExaminationResults =
        entity!!.run {
            val controlCurve = parseCurve(this)
            val examinationPoints = proxy.parseToPoints(points!!)
            val storableGraphs = if (graphs.isNotEmpty()) ArrayList(graphs)[0]
            else StorableGraph()
            val coordinates = if (graphs.isNotEmpty()) parseCoordinates(storableGraphs)
            else ArrayList()
            val calculatorGraph = if (coordinates.isNotEmpty()) parseCalculatorGraph(storableGraphs, coordinates)
            else BaseGraph()
            ExaminationResults(
                id,
                title,
                patternTitle,
                controlCurve,
                examinationPoints,
                calculatorGraph,
                java.util.List.of()
            )
        }

    private fun parseCalculatorGraph(
        storableGraphs: StorableGraph,
        coordinates: List<Coordinates>
    ): BaseGraph = storableGraphs.run {
        BaseGraph(
            coordinates,
            r!!,
            zeroBindingPercent!!
        )
    }

    private fun parseCoordinates(storableGraphs: StorableGraph): List<Coordinates> =
        storableGraphs
            .coordinates
            .map { (_, _, x, y) -> Coordinates(x!!, y!!) }
            .toList()

    private fun parseCurve(entity: LightResult): List<CurvePoint?> =
        entity.controlCurve
            .map { (_, identifier, pattern, probeNumber, position, cpm, flagged, ng, meterRead) ->
                CurvePoint(
                    isMeterRead(meterRead),
                    identifier,
                    pattern,
                    probeNumber!!,
                    position,
                    cpm!!,
                    flagged,
                    ng
                )
            }
            .toList()

    private fun isMeterRead(point: Double?): Double = point ?: 0.0
}