package pl.examinator.service.util.api

import io.vavr.control.Either
import io.vavr.control.Try
import org.springframework.data.domain.Example
import org.springframework.data.jpa.repository.JpaRepository
import org.zalando.problem.Status.*
import pl.examinator.repo.UpdateRepo
import pl.examinator.service.error.ApiStoreProblem
import java.util.function.Supplier

/**
 * Checks if stored entity fits request expectations
 * for example if repository contains entity with
 * search id.
 *
 *
 * You may also want to create ResponseEntityExceptionHandler
 * with ControllerAdvice to handle thrown exception.
 *
 *
 *
 * This interface works only if request comes from
 * RestController and request is provided by mapped
 * method. To do so you need @RestController and
 * method with @GetMapping.
 *
 *
 * If errors is not thrown check your controller path,
 * add all required annotations.
 *
 * @see RestController
 * @see GetMapping
 * @see PathVariable
 * @see ControllerAdvice
 */
interface ApiStoreChecker {
        /**
         * @param repo JpaRepository for entity type
         * @param <T>  type of entity
         * @param <R>  type of repository
         * @return entities if any exists in
         * repository or empty list
        </R></T> */
        fun <T, R : JpaRepository<T, Long>> findAll(repo: R): List<T> = Try
            .of { repo.findAll() }
            .recover(NullPointerException::class.java) {
                listOf()
            }
            .get()


        /**
         * @param entity entity that will be stored
         * @param repo   JpaRepository for entity type
         * @param <T>    type of entity
         * @param <R>    type of repository
         * @return entity if example of the entity does not exist in
         * repository or throw ApiStoreProblem
         * @see ApiStoreProblem
        </R></T> */
        fun <T, R : JpaRepository<T, Long>> save(entity: T, repo: R): T = Try
            .of { repo.exists(Example.of(entity)) }
            .map<Either<Any?, out Any?>> { isExist: Boolean ->
                if (isExist) Either.left(null)
                else Either.right(repo.save(entity))
            }
            .get()
            .map { it as T }
            .getOrElseThrow<ApiStoreProblem>(Supplier {
                ApiStoreProblem(
                    "save",
                    "cant't create: " + entity.toString() + " already exist",
                    "save",
                    "id not found",
                    BAD_REQUEST
                )
            })

        /**
         * @param id   entity id
         * @param repo JpaRepository for particular entity
         * @param <T>  type of return entity
         * @param <R>  type of repository
         * @return entity if entity with id exists in the repository
         * or ApiStoreProblem
         * @see ApiStoreProblem
        </R></T> */
        fun <T, R> getOne(id: Long, repo: R): T where R : JpaRepository<T, Long>, R : UpdateRepo<T> =
            Try
                .of { repo.existsById(id) }
                .map {
                    if (it) Either.right<Unit, T>(repo.getById(id))
                    else Either.left<Unit, T>(null)
                }
                .get()
                .getOrElseThrow(Supplier {
                    ApiStoreProblem(
                        "getOne",
                        "id: $id not found",
                        "getOne",
                        "id: $id not found",
                        NOT_FOUND
                    )
                }) as T

        fun <T, R> update(entity: T, id: Long, repo: R): Int where R : JpaRepository<T, Long>, R : UpdateRepo<T> =
            Try
                .of { repo.existsById(id) }
                .map { value: Boolean ->
                    if (value) Either.right<Any, Int>(1)
                    else Either.left<Any?, Any>(null)
                }
                .get()
                .getOrElseThrow(Supplier {
                    ApiStoreProblem(
                        "update",
                        "Can't update, id: " + id + "not found",
                        "update",
                        "Can't update, id: " + id + "not found",
                        NOT_FOUND
                    )
                }) as Int

        fun <T, R : JpaRepository<T, Long>> delete(id: Long, repo: R): Boolean {
            val entity = repo.findById(id)
            entity.ifPresent { repo.delete(it) }
            return entity.isPresent
        }

}