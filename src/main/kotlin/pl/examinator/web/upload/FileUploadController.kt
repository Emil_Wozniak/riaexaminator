/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package pl.examinator.web.upload

import calculator.model.ExaminationResults
import io.undertow.server.handlers.form.MultiPartParserDefinition.*
import lombok.RequiredArgsConstructor
import lombok.Value
import lombok.experimental.NonFinal
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.*
import org.springframework.http.MediaType.*
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import pl.examinator.annotations.ReactOrigin
import pl.examinator.annotations.RestApi
import pl.examinator.model.file.FileSettings
import pl.examinator.service.mapper.ResultsMapper
import pl.examinator.service.UploadFileService
import javax.validation.Valid
import kotlin.streams.toList

/**
 * REST controller for managing ExaminationFile.
 */
@Value
@RestApi
@NonFinal
@ReactOrigin
@RequiredArgsConstructor
class FileUploadController(
    private val service: UploadFileService
) {
    val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)

    companion object {
        private const val ROUTE = "/files"
    }

    /**
     * Facade for ExaminationFile.
     *
     * @see ExaminationFile
     */
    var mapper = ResultsMapper()

    /**
     * `POST  /files` : Create a new examinationFile.
     *
     * @param file     the file with data.
     * @param settings values to determine HormonePattern
     * @return the [ResponseEntity] with status `201 (Created)`
     * and with body the new examinationFile,
     * or with status `400 (Bad Request)`
     * handled by ExceptionTranslator.
     * "multipart/form-data"
     * @see pl.examinator.web.errors.ExceptionTranslator
     */
    @ResponseBody
    @PostMapping(
        value = [ROUTE],
        consumes = [MULTIPART_FORM_DATA_VALUE],
        produces = [APPLICATION_JSON_VALUE]
    )
    fun create(
        @RequestPart("settings") settings: @Valid FileSettings?,
        @RequestPart("file") file: @Valid MultipartFile?
    ): ResponseEntity<ExaminationResults> =
        ResponseEntity(mapper.convertToEntity(settings?.let { service.analyze(it, file) }), OK)

    /**
     * @return instance of Results
     */
    @GetMapping(value = [ROUTE], produces = [APPLICATION_JSON_VALUE])
    fun findAll(): List<ExaminationResults> = service
        .findAll()
        .stream()
        .map { mapper.convertToEntity(it) }
        .toList().let {
            log.info(it.toString())
            it
        }

    /**
     * @param id return instance id
     * @return Results instance if exists
     */
    @GetMapping(value = ["$ROUTE/{id}"])
    fun getById(@PathVariable id: Long): ResponseEntity<ExaminationResults?> =
        service.getOne(id).let {
            mapper.convertToEntity(it)
        }.let {
            ok<ExaminationResults>(it)
        }

    @PutMapping(value = ["$ROUTE/{id}"])
    fun update(
        @RequestBody entity: ExaminationResults,
        @PathVariable id: Long
    ): ResponseEntity<Int> = ok(service.update(mapper.convertToDto(entity), id))

    @DeleteMapping(value = ["$ROUTE/{id}"])
    fun delete(@PathVariable id: String): ResponseEntity<String> {
        val delete = service.delete(id.toLong())
        return ResponseEntity(
            if (delete) "Deleted" else "No deleted",
            if (delete) OK else BAD_REQUEST
        )
    }
}