/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.web

//@Configuration
open class UploadRoute(
//    private val handler: UploadHandler
    ) {
    val pattern = "/api/route/person"
//    fun uploadRoutes(): RouterFunction<ServerResponse> {
//        return route().path(pattern) { builder: Builder ->
//            builder
//                .nest(
//                    accept(APPLICATION_JSON),
//                    Consumer { req: Builder ->
//                        req
//                            .GET("/{id}") { request: ServerRequest -> handler.handleOne(request) }
//                            .before { request: ServerRequest ->
//                                ServerRequest.from(request)
//                                    .header("X-RequestHeader", "Value")
//                                    .build()
//                            }
//                    })
//                .POST(pattern) { request: ServerRequest? -> handler.handleSave(request) }
//                .DELETE("$pattern/{id}") { request: ServerRequest -> handler.handleDelete(request) }
//        }
//            .build()
//    }
}