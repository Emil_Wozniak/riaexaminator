/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */
package pl.examinator.web

import lombok.experimental.NonFinal
import lombok.RequiredArgsConstructor
import pl.examinator.service.AuditEventService
import org.springframework.boot.actuate.audit.AuditEvent
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import io.github.jhipster.web.util.ResponseUtil.wrapOrNotFound
import lombok.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.*
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import org.springframework.web.servlet.support.ServletUriComponentsBuilder.*
import java.time.LocalDate
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneId.*

/**
 * REST controller for getting the [AuditEvent]s.
 */
@Value
@NonFinal
@RestController
@RequiredArgsConstructor
@RequestMapping("/management/audits")
class AuditResource {
    var auditEventService: AuditEventService? = null

    /**
     * `GET /audits` : get a page of [AuditEvent]s.
     *
     * @param pageable the pagination information.
     * @return the [ResponseEntity] with status `200 (OK)` and the list of [AuditEvent]s in body.
     */
    @GetMapping
    fun getAll(pageable: Pageable?): ResponseEntity<List<AuditEvent>> {
        val page = auditEventService!!.findAll(pageable)
        val headers =
            PaginationUtil.generatePaginationHttpHeaders(fromCurrentRequest(), page)
        return ResponseEntity(page.content, headers, OK)
    }

    /**
     * `GET  /audits` : get a page of [AuditEvent] between the `fromDate` and `toDate`.
     *
     * @param fromDate the start of the time period of [AuditEvent] to get.
     * @param toDate   the end of the time period of [AuditEvent] to get.
     * @param pageable the pagination information.
     * @return the [ResponseEntity] with status `200 (OK)` and the list of [AuditEvent] in body.
     */
    @GetMapping(params = ["fromDate", "toDate"])
    fun getByDates(
        @RequestParam(value = "fromDate") fromDate: LocalDate,
        @RequestParam(value = "toDate") toDate: LocalDate,
        pageable: Pageable?
    ): ResponseEntity<List<AuditEvent>> {
        val from = fromDate.atStartOfDay(systemDefault()).toInstant()
        val to = toDate.atStartOfDay(systemDefault()).plusDays(1).toInstant()
        val page = auditEventService!!.findByDates(from, to, pageable)
        val headers =
            PaginationUtil.generatePaginationHttpHeaders(fromCurrentRequest(), page)
        return ResponseEntity(page.content, headers, OK)
    }

    /**
     * `GET  /audits/:id` : get an [AuditEvent] by id.
     *
     * @param id the id of the entity to get.
     * @return the [ResponseEntity] with status `200 (OK)` and the [AuditEvent] in body, or status `404 (Not Found)`.
     */
    @GetMapping("/{id:.+}")
    operator fun get(@PathVariable id: Long?): ResponseEntity<AuditEvent> {
        return wrapOrNotFound(auditEventService!!.find(id))
    }
}