/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.model.file

import java.io.Serializable

/**
 * @author emil.wozniak.591986@gmail.com
 * @see FileSettings.target   the characteristic start letters of the column row with data
 *
 * @see FileSettings.defaults if true Reader will use default settings
 *
 * @since 04.02.2020
 */
class FileSettings(
    var title: String = "",
    var target: String = "",
    var defaults: Boolean = false,
    var pattern: String = "",
    var startValue: String
) : Serializable