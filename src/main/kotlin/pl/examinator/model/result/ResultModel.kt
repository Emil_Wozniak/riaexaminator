package pl.examinator.model.result

import java.util.*

abstract class ResultModel<ER : ExaminationResult?> : ArrayList<ER>()