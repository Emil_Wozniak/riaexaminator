/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.model.result

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import lombok.EqualsAndHashCode
import lombok.Setter
import lombok.Singular
import lombok.ToString
import lombok.experimental.NonFinal
import java.io.Serializable
import javax.persistence.*
import javax.persistence.CascadeType.MERGE
import javax.persistence.FetchType.EAGER
import javax.persistence.GenerationType.IDENTITY

@Entity(name = "BaseGraph")
@Table(name = "base_graph")
class StorableGraph(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,

    @Singular(value = "coordinate")
    @JsonIgnoreProperties("baseGraph")
    @OneToMany(cascade = [MERGE], mappedBy = "baseGraph", fetch = EAGER, orphanRemoval = true)
    var coordinates: List<StorableCoordinate> = mutableListOf(),

    @Column(name = "r")
    var r: Double? = null,

    @Column(name = "zero_binding")
    var zeroBindingPercent: Double? = null,
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "results_id")
    @JsonIgnoreProperties(value = ["graphs"])
    var results: LightResult? = null
) : Serializable {

    fun addResults(results: LightResult?): StorableGraph {
        this.results = results
        return this
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as StorableGraph

        if (id != other.id) return false
        if (r != other.r) return false
        if (zeroBindingPercent != other.zeroBindingPercent) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (r?.hashCode() ?: 0)
        result = 31 * result + (zeroBindingPercent?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String =
        "StorableGraph(r=$r, zeroBindingPercent=$zeroBindingPercent)"
}