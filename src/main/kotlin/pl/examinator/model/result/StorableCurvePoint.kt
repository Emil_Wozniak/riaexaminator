/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.model.result

import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY
import kotlin.jvm.Transient

/**
 * @since 18.01.2020
 * @author emil.wozniak.591986@gmail.com
 */
@Entity(name = "ControlCurve")
@Table(name = "curve_point")
data class StorableCurvePoint(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,
    val identifier: String = "",
    val pattern: String = "",
    val probeNumber: Int? = null,
    val position: String = "",
    val cpm: Int? = null,
    val flagged: Boolean = false,
    val ng: String = "",
    @Column(name = "meter_read")
    var meterRead: Double? = null,
    @Transient
    var results: LightResult? = null
) : ExaminationResult(
    identifier,
    pattern,
    probeNumber,
    position,
    cpm,
    flagged,
    ng,
    results
) {
    fun addResults(results: LightResult): StorableCurvePoint {
        this.results = results
        return this
    }
}