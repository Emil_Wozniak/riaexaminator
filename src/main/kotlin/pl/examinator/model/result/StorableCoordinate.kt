/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.model.result

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.io.Serializable
import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY

@Entity(name = "Coordinates")
@Table(name = "coordinates")
data class StorableCoordinate(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "base_graph_id")
    @JsonIgnoreProperties("coordinates")
    var baseGraph: StorableGraph? = null,
    var x: Double? = null,
    var y: Double? = null
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as StorableCoordinate
        if (id != other.id) return false
        if (x != other.x) return false
        if (y != other.y) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    constructor(x: Double?, y: Double?) : this() {
        id = null
        baseGraph = null
        this.x = x
        this.y = y
    }
}