/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.model.result

import lombok.*

/**
 * Container for result generated
 * after upload [ExaminationFile].
 *
 * @see ExaminationFile
 *
 * @see FileUploadController
 */
data class Results(
    private val id: Long? = null,
    private val title: String? = null,
    private val patternTitle: String? = null,
    private val controlCurve: Set<StorableCurvePoint>? = null,
    private val resultPoints: List<StorableExaminationPoint>? = null,
    private val graphs: Set<StorableGraph>? = null,
    private val errors: Set<ExaminationError>? = null
) {
    companion object {
        const val serialVersionUID = 1L
    }
}