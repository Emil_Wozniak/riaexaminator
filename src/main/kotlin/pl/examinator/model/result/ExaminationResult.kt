/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.model.result

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import lombok.*
import java.io.Serializable
import javax.persistence.ManyToOne
import javax.persistence.MappedSuperclass

/**
 *
 */
@MappedSuperclass
abstract class ExaminationResult(
    private val identifier: String? = null,
    private val pattern: String? = null,
    private val probeNumber: Int? = null,
    private val position: String? = null,
    private val cpm: Int? = null,
    private val flagged: Boolean? = null,
    private val ng: String? = null,
    @JsonIgnore
    @ManyToOne
    @JsonIgnoreProperties(value = ["curve_point, results"])
    private val results: LightResult? = null
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun toString(): String {
        return "ExaminationResult(identifier=$identifier, pattern=$pattern, probeNumber=$probeNumber, position=$position, cpm=$cpm, flagged=$flagged, ng=$ng, results=$results)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ExaminationResult

        if (identifier != other.identifier) return false
        if (pattern != other.pattern) return false
        if (probeNumber != other.probeNumber) return false
        if (position != other.position) return false
        if (cpm != other.cpm) return false
        if (flagged != other.flagged) return false
        if (ng != other.ng) return false
        if (results != other.results) return false

        return true
    }

    override fun hashCode(): Int {
        var result = identifier?.hashCode() ?: 0
        result = 31 * result + (pattern?.hashCode() ?: 0)
        result = 31 * result + (probeNumber ?: 0)
        result = 31 * result + (position?.hashCode() ?: 0)
        result = 31 * result + (cpm ?: 0)
        result = 31 * result + (flagged?.hashCode() ?: 0)
        result = 31 * result + (ng?.hashCode() ?: 0)
        result = 31 * result + (results?.hashCode() ?: 0)
        return result
    }


}