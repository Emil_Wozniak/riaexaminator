/*
 *  Copyright 2001-2020 the original author or authors.
 *  
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *  
 *   http://www.apache.org/licenses/LICENSE-2.0
 *  
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator.model.result

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import lombok.Singular
import org.springframework.stereotype.Indexed
import java.io.Serializable
import javax.persistence.*
import javax.persistence.CascadeType.*
import javax.persistence.FetchType.*
import javax.persistence.GenerationType.IDENTITY

/**
 * Database container for results of examination of the file data
 *
 * @author emil.wozniak.591986@gmail.com
 * @since 05.05.2020
 */
@Entity
@Indexed
@Table(name = "results")
data class LightResult(
    /**
     * stored entity should not contains id equals null,
     * because it will be impossible to remove it on client side.
     */
    @GeneratedValue(strategy = IDENTITY)
    @Id var id: Long? = null,

    @Column(name = "pattern_title")
    var patternTitle: String = "",

    @OneToMany(mappedBy = "results", cascade = [ALL], fetch = EAGER, orphanRemoval = true)
    @JsonIgnoreProperties("results")
    var controlCurve: Set<StorableCurvePoint> = mutableSetOf(),

    @Lob
    @Column(name = "points")
    var points: ByteArray? = null,

    @JsonIgnoreProperties("results")
    @OneToMany(mappedBy = "results", cascade = [ALL], fetch = EAGER, orphanRemoval = true)
    var graphs: Set<StorableGraph> = mutableSetOf(),

    @Singular(value = "error")
    @JsonIgnoreProperties("results")
    @ManyToMany(cascade = [MERGE], fetch = LAZY)
    @JoinTable(
        name = "results_examination_errors",
        joinColumns = [JoinColumn(name = "results_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "examination_errors_id", referencedColumnName = "id")]
    )
    var errors: MutableSet<ExaminationError> = mutableSetOf(),

    @Column(name = "title", nullable = false)
    var title: String = ""

) : Serializable {

    override fun toString(): String =
        "LightResult(id=$id, patternTitle='$patternTitle', points=${points?.contentToString()}, title='$title')"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LightResult

        if (patternTitle != other.patternTitle) return false
        if (points != null) {
            if (other.points == null) return false
            if (!points!!.contentEquals(other.points!!)) return false
        } else if (other.points != null) return false
        if (title != other.title) return false

        return true
    }

    override fun hashCode(): Int {
        var result = patternTitle.hashCode()
        result = 31 * result + (points?.contentHashCode() ?: 0)
        result = 31 * result + title.hashCode()
        return result
    }


} 