/**
 * JPA domain objects.
 */
package pl.examinator.model.result

import pl.examinator.model.result.StorableCoordinate
import pl.examinator.model.result.LightResult
import lombok.NoArgsConstructor
import lombok.AllArgsConstructor
import pl.examinator.model.result.ResultModel
import pl.examinator.model.result.StorableExaminationPoint
import pl.examinator.model.result.StorableCurvePoint
import pl.examinator.model.result.StorableGraph
import lombok.RequiredArgsConstructor
import lombok.experimental.NonFinal
import lombok.Singular
