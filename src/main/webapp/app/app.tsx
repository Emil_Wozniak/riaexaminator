import 'react-toastify/dist/ReactToastify.css';
import './app.scss';
import React, {useEffect} from 'react';
import Header from 'app/components/layout/header/header';
import Footer from 'app/components/layout/footer/footer';
import AppRoutes from 'app/routes';
import HeaderImage from "app/components/layout/header/header-image";
import {connect} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import {toast, ToastContainer} from 'react-toastify';
import {hot} from 'react-hot-loader';
import {IRootState} from 'app/components/reducers';
import {getSession} from 'app/components/reducers/authentication';
import {getProfile} from 'app/components/reducers/application-profile';
import {setLocale} from 'app/components/reducers/locale';
import {hasAnyAuthority} from 'app/components/auth/private-route';
import ErrorBoundary from 'app/components/error/error-boundary';
import {AUTHORITIES} from 'app/config/constants';

const baseHref = document
    .querySelector('base')
    .getAttribute('href')
    .replace(/\/$/, '');

export interface IAppProps extends StateProps, DispatchProps {
}

export const App = (props: IAppProps) => {
    useEffect(() => {
        props.getSession();
        props.getProfile();
    }, []);

    return (
        <Router basename={baseHref}>
            <div className="app-container">
                <ToastContainer
                    position={toast.POSITION.TOP_LEFT}
                    className="toastify-container"
                    toastClassName="toastify-toast"/>
                <ErrorBoundary>
                    <Header
                        isAuthenticated={props.isAuthenticated}
                        isAdmin={props.isAdmin}
                        currentLocale={props.currentLocale}
                        onLocaleChange={props.setLocale}
                        ribbonEnv={props.ribbonEnv}
                        isInProduction={props.isInProduction}
                        isSwaggerEnabled={props.isSwaggerEnabled}
                        image={<HeaderImage/>}
                    />
                </ErrorBoundary>
                <div className="w-full mt-5" id="app-view-container">
                    <ErrorBoundary>
                        <AppRoutes/>
                    </ErrorBoundary>
                    <Footer title="RIA file analyzer"/>
                </div>
            </div>
        </Router>
    );
};

const mapStateToProps = ({authentication, applicationProfile, locale}: IRootState) => ({
    currentLocale: locale.currentLocale,
    isAuthenticated: authentication.isAuthenticated,
    isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN]),
    ribbonEnv: applicationProfile.ribbonEnv,
    isInProduction: applicationProfile.inProduction,
    isSwaggerEnabled: applicationProfile.isSwaggerEnabled
});

const mapDispatchToProps = {setLocale, getSession, getProfile};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(hot(module)(App));
