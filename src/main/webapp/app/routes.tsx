import React from 'react';
import {Switch} from 'react-router-dom';
import Loadable from 'react-loadable';

import Login from 'app/modules/login/login';
import Register from 'app/modules/account/register/register';
import Activate from 'app/modules/account/activate/activate';
import PasswordResetInit from 'app/modules/account/password-reset/init/password-reset-init';
import PasswordResetFinish from 'app/modules/account/password-reset/finish/password-reset-finish';
import Logout from 'app/modules/login/logout';
import Home from 'app/modules/home/home';
import Entities from 'app/entities';
import PrivateRoute from 'app/components/auth/private-route';
import ErrorBoundaryRoute from 'app/components/error/error-boundary-route';
import PageNotFound from 'app/components/error/page-not-found';
import Contact from "app/modules/contact/contact";
import About from "app/modules/about/about";
import {AUTHORITIES} from 'app/config/constants';
import Working from "app/modules/working/working";

const Account = Loadable({
    loader: () => import(/* webpackChunkName: "account" */ 'app/modules/account'),
    loading: () => <div>loading ...</div>
});

const Admin = Loadable({
    loader: () => import(/* webpackChunkName: "administration" */ 'app/modules/administration'),
    loading: () => <div>loading ...</div>
});

const Routes = () => {
    const authorities = [AUTHORITIES.ADMIN, AUTHORITIES.USER];
    return (
        <div className="view-routes">
            <Switch>
                <ErrorBoundaryRoute path="/login" component={Login}/>
                <ErrorBoundaryRoute path="/logout" component={Logout}/>
                <ErrorBoundaryRoute path="/account/register" component={Register}/>
                <ErrorBoundaryRoute path="/account/activate/:key?" component={Activate}/>
                <ErrorBoundaryRoute path="/account/reset/request" component={PasswordResetInit}/>
                <ErrorBoundaryRoute path="/account/reset/finish/:key?" component={PasswordResetFinish}/>
                <PrivateRoute path="/admin" component={Admin} hasAnyAuthorities={[AUTHORITIES.ADMIN]}/>
                <PrivateRoute path="/account" component={Account} hasAnyAuthorities={authorities}/>
                <ErrorBoundaryRoute path="/" exact component={Home}/>
                <ErrorBoundaryRoute path="/contact" exact component={Contact}/>
                <ErrorBoundaryRoute path="/about" exact component={About}/>
                <ErrorBoundaryRoute path="/working" exact component={Working}/>
                <ErrorBoundaryRoute path="/" component={Entities}/>
                <ErrorBoundaryRoute component={PageNotFound}/>
            </Switch>
        </div>
    );
};

export default Routes;
