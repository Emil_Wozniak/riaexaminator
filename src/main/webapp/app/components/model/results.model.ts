interface ExaminationResult {
  id?: number;
  identifier: string;
  pattern: string;
  probeNumber?: number;
  position: number;
  cpm: number;
  flagged: boolean;
  ng?: string;
}

export interface ExaminationPoint extends ExaminationResult {
  average?: number;
}

export interface CurvePoint extends ExaminationResult {
  meterRead?: string;
}

export interface ICoordinates {
  x: number;
  y: number;
}

export interface BaseGraph {
  coordinates?: Array<ICoordinates>;
  r: number;
  zeroBindingPercent: number;
}

export interface IResults {
  id?: number;
  title?: string;
  controlCurve: Array<CurvePoint>;
  resultPoints: Array<ExaminationPoint>;
  graph: BaseGraph;
  errors?: any;
}

export const defaultResults: IResults = {
  id: null,
  title: '',
  controlCurve: [],
  resultPoints: [],
  graph: { coordinates: [], r: 0.0, zeroBindingPercent: 0.0 },
  errors: null
};
