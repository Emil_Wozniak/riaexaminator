export interface IFileProps {
  title: string;
  target: string;
  defaults: boolean;
  pattern: string;
  startValue: string;
  patternProps?: IPatternProps;
}

export interface IPatternProps {
  totals?: number;
  zeros?: number;
  nsbs?: number;
  length?: number;
  repeats?: number;
  controlPoints?: number;
}

export const defaultSettings: Readonly<IFileProps> = {
  title: '',
  target: '',
  defaults: true,
  pattern: '',
  startValue: '',
  patternProps: {
    totals: 2,
    nsbs: 3,
    zeros: 3,
    length: 7,
    repeats: 2,
    controlPoints: 0
  }
};

function createPattern(patternProps: IPatternProps) {
  return `${patternProps.totals};${patternProps.zeros};${patternProps.nsbs};${patternProps.length};${patternProps.repeats};0`;
}

export const exportSettings = ({ title, target, defaults, startValue, patternProps }: IFileProps): IFileProps => {
  return {
    title,
    target,
    defaults,
    pattern: createPattern(patternProps),
    startValue,
    patternProps
  };
};
