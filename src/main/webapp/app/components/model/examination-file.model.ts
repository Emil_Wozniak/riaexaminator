export interface IExaminationFile {
  id?: number;
  fileContentType?: string;
  file?: any;
}

export const defaultValue: Readonly<IExaminationFile> = {};
