/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

import React from 'react';
import {Translate} from 'react-jhipster';
import {Link} from 'react-router-dom';

const NotLoginUser = () => <div>
    <Translate contentKey="global.messages.info.register.noaccount">
        You do not have an account yet?
    </Translate>&nbsp;
    <Link to="/account/register" className="alert-link">
        <Translate contentKey="global.messages.info.register.link">
            Register a new account
        </Translate>
    </Link>
</div>;

export default NotLoginUser;
