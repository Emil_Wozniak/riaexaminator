export type DocumentTemplate = 'xls' | 'xlsx' | 'csv';

const toXls = (data: string) =>
  '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-mic' +
  'rosoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta cha' +
  'rset="UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:Exce' +
  'lWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>' +
  '</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></' +
  `xml><![endif]--></head><body>${data}</body></html>`;

const handleSheet = sheet => String(sheet);

export const handleTemplate = (template: DocumentTemplate, tableDocument) => {
  switch (template) {
    case 'xls':
      return toXls(tableDocument);
    default:
      return null;
  }
};

export const handleExtension = (template: DocumentTemplate, filename: string) => {
  switch (template) {
    case 'xls':
      return `${String(filename)}.xls`;
    default:
      return null;
  }
};

export const uri = 'data:application/vnd.ms-excel;base64,';
export const getContext = (sheet: string, table: string) => ({
  worksheet: handleSheet(sheet) || 'Worksheet',
  table
});
