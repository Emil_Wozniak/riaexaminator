import {base64, format} from "app/components/util/decoder/decoder";
import {
    DocumentTemplate,
    getContext,
    handleExtension,
    handleTemplate,
    uri
} from "app/components/util/file-downloader/file-type";

interface IFileDownload {
    table: string;
    sheet: string;
    filename: string;
    template: DocumentTemplate;
}

export const handleDownload = (
    {
        table,
        sheet,
        filename,
        template
    }: IFileDownload) => {
    if (!document) {
        if (process.env.NODE_ENV !== 'production') {
            console.error('Failed to access document object');
        }
        return null;
    }

    if (document.getElementById(table).nodeType !== 1
        || document.getElementById(table).nodeName !== 'TABLE') {
        if (process.env.NODE_ENV !== 'production') {
            console.error('Provided table property is not html table element');
        }
        return null;
    }
    const tableDocument = document.getElementById(table).outerHTML;
    const convertedTemplate = handleTemplate(template, tableDocument);
    const context = getContext(sheet, tableDocument);
    const element = window.document.createElement('a');

    element.href = uri + base64(format(convertedTemplate, context));
    element.download = handleExtension(template, filename);
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);

    return true;
};