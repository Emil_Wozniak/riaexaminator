/*
 * Copyright (c) 2020. Emil Woźniak emil.wozniak.591986@gmail.com
 *
 * This file is part of examinator
 *
 * examinator can be copied and/or distributed without the express
 * permission of Emil Woźniak
 */

interface IntStream {
    range(start: number, end: number, numbers: Array<number>): Array<number>;
}

export const IntStream: IntStream = {
    range(start: number, end: number, numbers: Array<number>): Array<number> {
        return (start === end) ? numbers : IntStream.range(start + 1, end, numbers.concat(start));
    }
};

export const isOdd = (val: number): boolean => val % 2 === 1;

export const isEven = (val: number): boolean => val % 2 === 0;

export const halfOf = (val: number): number => val / 2;