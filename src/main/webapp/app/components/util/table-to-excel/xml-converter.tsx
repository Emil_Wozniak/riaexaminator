import React from 'react';
import {handleDownload} from "app/components/util/file-downloader/file-downloader";
import {DocumentTemplate} from "app/components/util/file-downloader/file-type";

interface ITableConverter {
    id?: string,
    table: string;
    filename: string,
    sheet: string;
    className?: string;
    style?: any;
    buttonText?: string;
    template: DocumentTemplate;
    children?: any
}

export const XmlConverter = (
    {
        id,
        filename,
        table,
        sheet,
        className,
        style,
        template,
        children
    }: ITableConverter) => (
    <button
        id={id ? id : 'button-download-as-xls'}
        className={className ? className : 'button-download'}
        style={style}
        type="button"
        onClick={() => handleDownload({table, sheet, filename, template})}>
        {children}
    </button>
);


export default XmlConverter;
