export const base64 = word => {
  return window.btoa(unescape(encodeURIComponent(word)));
};

export const format = (word, c) => {
  return word.replace(/{(\w+)}/g, (m, p) => c[p]);
};
