/* GRID */
export const ROW = "flex flex-row";
export const COLUMN = "flex flex-col";
export const COLUMN_CENTERED = "bg-orange-500 min-h-screen flex flex-col items-center justify-center";
export const CARD = "rounded overflow-hidden shadow-lg";
export const CELL = "w-1/5 p-8";
export const CARD_BODY = "px-6 px-4";



