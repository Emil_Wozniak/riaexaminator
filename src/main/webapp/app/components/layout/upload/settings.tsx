import React from 'react';
import {FormGroup} from 'reactstrap';
import {IFileProps, IPatternProps} from "app/components/model/file.props.model";
import PatternPlotter from "app/entities/plotter/pattern-plotter";
import {BaseTextField, NumericTextField, PatternField} from "app/components/layout/upload/settings.components";
import { translate } from 'react-jhipster';

interface ISettings {
    defaults: boolean;
    patternProps: IPatternProps;
    settings: IFileProps;
    onPatternChange: any;
}

export const Settings = (
    {
        defaults,
        settings,
        patternProps,
        onPatternChange
    }: ISettings) => (
    <>
        {!defaults
            ? <div className="card Source">
                <FormGroup className="form-row container">
                    <BaseTextField
                        name="title"
                        label={translate("examinatorApp.fileProps.name")}
                        value={settings.title}
                        onPatternChange={onPatternChange}
                        msg="required"/>
                    <BaseTextField
                        name="target"
                        label={translate("examinatorApp.fileProps.target")}
                        value={settings.target}
                        onPatternChange={onPatternChange}
                        msg="a word or regex"/>
                    <NumericTextField
                        name="start_value"
                        value={settings.startValue}
                        label={translate("examinatorApp.fileProps.start")}
                        msg="a start value"
                        onPatternChange={onPatternChange}/>
                </FormGroup>
                <FormGroup className="form-row container">
                    <PatternField
                        name="totals"
                        label="Total"
                        value={patternProps.totals}
                        onPatternChange={onPatternChange}/>
                    <PatternField
                        name="nsbs"
                        label="NSB"
                        value={patternProps.nsbs}
                        onPatternChange={onPatternChange}/>
                    <PatternField
                        name="zeros"
                        label="Zero"
                        value={patternProps.zeros}
                        onPatternChange={onPatternChange}/>
                    <PatternField
                        name="repeats"
                        label={translate("examinatorApp.fileProps.repeats")}
                        value={patternProps.repeats}
                        onPatternChange={onPatternChange}/>
                    <PatternField
                        name="length"
                        label={translate("examinatorApp.fileProps.length")}
                        value={patternProps.length}
                        onPatternChange={onPatternChange}/>
                </FormGroup>
                <div className="container">
                    <PatternPlotter values={patternProps}/>
                </div>
            </div>
            : <div/>
        }
    </>);
