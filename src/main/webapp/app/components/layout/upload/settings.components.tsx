import React from 'react';
import {AvField} from 'availity-reactstrap-validation';

interface IPatternField {
    name: string;
    label: string;
    value?: any;
    msg?: string;
    onPatternChange?: any;
}

export const PatternField = ({name, label, value, onPatternChange}: IPatternField) => (
    <AvField
        name={name}
        label={label}
        type="number"
        onChange={onPatternChange}
        min={2}
        max={10}
        value={value ? value : 0}
        validate={{required: {value: true, errorMessage: `${name} are required`}}}
        className="form-control form-control-sm  "/>
);

export const BaseTextField = ({name, label, msg, value, onPatternChange}: IPatternField) => (
    <AvField
        style={{width: "16rem"}}
        name={name}
        value={value}
        label={label}
        type="text"
        onChange={onPatternChange}
        validate={{required: {value: true, errorMessage: `Please enter ${msg}`}}}
        className="form-control form-control-md"/>
);

export const NumericTextField = ({name, label, msg, onPatternChange}: IPatternField) => (
    <AvField
        style={{width: "4rem"}}
        name={name}
        label={label}
        type="text"
        onChange={onPatternChange}
        validate={{
            required: {value: true, errorMessage: `Please enter ${msg}`},
            pattern: {value: '[0-9]+\\.[0-9]+$'}
        }}
        className="form-control form-control-md"/>
);