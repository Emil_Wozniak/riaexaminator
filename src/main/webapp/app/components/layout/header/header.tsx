import React from 'react';
import "app/components/layout/header/header.scss";
import "../grid/grid.styles.css";
import {Storage} from 'react-jhipster';
import {AccountMenu, AdminMenu, LocaleMenu} from '../menus';
import LoadingBar from 'react-redux-loading-bar';
import TranslateItem from "app/components/layout/header/TranslateItem";
import {Link} from 'react-router-dom';
import RenderDevRibbon from './render-dev-ribbon';

export interface IHeaderProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
  ribbonEnv: string;
  isInProduction: boolean;
  isSwaggerEnabled: boolean;
  currentLocale: string;
  image?: any;
  onLocaleChange: Function;
}


const Header = (props: IHeaderProps) => {
  const handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    props.onLocaleChange(langKey);
  };
  return (
    <header className="relative" style={{height: "24rem"}}>
      <RenderDevRibbon ribbonEnv={props.ribbonEnv} isInProduction={props.isInProduction}/>
      <LoadingBar className="loading-bar"/>
      {props.image}
      <nav className="flex justify-between p-8 items-center mb-16">
        <Link to="/" className="home-button md:text-4xl">
          RIA file analyzer
        </Link>
        <ul className="flex" style={{width: '60%'}}>
          <TranslateItem href="/" contentKey="global.menu.home"/>
          <TranslateItem href="/about" contentKey="global.menu.about"/>
          <TranslateItem href="/contact" contentKey="global.menu.contact"/>
          <TranslateItem href="/working" contentKey="global.menu.working"/>
          <AccountMenu isAuthenticated={props.isAuthenticated}/>
          <LocaleMenu onClick={handleLocaleChange}/>
          {props.isAuthenticated
          && props.isAdmin
          && <AdminMenu showSwagger={props.isSwaggerEnabled}/>}
        </ul>
      </nav>
    </header>
  )
};


export default Header;

