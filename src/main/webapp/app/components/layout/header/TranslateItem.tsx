import React from 'react';
import {Translate} from 'react-jhipster';
import {Link} from 'react-router-dom';

interface ITranslateItem {
    href: string;
    contentKey: string;
}

const TranslateItem = ({href, contentKey}: ITranslateItem) => (
    <li>
        <button>
            <Link to={href} className="text-gray-700 px-2 md:px-4 align-middle text-center">
                <Translate contentKey={contentKey}/>
            </Link>
        </button>
    </li>
);


export default TranslateItem;