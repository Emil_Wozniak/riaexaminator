/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

import React from 'react';
import {HoverableSegment} from "app/components/layout/header/hoverable";
import {of} from "app/components/layout/grid/model/util";

interface DropdownMenuType {
    amount?: number
}
const DropdownMenu = ({amount}: DropdownMenuType) => {

    const one =
        <HoverableSegment
            image={"M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"}
            heading={"Heading 1"}
            paragraph={"Quarterly sales are at an all-time low create spaces to explore the accountable talk and blind vampires."}/>;
    const two =
        <HoverableSegment
            background={of("blue", "900")}
            image={"M4.13 12H4a2 2 0 1 0 1.8 1.11L7.86 10a2.03 2.03 0 0 0 .65-.07l1.55 1.55a2 2 0 1 0 3.72-.37L15.87 8H16a2 2 0 1 0-1.8-1.11L12.14 10a2.03 2.03 0 0 0-.65.07L9.93 8.52a2 2 0 1 0-3.72.37L4.13 12zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4z"}
            heading={"Heading 2"}
            paragraph={"Prioritize these line items game-plan draw a line in the sand come up with something buzzworthy UX upstream selling."}/>;
    const three = <HoverableSegment
        image={"M2 4v14h14v-6l2-2v10H0V2h10L8 4H2zm10.3-.3l4 4L8 16H4v-4l8.3-8.3zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"}
        heading={"Heading 3"}
        paragraph={"This proposal is a win-win situation which will cause a stellar paradigm shift, let's touch base off-line before we fire the new ux experience."}/>;
    const four = <HoverableSegment
        image={"M9 12H1v6a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-6h-8v2H9v-2zm0-1H0V5c0-1.1.9-2 2-2h4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v1h4a2 2 0 0 1 2 2v6h-9V9H9v2zm3-8V2H8v1h4z"}
        heading={"Heading 4"}
        paragraph={"This is a no-brainer to wash your face, or we need to future-proof this high performance keywords granularity."}/>;
    switch (amount) {
        case 1:
            return <>{one}</>;
        case 2:
            return <>{one}{two}</>;
        case 3:
            return <>{one}{two}{three}</>;
        default:
            return <>{one}{two}{three}{four}</>;
    }
};

export default DropdownMenu;