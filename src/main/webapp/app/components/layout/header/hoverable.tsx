/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */
import React from 'react';
import "app/components/layout/header/hoverable.scss";
import {GridModel} from "app/components/layout/grid/model/grid.model";

interface IHoverableTitle {
    title?: string;
    subtitle?: string;
}

const HoverableTitle = ({title, subtitle}: IHoverableTitle) => (
    title
        ? <div className="w-full text-white mb-8">
            <h2 className="m-1 font-bold text-2xl">{title}</h2>
            <p className="m-2">{subtitle}</p>
        </div>
        : <div/>);

interface HoverableSegment extends GridModel {
    image: string
    heading: string;
    paragraph: string;
}

const SVG_ARROW = "M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z";

const SEGMENT_CLASS = "pb-6 pt-6 px-4 w-full sm:w-1/2 lg:w-1/4 border-r border-b";

export const HoverableSegment = ({image, heading, paragraph, background}: HoverableSegment) =>
    <ul
        className={`${background} ${SEGMENT_CLASS}`}>
        <div className={`flex items-center`}>
            <svg
                className="h-8 mb-3 mr-3 fill-current text-white" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20">
                <path d={image}/>
            </svg>
            <h3 className="font-bold text-xl text-white text-bold mb-2">
                {heading}
            </h3>
        </div>
        <p className="text-gray-100 text-sm">
            {paragraph}
        </p>
        <div className="flex items-center py-3">
            <svg
                className="h-6 pr-3 fill-current text-blue-300"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20">
                <path
                    d={SVG_ARROW}/>
            </svg>
            <a
                href="#"
                className="text-white bold border-b-2 border-blue-300 hover:text-blue-300">
                Find out more...
            </a>
        </div>
    </ul>;


interface IHoverable extends GridModel {
    btnIcon?: any;
    iconColor?: string;
    btnTitle?: string
    title?: string;
    subtitle?: string;
    style?: object;
    children?: any;
}

const DropdownContent = (
    {
        background,
        style,
        title,
        subtitle,
        children
    }: IHoverable) => (
    <div className={`dropdown-content bg-${background?.color}-${background?.strength}`}>
        <div className="mx-auto w-full flex flex-wrap justify-between text-black">
            <HoverableTitle title={title} subtitle={subtitle}/>
            {children}
        </div>
    </div>);

export const Hoverable = (props: IHoverable) => (
    <li
        className={`hoverable hover:bg-${props.background?.color}-${props.background?.strength} hover:text-white`}
        style={props.style}>
        <div className="dropdown">
            <button className="drop-btn">
                {props.btnIcon ? props.btnIcon : null}
                <h4>{props.btnTitle ? props.btnTitle : ""}</h4>
            </button>
            <DropdownContent {...props}/>
        </div>
    </li>
);
