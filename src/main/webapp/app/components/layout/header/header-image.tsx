/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

import React from 'react';
import "./header-image.scss"

const HeaderImage = () => {
    return (
        <div className="w-full">
            <div className="shadow header-image w-screen bg-blue-100 absolute object-cover h-full w-full"
                 style={{zIndex: -1}}/>
        </div>
    )
};

export default HeaderImage;