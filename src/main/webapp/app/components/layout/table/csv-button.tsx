import React from 'react';
import {IResultTable} from "app/entities/result-table/result-table";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {CSVLink} from "react-csv";

export const CsvButton = ({results}: IResultTable) => (
    <CSVLink
        separator=";"
        data={results.resultPoints.map((
            {
                id,
                identifier,
                pattern,
                probeNumber,
                position,
                cpm,
                flagged,
                ng,
                average
            }) =>
            (
                {
                    id: id + 1,
                    identifier,
                    pattern,
                    probeNumber,
                    position,
                    cpm: `${cpm}`,
                    flagged: flagged ? "Y" : "N",
                    ng: `${ng}`,
                    average: `${average}`
                }))}
        style={{top: "35rem"}}
        className="">
        <FontAwesomeIcon
            className=""
            icon="file-csv"
            size="4x"
            style={{color: "yellowgreen"}}/>
    </CSVLink>);