import React from 'react';
import "./table.styles.scss";
import {defaultResults} from "app/components/model/results.model";
import {IResultTable} from "app/entities/result-table/result-table";
import {sortByProbNumber} from "app/components/util/file-data-sorter";

export const CurveThread = ({results}: IResultTable) => (
    <>
        <thead>
        <tr style={{fontWeight: "bold", fontSize: 18}}>
            <th>nr</th>
            <th>pos</th>
            <th>rm</th>
            <th>cpm</th>
            <th>flag</th>
            <th>id</th>
        </tr>
        <tr/>
        </thead>
        {results !== defaultResults
            ? results.controlCurve
                .concat()
                .sort(sortByProbNumber())
                .map((point, i) => (
                    <tbody key={`controlCurve-${i}-`}>
                    <tr>
                        <td className="point-id"> {point.probeNumber + 1}</td>
                        <td> {point.position}</td>
                        <td> {point.meterRead ? point.meterRead : ""}</td>
                        <td> {point.cpm}</td>
                        <td className="center" style={{color: point.flagged ? "red" : "green"}}>
                            {point.flagged ? "Y" : "N"}
                        </td>
                        <td> {point.identifier}</td>
                    </tr>
                    </tbody>
                )) :
            <div/>
        }
    </>);
