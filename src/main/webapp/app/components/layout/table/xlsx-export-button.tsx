/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

import React from 'react';
import XLSXExporter from "app/components/util/xlsx-exporter";
import {getFilename} from "app/components/util/file-export-utils";
import {FileExcelOutlined} from '@ant-design/icons';
import {IResults} from "app/components/model/results.model";
import {Tag} from 'antd';

type XlsxExportButtonProps = {

    results: IResults;
}

interface ExportResult {
    id?: number;
    identifier: string;
    probeNumber?: string;
    cpm: number;
    flagged: string;
    ng?: string;
    average?: number;
    filter: 0 | 1
}

const convertToExport = (results: IResults) => results.resultPoints.map((
    {
        id,
        identifier,
        probeNumber,
        cpm,
        flagged,
        ng,
        average
    }): ExportResult => {
    const probe = probeNumber + 1
    return ({
        id,
        identifier,
        probeNumber: `${probe}`,
        cpm,
        flagged: flagged ? "TAK" : "NIE",
        ng,
        average,
        filter: probeNumber % 2 === 0 ? 1 : 0
    });
});

const XLSXExportButton = ({results}: XlsxExportButtonProps) => (
    <XLSXExporter className="" filename={getFilename(results)} data={convertToExport(results)}>
        <FileExcelOutlined style={{fontSize: '3.5rem', color: '#006400'}}/>
        <br/>
        <Tag color="geekblue" style={{margin: 4}}>XLSX</Tag>
    </XLSXExporter>
);

export default XLSXExportButton;