import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

type IFlagged = {
    flagged: boolean;
}
export const PointFlag = ({flagged}: IFlagged) => (
    <>
        {flagged ?
            <FontAwesomeIcon icon="circle" style={{color: "red"}}/> :
            <FontAwesomeIcon icon="circle" style={{color: "green"}}/>
        }
    </>);