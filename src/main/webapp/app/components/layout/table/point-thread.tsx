import React from 'react';
import "./table.styles.scss";
import {defaultResults} from "app/components/model/results.model";
import {IResultTable} from "app/entities/result-table/result-table";
import {sortByProbNumber} from "app/components/util/file-data-sorter";

export const PointsThread = ({results}: IResultTable) => (
    <>
        <thead>
        <tr style={{fontWeight: "bold", fontSize: 18}}>
            <th>pos</th>
            <th>ng</th>
            <th>avg</th>
            <th>cpm</th>
            <th>flag</th>
            <th>id</th>
            <td>filter</td>
        </tr>
        </thead>
        {results !== defaultResults
            ? results
                .resultPoints
                .concat()
                .sort(sortByProbNumber())
                .map((point, i) => (
                    <tbody key={`resultPoints-${i}-`}>
                    <tr className={i % 2 === 0 ? "point-row" : ""}>
                        <td className="point-id"> {point.probeNumber + 1}</td>
                        <td> {`${point.ng}`}</td>
                        <td> {point.average ? `${point.average}` : ""}</td>
                        <td> {point.cpm}</td>
                        <td className="center" style={{color: point.flagged ? "red" : "green"}}>
                            {point.flagged ? "Y" : "N"}
                        </td>
                        <td> {point.identifier}</td>
                        <td> {i % 2 === 0 ? 1 : 0}</td>
                    </tr>
                    </tbody>
                ))
            : null
        }
    </>);
