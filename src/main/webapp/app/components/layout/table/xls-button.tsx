import React from 'react';
import XmlConverter from "app/components/util/table-to-excel/xml-converter";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {CurveThread} from "app/components/layout/table/curve-thread";
import {PointsThread} from "app/components/layout/table/point-thread";
import {Table} from 'reactstrap';
import {IResultTable} from "app/entities/result-table/result-table";
import {getFilename} from "app/components/util/file-export-utils";
import {Tag} from 'antd';

export const XlsButton = ({results}: IResultTable) => (
    <XmlConverter
        id="test-table-xls-button"
        className=""
        style={{top: "30rem"}}
        table="table-to-xls"
        filename={getFilename(results)}
        sheet="tablexls"
        template="xls"
        buttonText="">
        <FontAwesomeIcon
            className="mt-1"
            icon="file-excel"
            size="4x"
            style={{color: "green"}}/>
        <br/>
        <Tag color="geekblue" style={{margin: 4}}>XLS</Tag>
        <div style={{display: "none"}}>
            <Table id="table-to-xls">
                <CurveThread results={results}/>
                <PointsThread results={results}/>
            </Table>
        </div>
    </XmlConverter>
);