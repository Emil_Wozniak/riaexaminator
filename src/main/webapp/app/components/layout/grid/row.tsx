import React from "react";
import {ROW} from "app/components/styles/style-constants";

export const Row = (props: any) =>
    <div className={ROW}>
        {props.children}
    </div>;
