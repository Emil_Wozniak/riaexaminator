import React from 'react';
import 'app/components/layout/grid/grid.styles.css';

interface IFadingElementProps {
    open?: boolean;
    children?: any;
    speed?: number;
}

const FadingElement = (props: IFadingElementProps) => {
    const velocity = props.speed ? `fadeIn ${props.speed}s` : `fadeIn 1s`;
    return (
        <div style={{animation: `${velocity}`}}>
            {props.children}
        </div>
    )
};

export default FadingElement;