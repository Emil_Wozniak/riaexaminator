import React from 'react';
import Item from 'antd/lib/list/Item';
import {Form, Input, InputNumber} from 'antd';

interface Item {
  key: string;
  name: string;
  age: number;
  address: string;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: 'number' | 'text';
  record: Item;
  index: number;
  children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = (
  {
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => (
  <td {...restProps}>
    {editing ? (
      <Form.Item
        name={dataIndex}
        style={{margin: 0}}
        rules={[{required: true, message: `Please Input ${title}!`}]}
      >
        {inputType === 'number'
          ? <InputNumber/>
          : <Input/>}
      </Form.Item>
    ) : (
      children
    )}
  </td>
);

export default EditableCell;
