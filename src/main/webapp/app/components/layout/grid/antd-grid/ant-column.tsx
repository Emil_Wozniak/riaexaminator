import {Popconfirm, Tag} from 'antd';
import React from 'react';
import {Item} from "app/components/layout/grid/antd-grid/item";
import {cancel, edit, save} from "app/components/layout/grid/antd-grid/editable-operations";

const AntColumn = (isEditing, samples, form, setEditingKey, data, setData) => [
    {
        title: 'id',
        dataIndex: 'probeNumber',
        width: '5%',
        editable: false,
        render: (pn => pn + 1)
    },
    {
        title: samples ? 'average' : "position",
        dataIndex: samples ? 'average' : "position",
        width: '15%',
        editable: false,
    },
    {
        title: samples ? 'ng' : "read",
        dataIndex: samples ? 'ng' : "meterRead",
        width: '15%',
        editable: false,
        render: (ng => ng && ng !== "" ? `${ng}` : "")
    },
    {
        title: 'cpm',
        dataIndex: 'cpm',
        width: '15%',
        editable: false,
    },
    {
        title: 'flagged',
        dataIndex: 'flagged',
        width: '15%',
        editable: true,
        render: (flag => flag
            ? <Tag color="volcano">Y</Tag>
            : <Tag color="geekblue">N</Tag>)
    },
    {
        title: 'pattern',
        dataIndex: 'pattern',
        width: '40%',
        editable: false,
    },
    {
        title: 'operation',
        dataIndex: 'operation',
        render: (_: any, record: Item) => isEditing(record)
            ? (
                <div>
                    <a
                        href="javascript:;"
                        onClick={() => save(record.key, form, data, setData, setEditingKey)}
                        style={{marginRight: 8}}>
                        Save
                    </a>
                    <Popconfirm
                        title="Sure to cancel?"
                        onConfirm={() => cancel(setEditingKey)}>
                        <a>Cancel</a>
                    </Popconfirm>
                </div>
            ) : (
                <a onClick={() => edit(record, form, setEditingKey)}>
                    Edit
                </a>
            )
    }
];

export default AntColumn;