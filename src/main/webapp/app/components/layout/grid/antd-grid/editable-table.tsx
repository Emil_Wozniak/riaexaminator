import React, {useState} from 'react';
import {Form, Table} from 'antd';
import EditableCell from "app/components/layout/grid/antd-grid/editable-cell";
import AntColumn from "app/components/layout/grid/antd-grid/ant-column";
import {Item} from "app/components/layout/grid/antd-grid/item";
import {cancel} from "app/components/layout/grid/antd-grid/editable-operations";

type EditableTable = {
    results?: any[];
    samples?: boolean;
}

const EditableTable = ({results, samples = false}: EditableTable) => {
    const [form] = Form.useForm();
    const [data, setData] = useState(results);
    const [editingKey, setEditingKey] = useState('');
    const isEditing = (record: Item) => record.key === editingKey;
    const columns = AntColumn(isEditing, samples, form, setEditingKey, data, setData)
        .map(col => !col.editable
            ? col
            : {
                ...col,
                onCell: (record: Item) => ({
                    record,
                    inputType: col.dataIndex === 'ng' ? 'number' : 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: isEditing(record),
                }),
            });
    return (
        <Form form={form} component={false} title='Control Curve'>
            <Table
                components={{body: {cell: EditableCell}}}
                bordered
                dataSource={data}
                columns={columns}
                rowClassName="editable-row"
                pagination={{
                    pageSize: data.length,
                    size: "small",
                    total: data.length,
                    onChange: () => cancel(setEditingKey),
                    hideOnSinglePage: true
                }}
            />
        </Form>
    );
};

export default EditableTable;
