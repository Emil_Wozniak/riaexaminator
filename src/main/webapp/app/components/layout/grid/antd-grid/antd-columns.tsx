import React from 'react';
import RemoveButton from "app/components/layout/button/RemoveButton";
import EditButton from "app/components/layout/button/EditButton";
import ViewButton from "app/components/layout/button/ViewButton";
import {Space, Tag} from 'antd';
import {Link} from 'react-router-dom';

const getColor = (factor: number, entities?: Array<any>, entity?: number) =>
  (entities !== null ? (entities.length < factor) : entity < factor)
    ? 'volcano'
    : 'green';

const AntdColumns = url => [
  {
    title: 'Id',
    dataIndex: 'id',
    key: 'id',
    render: (text => <b>{text}</b>),
    sorter: (a, b) => a.age - b.age
  },
  {
    title: 'Title',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: 'Control Curve',
    dataIndex: 'controlCurve',
    key: 'controlCurve',
    render: cc => (
      <Tag color={getColor(22, cc)}>
        Points: {cc.length}
      </Tag>
    )
  },
  {
    title: 'Graph',
    dataIndex: 'graph',
    key: 'graph',
    render: graph => (
      <>
        <Tag color={getColor(1, graph.coordinates)}>
          Points: {graph.coordinates.length}
        </Tag>
        <br/>
        <Tag color={getColor(20.0, graph.zeroBindingPercent)}>
          Zero binding: {graph.zeroBindingPercent}
        </Tag>
        <br/>
        <Tag color={getColor(0.95, null, graph.r)}>
          R: {graph.r}
        </Tag>
      </>
    )
  },
  {
    title: 'Result Points',
    dataIndex: 'resultPoints',
    key: 'resultPoints',
    render: (rp =>
        <Tag color={getColor(22, rp)}>
          {rp.length}
        </Tag>
    )
  },
  {
    title: 'Action',
    key: 'action',
    render: (data) => (
      <Space size="middle">
        <RemoveButton
          id={data.id}
          Link={Link}
          url={url}
          sort={false}
          activePage={false}
          order={false}
        />
        <EditButton
          id={data.id}
          Link={Link}
          url={url}
          sort={false}
          activePage={false}
          order={false}
        />
        <ViewButton url={url} id={data.id}/>
      </Space>
    ),
  },
];


export default AntdColumns;
