/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

import { Item } from 'app/components/layout/grid/antd-grid/item';

export const edit = (record: Item, form, setEditingKey) => {
  form.setFieldsValue({ name: '', age: '', address: '', ...record });
  setEditingKey(record.key);
};

export const cancel = setEditingKey => {
  setEditingKey('');
};

export const save = async (key: React.Key, form, data, setData, setEditingKey) => {
  try {
    const row = (await form.validateFields()) as Item;
    const newData = [...data];
    const index = newData.findIndex(item => key === item.key);
    if (index > -1) {
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...row
      });
      setData(newData);
      setEditingKey('');
    } else {
      newData.push(row);
      setData(newData);
      setEditingKey('');
    }
  } catch (errInfo) {
    console.error('Validate Failed:', errInfo);
  }
};
