import React from 'react';
import {Table} from 'antd';
import AntdColumns from "app/components/layout/grid/antd-grid/antd-columns";

type AntdTableProps = {
  data: any[];
  url: string;
}

const AntdTable = ({data, url}: AntdTableProps) => (
  <Table
    rowClassName="editable-row"
    bordered
    size="middle"
    columns={AntdColumns(url)}
    dataSource={data && data.length > 0 ? data : []}/>
);

export default AntdTable;
