/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

import React from 'react';

interface IDropdownItem {
    className?: any;
    header?: boolean;
    divider?: boolean;
    active?: boolean;
    toggle?: boolean;
    disabled?: boolean;
    tag?: string;
    onClickFunc?: Function;
    href?: any;
    tabIndex?: any;
    children?: any;
}

const DropdownItem = (
    {
        disabled,
        header,
        divider,
        onClickFunc,
        className,
        href,
        children,
        tag,
    }: IDropdownItem) => {

    let Tag = tag;

    const getTabIndex = () => {
        if (disabled || header || divider) {
            return -1;
        }
        return 0;
    };

    const tabIndex = getTabIndex();
    const role = tabIndex > -1
        ? 'button'
        : undefined;

    const assignOnClick = (e: Event) => {
        if (disabled
            || header
            || divider) {
            e.preventDefault();
            return;
        }
        if (onClickFunc) {
            onClickFunc(e);
        }
    };

    if (Tag === 'button') {
        Tag = header
            ? 'h6'
            : divider
                ? 'div'
                : href
                    ? 'a'
                    : '';
    }
    return (
        <div
            className={className}
            role={role}
            onClick={() => assignOnClick}>
            <div role={Tag}>
                {children}
            </div>
        </div>
    );
};


export default DropdownItem;