import React from "react";

type Data = {
  id: number;
  name: any;
  delete: any;
  edit: any;
  see: any;
  extraSpanData?: any;
}
type Table = {
  labels: Array<string>;
  metadata: Array<Data>
}
export const TailwindTable = ({labels, metadata}: Table) => {
  return (
    <table className="border-collapse w-full">
      <thead>
      <tr>
        {labels.map((label, i) => (
          <th
            key={`table-label-${label}-${i}`}
            className="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell">
            <p>{label}</p>
          </th>
        ))}
      </tr>
      </thead>
      <tbody>
      <tr
        className="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
        {metadata.map((data, i) => (
          <td
            key={`table-thread-data-${data}-${i}`}
            className="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static pb-4">
              <span
                className="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase">
                Name
              </span>
            {data.name}
            {data.extraSpanData}
            <span className="border-8 border-white"/>
            <span
              className="w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static mt-4">
              <span
                className="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase">
                Actions
              </span>
              {data.edit}
              {data.delete}
              {data.see}
            </span>
          </td>
        ))}
      </tr>
      </tbody>
    </table>
  );
}

