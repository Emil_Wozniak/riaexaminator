import React from 'react';
import {createClass} from "app/components/layout/grid/model/util";
import {GridModel} from "app/components/layout/grid/model/grid.model";

interface IImage extends GridModel {
    classes?: string;
    rounded?: boolean;
    source: string;
    alt: string;
}

const Image = (
    {
        classes,
        opacity,
        width,
        box,
        background,
        color,
        id,
        children,
        rounded,
        source,
        alt
    }: IImage) => {
    const style = createClass({background, color, id, children, opacity, width, box});
    const isRound = rounded ? "rounded-full" : '';
    return (
        <img
            src={source}
            className={`${isRound} ${style} ${classes}`}
            id={id}
            alt={alt}>
            {children}
        </img>)
};

export default Image;