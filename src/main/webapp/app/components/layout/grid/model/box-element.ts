export type Box = {
  element: BoxElement;
  side?: BoxSide;
  value: BoxPropsValues;
};

export type BoxModel = {
  BoxElement: BoxElement;
  BoxSide?: BoxSide;
  BoxPropsValues: BoxPropsValues;
};

export enum BoxElement {
  MARGIN = 'm',
  PADDING = 'p'
}

/**
 * html box model side.
 */
export enum BoxSide {
  Y = 'y',
  X = 'x',
  RIGHT = 'r',
  TOP = 'top',
  BOTTOM = 'b',
  LEFT = 'l'
}

type BoxPropsValues = 1 | 2 | 3 | 4 | 5 | 6 | 8 | 10 | 12 | 16 | 20 | 24 | 32 | 40 | 48 | 56 | 64 | 'auto';
