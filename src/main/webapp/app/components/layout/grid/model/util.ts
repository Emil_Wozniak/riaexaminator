import { BackgroundState, GridModel } from 'app/components/layout/grid/model/grid.model';
import { Box } from 'app/components/layout/grid/model/box-element';

export const of = (color: Color, strength: Strength): BackgroundState => ({ color, strength });

export const find = (color?: Color, strength?: Strength) =>
  color
    ? color === 'transparent' || color === 'white' || color === 'black'
      ? `text-${color}`
      : `text-${color}-${strength !== undefined || strength !== '' ? strength : 500}`
    : '';

const createBoxStyle = (boxes?: Array<Box>): string =>
  boxes ? boxes.map(b => `${b.element}${b.side ? b.side : ''}-${b.value}`).join(' ') : '';

const isShadow = (shadow: boolean): string => (shadow ? 'shadow' : '');

const getStyle = (color: Color, strength: Strength, boxes: Array<Box>, shadow) =>
  find(color, strength) + ' ' + createBoxStyle(boxes) + ' ' + isShadow(shadow);

const isColor = (color: Color) => (color ? color : '');

const isOpacity = (opacity: Opacity) => (opacity ? `opacity-${opacity} ` : '');

const isBackground = background => (background ? `bg-${background.color}-${background.strength} ` : ' ');

const isWidth = (width: Width) => (width ? `w-${width} ` : ' ');

export const createClass = ({ box, width, background, opacity, color, strength, shadow }: GridModel): string =>
  isColor(color) + isOpacity(opacity) + isBackground(background) + isWidth(width) + getStyle(color, strength, box, shadow);
