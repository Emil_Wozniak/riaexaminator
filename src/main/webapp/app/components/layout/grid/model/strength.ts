/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

/**
 * Tailind css className required for type color
 * */
type Strength = undefined | '' | '100' | '200' | '300' | '400' | '500' | '600' | '700' | '800' | '900';
