/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

/**
 * Tailind css className includes font-{value},
 * for example font-semibold.
 */
import {Box} from "app/components/layout/grid/model/box-element";
import {Decoration} from "app/components/layout/grid/model/decoration";

export interface Tailwind {
    family?: string;
    weight?: Weight;
    color?: Color;
    strength?: Strength;
    decoration?: Decoration;
    align?: Align;
    size?: Size;
    box?: string;
    hover?: string;
    background?: BackgroundState;
}

/**
 * @see color
 * @see strength
 * @see theme
 */
export interface BackgroundState {
    color: Color;
    strength: Strength;
}

/**
 * @see BoxModel
 * @see theme
 * @see color
 * @see BackgroundState
 * @see strength
 * @see background requires {@see createBackground}
 * function to create {@see BackgroundState}
 * example:
 *  <Card background={createBackground("blue", "500")}/>
 */
export interface GridModel {
    box?: Array<Box>;
    width?: Width;
    height?: Height;
    background?: BackgroundState;
    opacity?: Opacity;
    color?: Color;
    family?: string;
    strength?: Strength;
    shadow?: boolean;
    children?: any;
    id?: string;
}