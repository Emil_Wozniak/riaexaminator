/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

/**
 * Tailind css className includes:
 * for values "transparent" | "white" | "black"    =>    text-{value}               example text-white
 * for rest values includes also strength          =>    text-{value}-{strength}    example text-blue-300
 */
type Color =
  | undefined
  | 'inherit'
  | 'transparent'
  | 'white'
  | 'black'
  | 'gray'
  | 'red'
  | 'yellow'
  | 'green'
  | 'teal'
  | 'blue'
  | 'indigo'
  | 'purple'
  | 'pink';
