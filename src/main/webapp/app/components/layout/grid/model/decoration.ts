/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

/**
 * Tailind css className includes {value} only, for example italic
 */
export type Decoration =
  | 'italic'
  | 'not-italic'
  | 'normal'
  | 'uppercase'
  | 'lowercase'
  | 'capitalize'
  | 'normal-case'
  | 'underline'
  | 'line-through'
  | 'no-underline';
