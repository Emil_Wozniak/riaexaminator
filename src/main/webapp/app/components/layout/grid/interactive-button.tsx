import React from "react";
import FadingElement from "./fading-element";

interface IInteractiveButtonProps {
    hideClasses: string;
    activeClasses: string;
    label?: string;
    labelClass?: string;
    hideLabel?: boolean;
    fading?: boolean;
    fadeSpeed?: number;
}

interface IInteractiveButtonState {
    isHovering: boolean;
    isActive: boolean;
    hideLabel: boolean;
}

class InteractiveButton extends React.Component<IInteractiveButtonProps, IInteractiveButtonState> {
    constructor(props: IInteractiveButtonProps) {
        super(props);
        this.state = {
            isHovering: true,
            isActive: false,
            hideLabel: true
        };
    }

    handleMouseOver = () => {
        this.setState({isHovering: false, hideLabel: false});
    };

    handleMouseOut = () => {
        this.setState({isHovering: true, hideLabel: true});
    };

    handleClick = () => {
        const active = !this.state.isActive;
        this.setState({isActive: active, hideLabel: active});
    };

    setClassName = (isHovering: boolean): string => {
        return isHovering ? `${this.props.hideClasses}` : `${this.props.activeClasses}`;
    };

    render() {
        const {isHovering} = this.state;
        return (
            <div className={this.setClassName(isHovering)}
                 onClick={this.handleClick}
                 onMouseOver={this.handleMouseOver}
                 onMouseOut={this.handleMouseOut}>
                <ButtonLabel
                    label={this.props.label}
                    labelClass={this.props.labelClass}
                    hideLabel={this.props.hideLabel}
                    isHide={this.state.hideLabel}/>
                {!this.state.isHovering ?
                    this.props.fading ?
                        <FadingElement
                            speed={this.props.fadeSpeed}
                            open={!this.state.isHovering}>
                            {this.props.children}
                        </FadingElement>
                        : <>{this.props.children}</>
                    : null}
            </div>
        );
    }
}

interface IButtonLabel {
    label?: string;
    labelClass?: string;
    hideLabel?: boolean;
    isHide?: boolean;
}

const ButtonLabel = (props: IButtonLabel) =>
    <>{props.hideLabel && props.label ?
        props.isHide ?
            <div className={props.labelClass}>
                {props.label}
            </div> :
            null :
        null}
    </>;


export default InteractiveButton;