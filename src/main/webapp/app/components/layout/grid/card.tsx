import * as React from 'react';
import {GridModel} from "./model/grid.model";
import {createClass} from "app/components/layout/grid/model/util";
import {CARD} from "app/components/styles/style-constants";

interface ICard extends GridModel {
    classes?: string;
}

export const Card = (
    {
        classes,
        opacity,
        width,
        box,
        background,
        color,
        id,
        children
    }: ICard) => {
    const style = createClass({background, color, id, children, opacity, width, box});
    return (
        <div
            className={`${CARD} ${style} ${classes}`}
            id={id}>
            {children}
        </div>)
};