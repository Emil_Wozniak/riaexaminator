import React from 'react';
import {GridModel} from "app/components/layout/grid/model/grid.model";
import {createClass} from "app/components/layout/grid/model/util";

interface ICenteredItem extends GridModel {
    classes?: string;
    children: any;
    style?: {};
}

const CenteredItem = (
    {
        box,
        classes,
        children,
        width,
        style
    }: ICenteredItem) => {
    const className = createClass({box, width});
    return (
        <div
            className={`block ml-auto mr-auto ${className} ${classes}`}
            style={style}>
            {children}
        </div>
    );
};


export default CenteredItem;