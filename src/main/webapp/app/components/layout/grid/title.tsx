import * as React from 'react';
import {Tailwind} from "./model/grid.model";
import {find} from "app/components/layout/grid/model/util";

interface ITitle extends Tailwind {
    title: string;
    family: any;
    decoration: any;
    align: any;
    weight: any;
    color: any;
    size: any;
    strength: any;
    hover: any;
}

export const Title = (
    {
        title,
        family,
        decoration,
        align,
        weight,
        color,
        size,
        strength,
        hover
    }: ITitle) => {
    const colorValue = find(color, strength);
    return (
        <h2 className={`
           font-${family} 
           ${colorValue} 
           font-${weight} 
           text-${align} 
           ${decoration} 
           text-${size} 
           hover:${hover}`}>
            {title}
        </h2>)
};