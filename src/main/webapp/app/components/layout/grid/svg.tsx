import React from 'react';

interface ISvg {
    d: string;
    viewBox: string;
    title?: string;
}

export const Svg = ({d, viewBox, title}: ISvg) =>
    <svg className="h-4 fill-current text-teal-700 pr-4" xmlns="http://www.w3.org/2000/svg"
         viewBox={viewBox}>
        {title ? <title>{title}</title> : <div/>}
        <path d={d}/>
    </svg>;