import * as React from 'react';
import {COLUMN} from "app/components/styles/style-constants";
import {GridModel} from "./model/grid.model";
import {createClass} from "app/components/layout/grid/model/util";

/**
 * in order of create correct Column {@see GridModel}
 * @see GridModel
 * @see createBackground
 */
export const Column =
    ({
         box,
         width,
         strength,
         background,
         shadow,
         color,
         id,
         children
     }: GridModel) => {
        return (
            <div
                className={`${COLUMN} ${createClass({box, width, background, strength, shadow, color})}`}
                id={id}>
                {children}
            </div>)
    };