/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */
import React, {useState} from 'react';
import "app/components/layout/grid/styles/modal.scss"
import {GridModel} from "app/components/layout/grid/model/grid.model";
import {FADE_IN} from "app/components/layout/grid/styles/styles";

interface IModal extends GridModel {
    title: string;
    btnTitle: string;
    isOpen: boolean;
    className?: string;
}

const closeIcon = "M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z";

const Modal = ({children, title, btnTitle, isOpen, className}: IModal) => {
    const initState = isOpen !== undefined ? isOpen : false;
    const [modal, setModal] = useState(initState);
    const toggleModal = () => {
        setModal(!modal);
    };
    return (
        <>{modal
            ? <div className={`${FADE_IN} ${className} fixed z-50 pin overflow-auto bg-smoke-dark flex `}>
                <span
                    onClick={() => toggleModal()}
                    className="pin-t pin-r px-4">
                        <svg
                            className="h-12 w-12 text-grey hover:text-grey-darkest"
                            role="button"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20">
                            <title>Close</title>
                            <path d={closeIcon}/>
                        </svg>
                    </span>
                <div
                    className="animated fadeInUp fixed shadow-inner max-w-md md:relative pin-b pin-x align-top justify-end md:justify-center p-8 bg-white md:rounded w-full md:h-auto md:shadow flex flex-col">
                    <h2 className="text-4xl text-center font-hairline md:leading-loose text-grey mb-4">
                        {title}
                    </h2>
                    <div className="leading-normal">
                        {children}
                    </div>

                </div>
            </div>
            : <button
                className="bg-grey-lighter flex-1 border-b-2 md:flex-none border-grey ml-2 hover:bg-grey-lightest text-grey-darkest font-bold py-4 px-6 rounded"
                onClick={() => toggleModal()}>
                {btnTitle}
            </button>}
        </>
    );
};


export default Modal;