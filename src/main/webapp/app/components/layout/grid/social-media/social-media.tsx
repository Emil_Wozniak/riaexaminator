import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {SizeProp} from '@fortawesome/fontawesome-svg-core';

interface ISocialMedia {
    facebookUserName?: string;
    isTwitter?: string;
    githubUserName?: string;
    gitlabUserName?: string;
    isInstagram?: string;
    isYT?: string;
    size: SizeProp
}

function getAmount(facebookUserName: string, isTwitter: string, githubUserName: string, gitlabUserName: string, isInstagram: string, isYT: string) {
    return facebookUserName ? 1 : 0 + isTwitter ? 1 : 0 + githubUserName ? 1 : 0 + gitlabUserName ? 1 : 0 + isInstagram ? 1 : 0 + isYT ? 1 : 0;
}

const SocialMedia = (
    {
        facebookUserName,
        isTwitter,
        githubUserName,
        gitlabUserName,
        isInstagram,
        isYT,
        size
    }: ISocialMedia) => (
    <div
        className={`mt-6 pb-16 lg:pb-0 lg:w-full mx-auto flex flex-wrap items-center justify-between w-${
            getAmount(
                facebookUserName,
                isTwitter,
                githubUserName,
                gitlabUserName,
                isInstagram,
                isYT)}/5`}>
        {facebookUserName
            ? <a className="link" href={`https://www.facebook.com/${facebookUserName}`}>
                <FontAwesomeIcon icon={['fab', 'facebook']} size={size}/>
            </a>
            : null}
        {isTwitter
            ? <a className="link" href={isTwitter}>
                <FontAwesomeIcon icon={['fab', 'twitter']} size={size}/>
            </a>
            : null}
        {githubUserName
            ? <a className="link" href={`https://github.com/${githubUserName}`}>
                <FontAwesomeIcon icon={['fab', 'github']} size={size}/>
            </a>
            : null}
        {gitlabUserName
            ? <a className="link" href={`https://gitlab.com/${gitlabUserName}`}>
                <FontAwesomeIcon icon={['fab', 'gitlab']} size={size}/>
            </a>
            : null}
        {isInstagram
            ? <a className="link" href={isInstagram}>
                <FontAwesomeIcon icon={['fab', 'instagram']} size={size}/>
            </a>
            : null}
        {isYT
            ? <a className="link" href={isYT}>
                <FontAwesomeIcon icon={['fab', 'youtube']} size={size}/>
            </a>
            : null}
    </div>);

export default SocialMedia;