import React from 'react';
import Image from "app/components/layout/grid/image";
import {BoxElement, BoxSide} from "app/components/layout/grid/model/box-element";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

interface IProfile {
    name: string;
    image: string;
    job: string;
    geolocation: string;
    description?: string;
    socialMedia?: any
}

const Profile = (
    {
        name,
        image,
        job,
        geolocation,
        description,
        socialMedia
    }: IProfile) => (
    <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-16 lg:my-0">
        <div
            id="profile"
            className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
            <div className="p-4 md:p-12 text-center lg:text-left">
                <Image
                    source={image}
                    alt="fb image"
                    width={48}
                    height={48}
                    box={[
                        {element: BoxElement.MARGIN, side: BoxSide.X, value: "auto"},
                        {element: BoxElement.MARGIN, side: BoxSide.TOP, value: 16}
                    ]}
                    classes="block lg:hidden  shadow-xl bg-cover bg-center"
                    rounded/>
                <h1 className="text-3xl font-bold pt-8 lg:pt-0">{name}</h1>
                <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"/>
                <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
                    <FontAwesomeIcon icon={'suitcase'}/>&nbsp;&nbsp;
                    {job}
                </p>
                <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
                    <FontAwesomeIcon icon={'globe-europe'}/>&nbsp;&nbsp;
                    {geolocation}
                </p>
                {description
                    ? <p className="pt-8 text-sm">{description} </p>
                    : <div/>
                }
                {socialMedia ? socialMedia : null}
            </div>
        </div>
        <div className="absolute top-0 right-0 h-12 w-18 p-4"/>
    </div>

);

export default Profile;