import * as React from 'react';
import {Tailwind} from "./model/grid.model"
import {find} from "app/components/layout/grid/model/util";

/**
 * @see Tailwind
 */
type Field = {
    family?: string;
    color?: Tailwind["color"];
    strength?: Tailwind["strength"];
    weight?: Tailwind["weight"];
    decoration?: Tailwind["decoration"];
    size?: Tailwind["size"];
    align?: Tailwind["align"];
    hover?: string;
    children?: any;
};

export const Field = (props: Field) => {
    const {family, color, strength, weight, size, align, hover, children, decoration} = props;
    const colorValue = find(color, strength);
    return (
        <div
            className={`font-${family} ${colorValue} font-${weight} text-${align} ${decoration} text-${size} hover:${hover}`}>
            {children}
        </div>
    );
};

