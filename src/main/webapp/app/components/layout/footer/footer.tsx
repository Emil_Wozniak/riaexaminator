import React from 'react';
import {ColumnElement, ColumnSection} from "app/components/layout/grid/grid.components";
import {translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

interface IFooter {
    title: string;
}

const Footer = ({title}: IFooter) => (
    <footer>
        <div className="container p-2">
            <div className="flex mb-2">
                <ColumnSection
                    name={translate("global.menu.contact")}
                    nameClass={"text-orange mb-2"}>
                    <ColumnElement
                        listClass="text-grey">
                        <FontAwesomeIcon icon="envelope-open-text"/>&nbsp;
                        emil.wozniak.591986@gmail.com
                    </ColumnElement>
                </ColumnSection>
                <ColumnSection
                    name={"Gitlab"}
                    nameClass={"text-orange mb-2"}>
                    <ColumnElement
                        listClass="text-grey">
                        <a href="https://gitlab.com/Emil_Wozniak/riaexaminator">
                            <FontAwesomeIcon icon={["fab","gitlab"]}/>&nbsp;
                            {translate("global.repo")}
                        </a>
                    </ColumnElement>
                </ColumnSection>
            </div>
            <b>{title}</b>
        </div>
    </footer>
);

export default Footer;
