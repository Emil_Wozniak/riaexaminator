import React from 'react';
import MenuItem from 'app/components/layout/menus/menu-item';
import {Translate} from 'react-jhipster';
import {NavDropdown} from './menu-components';

export type IconSize = "sm"| "md"| "lg";

interface IEntitiesMenu {
    size?: IconSize;
}
export const EntitiesMenu = ({size}: IEntitiesMenu)  => (
  <NavDropdown icon="file-alt" name="" id="entity-menu" style={{color: 'black'}} size={size} >
    <MenuItem icon="file-word" to="/examination-file">
      <Translate contentKey="examinatorApp.examinationFile.txt" />
    </MenuItem>
  </NavDropdown>
);
