import React from 'react';
import {DropdownItem} from 'reactstrap';
import {NavLink as Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IconProp} from '@fortawesome/fontawesome-svg-core';

export interface IMenuItem {
    icon: IconProp;
    to: string;
    id?: string;
}

export default class MenuItem extends React.Component<IMenuItem> {
    render() {
        const {to, icon, id, children} = this.props;
        return (
            <DropdownItem tag={Link} to={to} id={id} className="dropdown text-black">
                <FontAwesomeIcon icon={icon} fixedWidth/>{children}
            </DropdownItem>
        );
    }
}

interface IMenuField {
    to: string;
    text: string;
    icon: IconProp;
    iconStyle?: React.CSSProperties
    id?: string;
}

export const MenuField = ({to, text, icon, id, iconStyle}: IMenuField) =>
    <Link to={to} id={id}>
        <button type="button" style={{color: "black"}}>
            <FontAwesomeIcon style={iconStyle} icon={icon}/>{text}
        </button>
    </Link>;

