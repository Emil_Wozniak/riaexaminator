import React from 'react';
import MenuItem, {MenuField} from 'app/components/layout/menus/menu-item';
import {Translate, translate} from 'react-jhipster';
import {NavDropdown} from './menu-components';

const AccountMenuItemsAuthenticated = () =>
    <>
        <MenuItem icon="wrench" to="/account/settings">
            <Translate contentKey="global.menu.account.settings">Settings</Translate>
        </MenuItem>
        <MenuItem icon="lock" to="/account/password">
            <Translate contentKey="global.menu.account.password">Password</Translate>
        </MenuItem>
        <MenuItem icon="sign-out-alt" to="/logout">
            <Translate contentKey="global.menu.account.logout">Sign out</Translate>
        </MenuItem>
    </>;

const AccountMenuItems = () => (
    <>
        <MenuField
            to="/login"
            text={translate('global.menu.account.login')}
            id="login-item"
            iconStyle={{color: "grey"}}
            icon="fingerprint"/>
        <MenuField
            to="/account/register"
            text={translate('global.menu.account.register')}
            id="register"
            icon="sign-in-alt"/>
    </>
);


export const AccountMenu = ({isAuthenticated = false}) => (
    <NavDropdown icon="user" name="" id="account-menu" iconColor="grey">
        {isAuthenticated
            ? <AccountMenuItemsAuthenticated/>
            : <AccountMenuItems/>}
    </NavDropdown>
);

export default AccountMenu;
