import React from 'react';

import {DropdownMenu, DropdownToggle, UncontrolledDropdown} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function getSize(size) {
    const large = 48;
    const medium = 36;
    const small = 24;
    return size === "lg"
        ? large
        : size === "md"
            ? medium
            : small;
}

type NavDropdown = {
  size: "lg" | "md";
  id: string;
  iconColor: string;
}

export const NavDropdown = props => {
    const size = getSize(props.size);
    return (
        <UncontrolledDropdown nav inNavbar id={props.id}>
            <DropdownToggle nav className="d-flex align-items-center">
                <FontAwesomeIcon
                    icon={props.icon}
                    style={{
                        color: props.iconColor ? props.iconColor : 'white',
                        height: size, width: size
                    }}/>
                <span>{props.name}</span>
            </DropdownToggle>
            <DropdownMenu right style={props.style}>
                {props.children}
            </DropdownMenu>
        </UncontrolledDropdown>
    )
};
