import React from 'react';
import {DropdownItem} from 'reactstrap';
import {NavDropdown} from './menu-components';
import {languages, locales} from 'app/config/translation';

export const LocaleMenu = ({onClick}) =>
  Object.keys(languages).length > 1 && (
    <NavDropdown icon="flag" name="" iconColor="grey">
      {locales.map(locale => (
        <DropdownItem key={locale} value={locale} onClick={onClick}>
          {languages[locale].name}
        </DropdownItem>
      ))}
    </NavDropdown>
  );
