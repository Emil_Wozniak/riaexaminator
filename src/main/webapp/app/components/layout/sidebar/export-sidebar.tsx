/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
import React from 'react';
import XLSXExportButton from "app/components/layout/table/xlsx-export-button";
import {XlsButton} from "app/components/layout/table/xls-button";
import {CsvButton} from "app/components/layout/table/csv-button";
import {Col, Row} from 'antd';
import {IResults} from "app/components/model/results.model";

export interface ExportSidebarProps {
    results: IResults;
}

const ExportSidebar = ({results}: ExportSidebarProps) => (
    <Row gutter={[48, 48]} className="right-0 fixed z-10">
        <Col span={24}>
            <XLSXExportButton results={results}/>
        </Col>
        <Col span={24}>
            <XlsButton results={results}/>
        </Col>
        <Col span={24}>
            <CsvButton results={results}/>
        </Col>
    </Row>
);

export default ExportSidebar;
