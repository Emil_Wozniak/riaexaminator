import React from "react";
import "./buttons.scss";
import {AvGroup} from 'availity-reactstrap-validation';
import {Col, FormGroup} from 'reactstrap';
import CenteredItem from "app/components/layout/grid/centered-item";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

export const ButtonSet = (props: any) => (
    <div className="card p-2 m2">
        <CenteredItem>
            <div className="row">
                {props.children}
            </div>
        </CenteredItem>
    </div>
);

interface IFileInputBtn {
    onChange: any;
}

export const FileInputBtn = ({onChange}: IFileInputBtn) => (
    <div className="image-upload">
        <label
            htmlFor="file-input"
            style={{background: "#90ee90"}}
            className="button">
            <FontAwesomeIcon icon="file"/>
        </label>
        <input id="file-input" type="file" onChange={onChange}/>
    </div>
);

export const UploadBtn = () => (
    <button
        type="submit"
        className="button"
        style={{background: "#B5D0E1"}}>
        <FontAwesomeIcon icon="upload"/>
    </button>
);

interface ISettingBtn {
    defaults: boolean;
    onDefaultsChange: any;
}

export const SettingBtn = ({defaults, onDefaultsChange}: ISettingBtn) => (
    <FormGroup>
        <Col sm="12" md="12" lg="12" xl="12">
            <AvGroup check className="image-upload">
                <label
                    htmlFor="settings"
                    className="button"
                    style={{background: "#e07872"}}>
                    <FontAwesomeIcon icon="sliders-h"/>
                </label>
                <input
                    id="settings"
                    type="checkbox"
                    onClick={onDefaultsChange}
                    defaultChecked={defaults}/>
            </AvGroup>
        </Col>
    </FormGroup>
);