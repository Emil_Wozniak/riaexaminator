import React from 'react';
import {Translate} from 'react-jhipster';
import {Button} from 'reactstrap';

type EditButton = {
  Link: any;
  url: string;
  id: number;
  activePage: boolean;
  sort: boolean;
  order: any;
}
const EditButton = ({Link, url, id, activePage, sort, order}: EditButton) => (
  <Button
    tag={Link}
    to={`${url}/${id}/edit?page=${activePage}&sort=${sort},${order}`}
    color="primary"
    size="sm">
    <Translate contentKey="entity.action.edit">Edit</Translate>
  </Button>
);

export default EditButton;
