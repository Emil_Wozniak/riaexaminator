import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Translate} from 'react-jhipster';
import {Button} from 'reactstrap';
import {Link} from 'react-router-dom';

type ViewButton = {
  size?: string;
  url: string;
  id: number
}
const ViewButton = ({size, url, id}: ViewButton) => (
  <Button
    style={{color: "#fff"}}
    tag={Link}
    to={`${url}/${id}`}
    color="info"
    size={size ? size : "sm"}>
    <FontAwesomeIcon icon="eye"/>{' '}
    <Translate contentKey="entity.action.view">View</Translate>
  </Button>
);

export default ViewButton;
