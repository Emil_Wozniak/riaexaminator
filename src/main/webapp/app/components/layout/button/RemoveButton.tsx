import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Translate} from 'react-jhipster';
import {Button} from 'reactstrap';

type RemoveButton = {
  size?: string;
  Link: any;
  id: number;
  url: string;
  activePage: boolean;
  sort: boolean;
  order: any;
}

const RemoveButton = ({size, Link, id, url, activePage, sort, order}: RemoveButton) => (
  <Button
    style={{color: "#fff"}}
    tag={Link}
    to={`${url}/${id}/delete?page=${activePage}&sort=${sort},${order}`}
    color="danger"
    size={size ? size : "sm"}>
    <FontAwesomeIcon icon="trash"/>{' '}
    <Translate contentKey="entity.action.delete">
      Delete
    </Translate>
  </Button>
);

export default RemoveButton;
