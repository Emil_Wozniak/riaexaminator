/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
import React from 'react';
import {IResults} from "app/components/model/results.model";
import {Row} from "app/components/layout/grid/row";
import {Col, Divider} from 'antd';

const getStyle = (value: number, factor: number) => value > factor
    ? {color: "green"}
    : {color: "red"};

export interface ResultInfoProps {
    results: IResults;
}

const ResultInfo = ({results}: ResultInfoProps) => (
    <>
        <Divider>Examination results:</Divider>
        {results.graph !== undefined && results.graph
            ? (
                <Row gutter={12}>
                    <Col span={4}/>
                    <Col className="gutter-row" span={4}>
                        <p style={getStyle(results.graph.zeroBindingPercent, 20)}>
                            Wiązanie: {results.graph.zeroBindingPercent}%
                        </p>
                    </Col>
                    <Col className="gutter-row" span={4}>
                        <p style={getStyle(results.graph.r, 0.9)}>
                            R: {results.graph.r}%
                        </p>
                    </Col>
                    <Col span={4}/>
                </Row>
            )
            : <div/>}
    </>
);

export default ResultInfo;
