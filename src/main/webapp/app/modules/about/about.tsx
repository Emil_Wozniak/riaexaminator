import React from 'react';
import Profile from "app/components/layout/grid/profile";
import SocialMedia from "app/components/layout/grid/social-media/social-media";

const FB_IMG = "https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-9/59690607_1404464249696678_3864428345340985344_o.jpg?_nc_cat=109&_nc_sid=174925&_nc_ohc=hAloW0pkrIEAX-5sD4H&_nc_ht=scontent-waw1-1.xx&oh=bb0c6aaeff3e4b16feaee1fe90ffbf5e&oe=5EB36EBA";

const About = () => (
    <div className="bg-gray-100 m-2 p-8 shadow text-center Baskerville">
        <h4 className="m-2 p-2">
            Osoby biorące udział przy wykonaniu strony:
        </h4>
        <Profile
            name="Emil Woźniak"
            image={FB_IMG}
            geolocation="Ludowa 2 (ul), 00-780 Warszawa (Mokotów)"
            job="Chemistry Technician & Web-Developer"
            description=""
            socialMedia={
                <SocialMedia
                    githubUserName="Emil-Wozniak"
                    gitlabUserName="Emil_Wozniak"
                    facebookUserName="emil.wozniak.397"
                    size={"2x"}/>}
        />
        <div className="flex inline-block m-2 w-full">
        </div>
    </div>
);


export default About;