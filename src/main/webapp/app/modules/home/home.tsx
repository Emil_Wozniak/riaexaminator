import './home.scss';

import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import NotLoginUser from 'app/components/home/not-login-user';
import LoginUser from 'app/components/home/login-user';

export type IHomeProp = StateProps;

export const Home = (props: IHomeProp) => {
    const {account} = props;
    return (
        <article className="bg-gray-500 m-2 shadow">
            <div className="white-center">
                <h1 className="font-medium text-4xl p-4 Baskerville">
                    Wyślij plik aby otrzymać wyniki
                </h1>
                <h2 className="text-2xl font-thin tracking-wide mt-2 mb-4 Source">
                    Dane sporządzone za pomocą tego pliku będą przechowywane dla
                    Ciebie przez nieograniczony czas, jeśli się zalogujesz
                </h2>
                <div className="bg-orange text-white p-4 font-bold inline-block mb-4 Source">
                    {account && account.login
                        ? <LoginUser account={account}/>
                        : <NotLoginUser/>}
                </div>
            </div>
            <div className="block ml-auto mr-auto file-upload">
                <Link to="/examination-file">
                    <FontAwesomeIcon size="5x" icon="file-word" fixedWidth/>
                </Link>
            </div>
        </article>
    );
};

const mapStateToProps = storeState => ({
    account: storeState.authentication.account,
    isAuthenticated: storeState.authentication.isAuthenticated
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
