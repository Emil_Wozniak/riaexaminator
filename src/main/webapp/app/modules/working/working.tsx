import React, {useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Translate} from 'react-jhipster';
import CenteredItem from "app/components/layout/grid/centered-item";
import {Row} from "app/components/layout/grid/row";
import {Column} from "app/components/layout/grid/column";
import {BoxElement, BoxSide} from "app/components/layout/grid/model/box-element";

const whiteSpace = '␣';
const tabulator = '↹';

function replaceWhiteSpaces() {
    return line => line.startsWith(" ")
        ? line.replace(" ", whiteSpace)
        : line;
}

function replaceTabulators() {
    return line =>
        line.startsWith(" \t")
        || line.startsWith(`${whiteSpace}\t`)
            ? line.replace("\t", tabulator)
            : line;
}

const handleClick = (setState) => {
    setState([""]);
    fetch("../../content/files/A16_244.txt")
        .then((response) => response.text())
        .then(text => text.split('\n'))
        .then(text => text
            .map(replaceWhiteSpaces())
            .map(replaceTabulators()))
        .then(text => setState(text))
};

const handleShow = (setShow, hide) => hide ? setShow(false) : setShow(true);

const FetchButton = ({setText, setShow, hide}) => (
    <button
        className="my-2"
        onClick={() => {
            handleClick(setText);
            handleShow(setShow, hide);
        }}>
        <Row>
            <FontAwesomeIcon icon="file-word" size="2x" color="gray"/>
            <h4 className="m-1 flex text-center text-gray-700">Podgląd pliku</h4>
        </Row>
    </button>);

interface ISampleTextColumn {
    hide: boolean[];
    text: string[];
}

const SampleFileColumn = ({text, hide}: ISampleTextColumn) => (
    <Column
        box={[{element: BoxElement.MARGIN, value: 4},
            {element: BoxElement.MARGIN, side: BoxSide.LEFT, value: 40}]}
        width="1/2">
        {!hide
            ? <p className="shadow-2xl p-1">
                {text.map((line, i) => (
                    <li key={`sample-line-${i}`} style={{listStyle: 'none'}}>
                        {line}
                    </li>)
                )}
            </p>
            : <div/>
        }
    </Column>);

const SampleFile = ({setText, setShow, hide}) => (
    <Column
        box={[{element: BoxElement.MARGIN, value: 4}]}
        width="1/2">
        <h2>Instrucja</h2>
        <h5>
            Plik do analizy powinien wyglądać jak plik po kliknięciu poniższego przycisku:
        </h5>
        <FetchButton
            setText={setText}
            setShow={setShow}
            hide={hide}/>
        <h6>Opis symboli</h6>
        <p>{whiteSpace} - oznacza spację</p>
        <p>{tabulator} - oznacza tabulator</p>
        <h5 className="text-justify">Aplikacja jest czuła na każdy symbol, również symbole specjalne, w związku
            z tym muszą one znajdować się w pliku przesłanym jeśli zostaną zastosowane
            standardowe ustawienia.</h5>
        <h3>Standardowe ustawienia:</h3>
        <h5>Jeśli użytkownik nie kliknie przycisku <FontAwesomeIcon icon="sliders-h" color="red"/>
            &nbsp;i nie wprowadzi żadnej wartości dla pola &ldquo;<b>string startowy</b>&ldquo;.
            Aplikacja przyjmię że każda linia z wynikami powinna zaczynać się od
            &ldquo;<b>{whiteSpace}{tabulator}Unk_</b>&ldquo;
        </h5>
    </Column>);

const Working = () => {
        const [text, setText] = useState([""]);
        const [hide, setShow] = useState([true]);
        return (
            <CenteredItem>
                <CenteredItem width="11/12">
                    <h1><Translate contentKey="global.menu.working"/></h1>
                </CenteredItem>
                <Row>
                    <SampleFileColumn
                        hide={hide}
                        text={text}/>
                    <SampleFile
                        setText={setText}
                        setShow={setShow}
                        hide={hide}/>
                </Row>
            </CenteredItem>
        )
    }
;

export default Working;