import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const Contact = () => {
    return (
        <div className="shadow m-2 text-gray-700">
            <div className="text-center text-2xl m-4 pt-16">
                Jeśli chcesz się z nami skontaktować możesz to zrobić za pomocą poniższego maila:
            </div>
            <div className="text-center text-xl font-thin m-2 p-4">
                <a href="/contact">
                    <FontAwesomeIcon icon="envelope-open-text"/> emil.wozniak.591986@gmail.com
                </a>
            </div>
        </div>
    );
};

export default Contact;