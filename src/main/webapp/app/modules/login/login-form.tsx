/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

import React from 'react';
import {Row} from "app/components/layout/grid/row";
import {Column} from "app/components/layout/grid/column";
import {Translate, translate} from 'react-jhipster';
import {Alert, Button, Label, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import {AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import {Link} from 'react-router-dom';


interface OwnProps {
    handleSubmit: Function;
    handleClose: Function;
    loginError: any;
}

type Props = OwnProps;

type State = Readonly<{}>;

class LoginForm extends React.Component<Props, State> {
    readonly state: State = {};

    render() {
        const input = "shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline";
        return (
            <AvForm onSubmit={this.props.handleSubmit} className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                <ModalHeader id="login-title" toggle={this.props.handleClose}>
                    <Translate contentKey="login.title">Sign in</Translate>
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Column>
                            {this.props.loginError ? (
                                <Alert color="danger">
                                    <Translate contentKey="login.messages.error.authentication">
                                        <strong>Failed to sign in!</strong> Please check your credentials and try
                                        again.
                                    </Translate>
                                </Alert>
                            ) : null}
                        </Column>
                        <Column>
                            <AvField
                                name="username"
                                label={translate('global.form.username.label')}
                                placeholder={translate('global.form.username.placeholder')}
                                required
                                className={input}
                                errorMessage="Username cannot be empty!"
                                autoFocus
                            />
                            <AvField
                                name="password"
                                type="password"
                                label={translate('login.form.password')}
                                placeholder={translate('login.form.password.placeholder')}
                                required
                                className={input}
                                errorMessage="Password cannot be empty!"
                            />
                            <AvGroup check inline>
                                <Label className="block text-gray-700 text-sm font-bold mb-2 color-picker-label">
                                    <AvInput type="checkbox" name="rememberMe"/> <Translate
                                    contentKey="login.form.rememberme">Remember me</Translate>
                                </Label>
                            </AvGroup>
                        </Column>
                    </Row>
                    <div className="mt-1">&nbsp;</div>
                    <Alert color="warning">
                        <Link to="/account/reset/request">
                            <Translate contentKey="login.password.forgot">Did you forget your password?</Translate>
                        </Link>
                    </Alert>
                    <Alert color="warning">
              <span>
                <Translate
                    contentKey="global.messages.info.register.noaccount">You don&apos;t have an account yet?</Translate>
              </span>{' '}
                        <Link to="/account/register">
                            <Translate contentKey="global.messages.info.register.link">Register a new
                                account</Translate>
                        </Link>
                    </Alert>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.props.handleClose} tabIndex="1">
                        <Translate contentKey="entity.action.cancel">Cancel</Translate>
                    </Button>{' '}
                    <Button color="primary" type="submit">
                        <Translate contentKey="login.form.button">Sign in</Translate>
                    </Button>
                </ModalFooter>
            </AvForm>

        );
    }
}

export default LoginForm;
