/*
 * Copyright (c) 2020. Emil Woźniak emil.wozniak.591986@gmail.com
 *
 * This file is part of examinator
 *
 * examinator can be copied and/or distributed without the express
 * permission of Emil Woźniak
 */

import { CurvePoint } from 'app/components/model/results.model';
import { halfOf, IntStream, isEven, isOdd } from 'app/components/util/Streams';

export interface IGraphPlotter {
  x: Array<number>;
  y: Array<number>;
  r: number;
}

type PlotterParser = {
  x1: Array<number>;
  x2: Array<number>;
  y1: Array<number>;
  y2: Array<number>;
};

type DataTuple = {
  key: number;
  value: number;
};
/**
 *
 * @param odd if true it will take data elements of odd id value
 * @param meter if true it will take {@link CurvePoint#meterRead} value of the elements or {@link CurvePoint#position} if false
 * @param data list of {@link CurvePoint}
 * @param standards values which each element will be subtracted from data each element
 * @see CurvePoint
 * @see IntStream
 */
const separate = (odd: boolean, data: Array<DataTuple>) =>
  IntStream.range(0, halfOf(data.length), new Array<number>())
    .map(i => data.filter(val => (odd ? isOdd(val.key) : isEven(val.key)))[i])
    .map((val, i) => parseFloat(val.value.toString()));

export const convert = (x: Array<number>, y: Array<number>): PlotterParser => {
  const xTuple = x.map((val, i) => ({ key: i, value: val }));
  const yTuple = y.map((val, i) => ({ key: i, value: val }));
  const x1 = separate(false, xTuple);
  const x2 = separate(true, xTuple);
  const y1 = separate(false, yTuple);
  const y2 = separate(true, yTuple);
  return { x1, x2, y1, y2 };
};

type Measure = {
  x: Array<number>;
  y: Array<number>;
  name;
  mode;
  marker;
};

/**
 * creates object pattern compatible to the Plotly data arg
 * @param x line points of the graph
 * @param y line points of the graph
 * @param drawType type of render
 * @param color line color
 * @see Plotly refs https://plot.ly/javascript/reference/
 */
const createMeasure = (x: Array<number>, y: Array<number>, color: string, drawType: string): Measure => ({
  x,
  y,
  name: 'measured',
  mode: drawType,
  marker: { color }
});

const createMiddleLine = (results: PlotterParser) => {
  const size = results.x1.length - 1;
  const x = [results.x1[0], results.x1[size]];
  const y = [(results.y1[0] + results.y2[0]) / 2, (results.y1[size] + results.y2[size]) / 2];
  return createMeasure(x, y, 'red', 'lines');
};

export const parseToPlotter = (results: PlotterParser): Array<Measure> => [
  createMeasure(results.x1, results.y1, 'blue', 'markers'),
  createMeasure(results.x2, results.y2, 'green', 'markers'),
  createMiddleLine(results)
];
