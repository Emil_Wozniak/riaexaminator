/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
import React from 'react';
import {TimelineChart} from 'ant-design-pro/lib/Charts';
import {BaseGraph} from "app/components/model/results.model";
import {convert} from "app/entities/graph/graph-plotter.components";

const chartData = [];
for (let i = 0; i < 20; i += 1) {
    chartData.push({
        x: new Date().getTime() + 1000 * 60 * 30 * i,
        y1: Math.floor(Math.random() * 100) + 1000,
        y2: Math.floor(Math.random() * 100) + 10,
    });
}

interface AntChartProps {
    data: BaseGraph
}

type Lines = {
    x, y1, y2
}

const AntChart = ({data}: AntChartProps) => {
    if (data.coordinates && data.coordinates.length > 1) {
        const x = data.coordinates.map(val => val.x);
        const y = data.coordinates.map(val => val.y);
        const parse = convert(x, y);
        const lines: Array<Lines> =
            parse.y2.map((y2, i) => ({x: parse.x1[i], y1: parse.y1[i], y2: parse.y2[i]}))
        console.warn(lines)
        return (
            <TimelineChart
                height={300}
                data={lines}
                titleMap={{y1: 'measure a', y2: 'measure b'}}
            />
        );
    } else return <div/>
};

export default AntChart;
