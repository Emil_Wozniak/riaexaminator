import React from 'react';
import {BaseGraph} from "app/components/model/results.model";
import {convert, IGraphPlotter, parseToPlotter} from "app/entities/graph/graph-plotter.components";
import Plot from 'react-plotly.js';

export function createGraph(data: BaseGraph) {
    if (data.coordinates !== undefined && data.coordinates.length > 1) {
        const x = data.coordinates.map(val => val.x);
        const y = data.coordinates.map(val => val.y);
        return <GraphPlotter x={x} y={y} r={data.r}/>
    }
}

const defaultFont = {
    family: 'Montserrat',
    color: '#555'
};

const defaultMargin = {
    l: 20,
    r: 80,
};

const xColor = '#50a3a2';
const yColor = '#50a3a2';

// noinspection SpellCheckingInspection
const axis = (color: string, title: string) => ({
    color,
    title,
    font: {
        family: defaultFont,
        size: 8
    },
    dtick: 0.2,
    tickson: "boundaries",
    automargin: true,
    showspikes: true,
    tickfont: {
        size: 10,
        family: "Arial"
    },
    ticksuffix: "ng"
});

/**
 * @link Plotly refs https://plot.ly/javascript/reference/
 * @link Plotly react https://plot.ly/javascript/error-bars/
 */
const GraphPlotter = ({x, y}: IGraphPlotter) => (
    <div className="col-12">
        <div style={{paddingBottom: '1rem'}}>
            <Plot
                data={parseToPlotter(convert(x, y))}
                layout={{
                    xaxis: axis(xColor, 'log(dose)'),
                    yaxis: axis(yColor, 'log(real zero)'),
                    font: defaultFont,
                    autosize: true,
                    margin: defaultMargin,
                    paper_bgcolor: "transparent",
                    plot_bgcolor: "transparent",
                    width: 680,
                    height: 450,
                    title: 'Curve',
                    sliders: {
                        x: 3
                    }
                }}/>
        </div>
    </div>

);

