/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */
import React from 'react';
import {Translate} from 'react-jhipster';

interface INotFound {
    contentKey: string;
}

export const NotFound = ({contentKey}: INotFound) => (
    <div className="alert alert-warning">
        <Translate contentKey={contentKey}/>
    </div>
);