import React from 'react';
import {connect} from 'react-redux';
import {RouteComponentProps} from 'react-router-dom';

import {IRootState} from 'app/components/reducers';
import {getEntity} from './examination-file.reducer';
import {Graph} from "app/entities/examination-file/file-upload.components";
import ResultTable from "app/entities/result-table/result-table";
import {defaultResults, IResults} from "app/components/model/results.model";
import AntChart from "app/entities/graph/ant-chart";

export interface IExaminationFileDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

interface ResultState {
  results: IResults
}

class ExaminationFileDetail extends React.Component<IExaminationFileDetailProps, ResultState> {
  constructor(props) {
    super(props);
    this.state = {results: defaultResults}
  }

  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.results !== nextProps.results) {
      this.setState({results: nextProps.results});
      return true;
    }
    return false;
  }


  render() {
    const {results} = this.state;
    return (
      <div className="container Montserrat">
        <div className="card">
          <Graph graph={results.graph ? results.graph : null}/>
          <ResultTable results={results}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  results: storeState.examinationFile.results
});

const mapDispatchToProps = {getEntity};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ExaminationFileDetail);
