import axios from 'axios';
import { ICrudDeleteAction, ICrudGetAllAction, ICrudPutAction, IPayload } from 'react-jhipster';
import { cleanEntity } from 'app/components/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/components/reducers/action-type.util';
import { defaultValue, IExaminationFile } from 'app/components/model/examination-file.model';
import { defaultResults, IResults } from 'app/components/model/results.model';

export const ACTION_TYPES = {
  FETCH_EXAMINATIONFILE_LIST: 'examinationFile/FETCH_EXAMINATIONFILE_LIST',
  FETCH_RESULT_LIST: 'examinationFile/FETCH_RESULT_LIST',
  FETCH_RESULT: 'examinationFile/FETCH_RESULT',
  FETCH_EXAMINATIONFILE: 'examinationFile/FETCH_EXAMINATIONFILE',
  CREATE_EXAMINATIONFILE: 'examinationFile/CREATE_EXAMINATIONFILE',
  UPDATE_EXAMINATIONFILE: 'examinationFile/UPDATE_EXAMINATIONFILE',
  DELETE_EXAMINATIONFILE: 'examinationFile/DELETE_EXAMINATIONFILE',
  SET_BLOB: 'examinationFile/SET_BLOB',
  RESET: 'examinationFile/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  resultList: [] as ReadonlyArray<IResults>,
  results: defaultResults,
  entities: [] as ReadonlyArray<IExaminationFile>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ExaminationFileState = Readonly<typeof initialState>;

// Reducer

export default (state: ExaminationFileState = initialState, action): ExaminationFileState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_EXAMINATIONFILE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RESULT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_EXAMINATIONFILE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_EXAMINATIONFILE):
    case REQUEST(ACTION_TYPES.UPDATE_EXAMINATIONFILE):
    case REQUEST(ACTION_TYPES.DELETE_EXAMINATIONFILE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_EXAMINATIONFILE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RESULT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_EXAMINATIONFILE):
    case FAILURE(ACTION_TYPES.CREATE_EXAMINATIONFILE):
    case FAILURE(ACTION_TYPES.UPDATE_EXAMINATIONFILE):
    case FAILURE(ACTION_TYPES.DELETE_EXAMINATIONFILE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_EXAMINATIONFILE_LIST):
    case SUCCESS(ACTION_TYPES.FETCH_RESULT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        resultList: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_EXAMINATIONFILE):
    case SUCCESS(ACTION_TYPES.FETCH_RESULT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
        results: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_EXAMINATIONFILE):
    case SUCCESS(ACTION_TYPES.UPDATE_EXAMINATIONFILE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
        resultList: action.payload.data,
        results: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_EXAMINATIONFILE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/files';

// Actions

export declare type ICrudGetTransformAction<T, R> = (id: string | number) => IPayload<R> | ((dispatch: any) => IPayload<R>);

export const getEntities: ICrudGetAllAction<IResults> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_RESULT_LIST,
    payload: axios.get<IResults>(requestUrl)
  };
};

export const getEntity: ICrudGetTransformAction<IExaminationFile, IResults> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_RESULT,
    payload: axios.get<IResults>(requestUrl)
  };
};

export const createEntity: (settings, file) => (dispatch) => Promise<IExaminationFile> = (settings, file) => async dispatch => {
  const url = 'api/files';
  const formData = new FormData();
  formData.append('settings', new Blob([JSON.stringify(settings)], { type: 'application/json' }));
  formData.append('file', file);
  const config = { headers: { 'content-type': undefined /* 'multipart/form-data' */ } };
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_EXAMINATIONFILE,
    payload: axios.post(url, formData, config)
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IExaminationFile> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_EXAMINATIONFILE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IExaminationFile> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_EXAMINATIONFILE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
