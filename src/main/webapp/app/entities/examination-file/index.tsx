import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/components/error/error-boundary-route';

import ExaminationFile from './examination-file';
import ExaminationFileDetail from './examination-file-detail';
import ExaminationFileUpdate from './file-upload';
import ExaminationFileDeleteDialog from './examination-file-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ExaminationFileDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ExaminationFileUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ExaminationFileUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ExaminationFileDetail} />
      <ErrorBoundaryRoute path={match.url} component={ExaminationFile} />
    </Switch>
  </>
);

export default Routes;
