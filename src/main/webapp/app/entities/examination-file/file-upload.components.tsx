import React from "react";
import {BaseGraph} from "app/components/model/results.model";
import {createGraph} from "app/entities/graph/graph-plotter";

interface IGraph {
    graph: BaseGraph;
}

export const Graph = ({graph}: IGraph) => (
    <>
        {graph ? createGraph(graph) : <div/>}
    </>
);
