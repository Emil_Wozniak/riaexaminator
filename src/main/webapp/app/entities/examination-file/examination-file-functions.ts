/*
 * Copyright (c) 2020. Emil Woźniak emil.wozniak.591986@gmail.com
 *
 * This file is part of examinator
 *
 * examinator can be copied and/or distributed without the express
 * permission of Emil Woźniak
 */
export function changePatternValue(name, value) {
  let {
    title,
    defaults,
    target,
    pattern,
    startValue,
    patternProps: { totals, nsbs, zeros, length, repeats, controlPoints }
  } = this.state.settings;

  const search = name;
  if (search === 'target') {
    target = value;
  } else if (search === 'start_value') {
    startValue = value;
  } else if (search === 'defaults') {
    defaults = value;
  } else if (search === 'pattern') {
    pattern = value;
  } else if (search === 'totals') {
    totals = Number(value);
  } else if (search === 'nsbs') {
    nsbs = Number(value);
  } else if (search === 'zeros') {
    zeros = Number(value);
  } else if (search === 'repeats') {
    repeats = Number(value);
  } else if (search === 'length') {
    length = Number(value);
  } else if (search === 'controlPoints') {
    controlPoints = Number(value);
  } else if (search === 'title') {
    title = value;
  }

  this.setState(
    state => ({
      settings: {
        title,
        target,
        defaults,
        pattern,
        startValue,
        patternProps: {
          totals,
          nsbs,
          zeros,
          repeats,
          length,
          controlPoints
        }
      }
    }),
    () => this.state
  );

  return { totals, nsbs, zeros, length, repeats, controlPoints };
}
