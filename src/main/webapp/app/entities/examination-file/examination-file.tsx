import React from 'react';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from "app/components/reducers";
import {deleteEntity, getEntities} from "app/entities/examination-file/examination-file.reducer";
import {connect} from 'react-redux';
import AntdTable from "app/components/layout/grid/antd-grid/antd-table";

export interface IExaminationFileProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

export class ExaminationFile extends React.Component <IExaminationFileProps, any> {
  constructor(props: Readonly<IExaminationFileProps>) {
    super(props);
  }

  componentDidMount(): void {
    this.props.getEntities();
  }

  render() {
    const {resultList, match} = this.props;
    return (
      <div className="container">
        <h2 id="examination-file-heading">
          <Translate contentKey="examinatorApp.examinationFile.home.title">
            Examination Files
          </Translate>
          <Link
            to={`${match.url}/new`}
            className="btn float-right"
            id="create-entity">
            <FontAwesomeIcon icon="plus"/>
            &nbsp;
            <Translate contentKey="examinatorApp.examinationFile.home.createLabel">
              Create new results
            </Translate>
          </Link>
        </h2>
        <AntdTable data={resultList.concat()} url={match.url}/>
      </div>
    );
  }
}

const mapStateToProps = ({examinationFile}: IRootState) => ({
  resultList: examinationFile.resultList,
  totalItems: examinationFile.totalItems
});

const mapDispatchToProps = {
  getEntities,
  deleteEntity
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ExaminationFile);
