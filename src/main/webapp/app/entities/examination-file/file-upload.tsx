import React, {Component} from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {IRootState} from 'app/components/reducers';
import {createEntity, reset} from './examination-file.reducer';
import {connect} from 'react-redux';
import {defaultSettings, exportSettings, IFileProps} from "app/components/model/file.props.model";
import {defaultResults, IResults} from "app/components/model/results.model";
import {AvForm} from 'availity-reactstrap-validation';
import {ButtonSet, FileInputBtn, SettingBtn, UploadBtn} from "app/components/layout/button/buttons";
import {changePatternValue} from "app/entities/examination-file/examination-file-functions";
import {translate} from 'react-jhipster';
import {Settings} from "app/components/layout/upload/settings";

export interface IExaminationFileUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
    results: IResults;
}

interface IFileUploadState {
    file?: any;
    settings?: IFileProps;
    results?: IResults;
}

export class FileUpload extends Component<IExaminationFileUpdateProps, IFileUploadState> {
    constructor(props) {
        super(props);
        this.state = {settings: defaultSettings, file: null, results: defaultResults}
    }

    onFormSubmit = (event: any) => {
        event.preventDefault();
        const settings = exportSettings(this.state.settings);
        this.fileUpload(this.state.file, settings)
            .then(id => this.props.history.push(`/examination-file/${id}`));
    };

    onChange = (event: any) => {
        this.setState({file: event.target.files[0]})
    };

    onPatternChange = (event: any) => {
        const {target: {name, value}} = event;
        changePatternValue.call(this, name, value);
    };

    onDefaultsChange = ({target}) => {
        if (target.checked) {
            target.removeAttribute('checked');
        } else {
            target.setAttribute('checked', true);
        }

        this.setState((state) => ({
            settings: {
                title: state.settings.title,
                target: state.settings.target,
                defaults: !state.settings.defaults,
                pattern: state.settings.pattern,
                startValue: state.settings.startValue,
                patternProps: state.settings.patternProps
            },
            file: state.file
        }));
    };

    /* eslint require-await: "error" */
    fileUpload = async (file: any, settings: IFileProps) => {
        await this.props.createEntity(settings, file);
        return this.props.result.id
    };

    handleClose = () => {
        this.props.history.push('/examination-file/');
    };

    componentDidMount(): void {
    }

    shouldComponentUpdate(
        nextProps: Readonly<IExaminationFileUpdateProps>,
        nextState: Readonly<IFileUploadState>,
        _: any): boolean {
        const {settings} = this.state;
        if (this.state.results !== nextProps.results) {
            this.setState({results: nextProps.results});
            return true;
        } else if (
            settings.defaults !== nextState.settings.defaults
            || settings.startValue !== nextState.settings.startValue
            || settings.target !== nextState.settings.target
            || settings.patternProps !== nextState.settings.patternProps) {
            return true;
        }
        return false;
    }

    componentWillUnmount(): void {
    }

    render() {
        const {settings} = this.state;
        return (
            <div className="container Montserrat">
                <AvForm onSubmit={this.onFormSubmit}>
                    <h1>{translate("examinatorApp.examinationFile.upload")}</h1>
                    <ButtonSet>
                        <FileInputBtn onChange={this.onChange}/>
                        <UploadBtn/>
                        <SettingBtn
                            defaults={settings.defaults}
                            onDefaultsChange={this.onDefaultsChange}/>
                    </ButtonSet>
                    <Settings
                        patternProps={this.state.settings.patternProps}
                        defaults={settings.defaults}
                        settings={this.state.settings}
                        onPatternChange={this.onPatternChange}/>
                    <br/>
                    <br/>
                </AvForm>
                {/* <div className="card">*/}
                {/*  <Graph graph={results.graph}/>*/}
                {/*  <ResultTable results={results}/>*/}
                {/* </div>*/}
            </div>
        )
    }
}

const mapStateToProps = (storeState: IRootState) => ({
    results: storeState.examinationFile.results,
    result: storeState.examinationFile.entity,
    loading: storeState.examinationFile.loading,
    updating: storeState.examinationFile.updating,
    updateSuccess: storeState.examinationFile.updateSuccess
});

const mapDispatchToProps = {
    createEntity,
    reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(FileUpload);
