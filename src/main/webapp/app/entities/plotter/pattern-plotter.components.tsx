import React from 'react';
import {IntStream} from "app/components/util/Streams";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

export function createIconArray(end: number, color: string) {
    return IntStream
        .range(0, end, [])
        .map(i => <FontAwesomeIcon
            key={`icon-totals-${i}`}
            icon="circle"
            color={color}
            style={{height: 32, width: 32}}/>);
}

interface IPlotterSegment {
    name: string;
    children: any;
}

export const PlotterSegment = ({name, children}: IPlotterSegment) => (
    <div style={{textAlign: "center"}}>
        <h5>{name}</h5>
        {children}
    </div>
);

interface IDotColumn {
    dots: Array<any>;
    name: string;
}

const DotColumn = ({dots, name}: IDotColumn) => (
    <div className={"col-1"}>
        {dots.map((element, i) => (
            <Dot key={`elements-${name}-${i}`}
                 element={element}/>)
        )}
    </div>);

interface IDotArray {
    dots: Array<any>;
    name: string;
    clone?: number;
}

const DotRow = ({dots, name, clone}: IDotArray) => (
    <div className="row pl-2">
        {IntStream
            .range(0, clone, [])
            .map(j => (
                <DotColumn
                    dots={dots}
                    name={name}
                    key={`elements-${name}-${j}`}/>
            ))}
    </div>
);

export const DotArray = ({dots, name, clone}: IDotArray) => (
    <>
        {clone
            ? <DotRow name={name} clone={clone} dots={dots}/>
            : <DotColumn dots={dots} name={name}/>
        }
    </>);

interface IDot {
    element: any;
}

const Dot = ({element}: IDot) => (
    <div>
        {element}
    </div>);
