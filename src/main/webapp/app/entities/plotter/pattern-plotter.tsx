import React from 'react';
import {IPatternProps} from "app/components/model/file.props.model";
import {DotArray, createIconArray, PlotterSegment} from "app/entities/plotter/pattern-plotter.components";

type PatternPlotterProps = { values: IPatternProps; }

type PatternPlotterState = Readonly<{ values: IPatternProps; }>;

class PatternPlotter extends React.Component<PatternPlotterProps, PatternPlotterState> {
    constructor(props: PatternPlotterProps) {
        super(props);
        this.state = {values: this.props.values}
    }

    shouldComponentUpdate(
        nextProps: Readonly<PatternPlotterProps>,
        nextState: Readonly<PatternPlotterState>,
        nextContext: any): boolean {
        const {values} = this.state;
        if (values !== nextProps.values) {
            this.setState({values: nextProps.values}, () => this.state);
            return true;
        }
        return false;
    }

    render() {
        const {values} = this.state;
        const totals = createIconArray(values.totals, "#807d7d");
        const nsbs = createIconArray(values.nsbs, "#6b7a99");
        const zeros = createIconArray(values.zeros, "#45993d");
        const repeats = createIconArray(values.repeats, "#3192d6");
        return (
            <div className="row">
                <PlotterSegment name="T">
                    <DotArray dots={totals} name="T"/>
                </PlotterSegment>
                <PlotterSegment name="N">
                    <DotArray dots={nsbs} name="N"/>
                </PlotterSegment>
                <PlotterSegment name="O">
                    <DotArray dots={zeros} name="O"/>
                </PlotterSegment>
                <PlotterSegment name="Points">
                    <DotArray dots={repeats} name="P" clone={values.length}/>
                </PlotterSegment>
            </div>
        );
    }
}

export default PatternPlotter;
