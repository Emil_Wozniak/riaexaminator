import React from 'react';
import {IResults} from "app/components/model/results.model";
import EditableTable from "app/components/layout/grid/antd-grid/editable-table";
import {sortByProbNumber} from "app/components/util/file-data-sorter";
import ExportSidebar from "app/components/layout/sidebar/export-sidebar";
import ResultInfo from "app/components/results/result-info";

export interface IResultTable {
    results: IResults;
}

const ResultTable = ({results}: IResultTable) =>
    results.resultPoints.length > 0
        ? <>
            <ExportSidebar results={results}/>
            <ResultInfo results={results}/>
            <EditableTable results={results.controlCurve.sort(sortByProbNumber())}/>
            <EditableTable samples results={results.resultPoints.sort(sortByProbNumber())}/>
        </>
        : <div/>;

export default ResultTable;
