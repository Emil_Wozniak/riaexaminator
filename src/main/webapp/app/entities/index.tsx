import React from 'react';
import {Switch} from 'react-router-dom';
import ErrorBoundaryRoute from 'app/components/error/error-boundary-route';
import ExaminationFile from './examination-file';

const Routes = ({match}) => (
  <div>
    <Switch>
      <ErrorBoundaryRoute path={`${match.url}examination-file`} component={ExaminationFile}/>
    </Switch>
  </div>
);

export default Routes;
