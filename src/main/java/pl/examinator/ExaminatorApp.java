package pl.examinator;

import calculator.Executor;
import io.github.jhipster.config.DefaultProfileUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import pl.examinator.config.ApplicationProperties;

import java.util.Arrays;
import java.util.List;

import static io.github.jhipster.config.JHipsterConstants.*;
import static pl.examinator.config.constant.LogConstantKt.*;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
@EnableConfigurationProperties({
        LiquibaseProperties.class,
        ApplicationProperties.class
})
public class ExaminatorApp implements InitializingBean {

    private final Environment env;
    private List<String> profiles;

    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        val app = new SpringApplication(ExaminatorApp.class);
        DefaultProfileUtil.addDefaultProfile(app);
        val env = (Environment) app.run(args).getEnvironment();
        StartupLogger.register(env);
        new Executor().execute();
    }

    /**
     * Initializes examinator.
     * <p>
     * Spring profiles can be configured with a program argument --spring.profiles.active=your-active-profile
     * <p>
     * You can find more information on how profiles work with JHipster on <a href="https://www.jhipster.tech/profiles/">https://www.jhipster.tech/profiles/</a>.
     */
    @Override
    public void afterPropertiesSet() {
        profiles = Arrays.asList(env.getActiveProfiles());
        if (isProfile(DEV) && isProfile(PROD)) {
            log.error(DEV_PROD_LOG);
        }
        if (isProfile(DEV) && isProfile(CLOUD)) {
            log.error(DEV_CLOUD_LOG);
        }
    }

    private boolean isProfile(String profile) {
        return profiles.contains(profile);
    }
}
