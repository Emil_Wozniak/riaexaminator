package pl.examinator.model.file;

import calculator.constants.Ansi;
import cyclops.control.Eval;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * The ExaminationFile return content of the uploaded multipart file.
 */
@Data
@Log4j2
@Builder
public class ExaminationFile implements Serializable {

    /**
     * Uploaded file.
     */
    private MultipartFile file;
    /**
     * Stores content of uploaded file.
     */
    private final Supplier<List<String>> contents =
            Eval.later(this::loadContents);
    /**
     * determines array of characters for FileReader.
     */
    private String target;
    private String patternTitle;
    private boolean defaults;

    private List<String> loadContents() {
        return Try.of(this::loadFromFile)
                .onFailure(error -> log.error(error.getMessage()))
                .getOrElse(ArrayList::new);
    }

    /**
     * @return uploaded file lines on success or empty array list on failed.
     */
    private List<String> loadFromFile() {
        String SUCCESS_MSG = "Finished with: " + Ansi.COLOR.GREEN + "{}" + Ansi.RESET + " lines";
        return Try
                .of(() -> {
                    List<String> metadata = new ArrayList<>();
                    BufferedReader bufferedReader;
                    String line;
                    InputStream inputStream = file.getInputStream();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    while ((line = bufferedReader.readLine()) != null) {
                        metadata.add(line);
                    }
                    return metadata;
                })
                .onSuccess(val -> log.info(SUCCESS_MSG, val.size()))
                .onFailure(err -> log.warn(err.getMessage()))
                .getOrElse(ArrayList::new);
    }
}
