/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

package pl.examinator.model.pattern;

import pl.examinator.model.enums.PointType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Creates points for instance of {@link HormonePattern}.
 */
public interface PointBuilder {

    /**
     * @param patternValues each return entity values
     * @return list of {@link Point} in range of
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    static List<Point> build(List<Integer> patternValues, Double startValue) {
        List<Point> points = new ArrayList<>();
        points.addAll(createPoint(patternValues.get(0), PointType.T));
        points.addAll(createPoint(patternValues.get(1), PointType.NSB));
        points.addAll(createPoint(patternValues.get(2), PointType.ZERO));
        points.addAll(createStandardPattern(patternValues, startValue));
        points.addAll(createPoint(patternValues.get(5), PointType.K));
        return points;
    }

    static List<Point> createPoint(Integer amount, PointType type) {
        return IntStream
                .range(0, amount)
                .mapToObj(point -> new Point(type.name(), 0.0))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    static List<Point> createStandardPattern(List<Integer> patternValues, Double startValue) {
        List<Double> points = populatePoints(patternValues.get(3), startValue);
        return handleDuplicates(patternValues.get(4), points)
                .stream()
                .map(point -> new Point(String.valueOf(point), point))
                .collect(Collectors.toList());
    }

    /**
     * @param length     amount of standard points
     * @param startValue the lowest value of standard pattern
     * @return list of standard hormone values
     * <p>
     * takes defined {@link HormonePattern#getLength()} and {@link HormonePattern#getStartValue()},
     * then generates each next value for standard in order that each next should be {@code 2x startValue}
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    static List<Double> populatePoints(Integer length, Double startValue) {
        AtomicInteger atomic = new AtomicInteger();
        atomic.set(1);
        return IntStream.range(0, length)
                .mapToObj(i -> i == 0
                        ? 1
                        : atomic.get() + atomic.get())
                .peek(atomic::set)
                .map(val -> val * startValue)
                .collect(Collectors.toList());
    }

    /**
     * @param repeats amount of repeats of the standard point
     * @param points  list of points
     * @return list of points which each Point will have requested amount of duplicates
     * Process will be skipped if repeats equals 1 or less
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    static List<Double> handleDuplicates(int repeats, List<Double> points) {
        return repeats > 1
                ? points.stream()
                .map(current -> duplicate(repeats, current))
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
                : points;
    }

    /**
     * @param repeats amount of repeats of the standard point
     * @param value   current duplicated value
     * @return list of same values
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    static List<Double> duplicate(int repeats, double value) {
        return IntStream
                .range(0, repeats)
                .mapToObj(i -> value)
                .collect(Collectors.toList());
    }
}
