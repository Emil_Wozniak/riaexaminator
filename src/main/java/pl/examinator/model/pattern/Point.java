package pl.examinator.model.pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor
public class Point {
    private String name;
    private Double value;
}
