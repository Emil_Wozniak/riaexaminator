package pl.examinator.service.error;


import lombok.Getter;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.HashMap;
import java.util.Map;

public class ApiStoreProblem extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    @Getter
    private final String entityName;
    @Getter
    private final String errorKey;

    public ApiStoreProblem(String title, String details, String entityName, String errorKey, Status status) {
        super(
            null,
            title,
            status,
            details,
            null,
            null,
            getAlertParameters(errorKey, entityName)
        );
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}

