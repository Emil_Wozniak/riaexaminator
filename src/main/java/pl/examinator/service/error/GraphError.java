package pl.examinator.service.error;

import lombok.Builder;
import lombok.Getter;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;
import pl.examinator.web.errors.ErrorConstants;

import java.util.HashMap;
import java.util.Map;

import static pl.examinator.service.error.ServiceErrors.GRAPH_MSG;

public class GraphError extends AbstractThrowableProblem {
    private static final long serialVersionUID = 1L;

    @Getter
    private final String entityName;
    @Getter
    private final String errorKey;

    @Builder
    public GraphError(String entityName, String errorKey) {
        super(
                null,
                GRAPH_MSG,
                Status.CONFLICT,
                ErrorConstants.READ_METER_FAIL,
                null,
                null,
                getAlertParameters(errorKey, entityName)
        );
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}
