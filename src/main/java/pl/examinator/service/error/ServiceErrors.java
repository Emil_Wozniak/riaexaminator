package pl.examinator.service.error;

public final class ServiceErrors {

    public static final String CONTROL_CURVE_MSG = "Control Curve failed";
    public static final String GRAPH_MSG = "Can't create graph";
}
