/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

package pl.examinator.service.error;

public class InvalidPasswordException extends RuntimeException {

    public InvalidPasswordException() {
        super("Incorrect password");
    }

}
