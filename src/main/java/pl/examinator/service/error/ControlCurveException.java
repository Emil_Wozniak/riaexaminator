package pl.examinator.service.error;

import lombok.Getter;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;
import pl.examinator.web.errors.ErrorConstants;

import java.util.HashMap;
import java.util.Map;

import static pl.examinator.service.error.ServiceErrors.CONTROL_CURVE_MSG;

public class ControlCurveException extends AbstractThrowableProblem  {

    private static final long serialVersionUID = 1L;

    @Getter
    private final String entityName;
    @Getter
    private final String errorKey;

    public ControlCurveException(String entityName, String errorKey) {
        super(
            null,
            CONTROL_CURVE_MSG,
            Status.CONFLICT,
            ErrorConstants.READ_METER_FAIL,
            null,
            null,
            getAlertParameters(errorKey, entityName)
        );
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}
