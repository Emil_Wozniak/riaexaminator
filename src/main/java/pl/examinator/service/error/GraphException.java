package pl.examinator.service.error;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GraphException extends Throwable {

    private String massage;

    public GraphException(String message, Double correlation, Double zeroBindingPercentage) {
        super(message);
        this.massage = getMessage(correlation, zeroBindingPercentage);
    }

    private String getMessage(Double correlation, Double zeroBindingPercentage) {
        massage = "";
        if (correlation == null) {
            massage += "Correlation is null";
            log.error(massage);
        } else if (zeroBindingPercentage == 0.0) {
            massage += "zero is 0";
            log.error(massage);
        } else {
            massage += "regressionParameterB is 0";
            log.error(massage);
        }
        return massage;
    }
}
