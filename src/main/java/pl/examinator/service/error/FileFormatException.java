package pl.examinator.service.error;

import lombok.Getter;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;
import pl.examinator.web.errors.ErrorConstants;

import java.util.HashMap;
import java.util.Map;

public class FileFormatException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    @Getter
    private final String entityName;
    @Getter
    private final String errorKey;

    public FileFormatException(String entityName, String errorKey) {
        super(
            null,
            "Unsupported type",
            Status.CONFLICT,
            ErrorConstants.NOT_SUPPORT_TYPE,
            null,
            null,
            getAlertParameters(entityName, errorKey));
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}
