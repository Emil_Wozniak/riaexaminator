package pl.examinator.service;

import io.github.jhipster.config.JHipsterProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import pl.examinator.model.User;

import javax.mail.MessagingException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Locale.forLanguageTag;
import static pl.examinator.config.Constants.SEND_MSG;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MailService {


    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties properties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug(SEND_MSG, isMultipart, isHtml, to, subject, content);
        val mimeMessage = javaMailSender.createMimeMessage();
        try {
            new MimeMessageHelper(mimeMessage, isMultipart, UTF_8.name()) {{
                setTo(to);
                setFrom(properties.getMail().getFrom());
                setSubject(subject);
                setText(content, isHtml);
            }};
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (MailException | MessagingException e) {
            log.warn("Email could not be sent to user '{}'", to, e);
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        if (user.getEmail() == null) {
            log.debug("Email doesn't exist for user '{}'", user.getLogin());
            return;
        }
        val locale = forLanguageTag(user.getLangKey());
        val context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, properties.getMail().getBaseUrl());
        val content = templateEngine.process(templateName, context);
        val subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }
}
