package pl.examinator.service.util.proxy

import calculator.model.results.ExaminationPoint
import calculator.model.results.ExaminationResult
import java.nio.charset.StandardCharsets.UTF_8
import java.util.*
import java.util.stream.Stream
import kotlin.streams.toList

open class ResultProxy : DatabaseProxy {
    protected open fun <T : ExaminationResult?> stringify(results: List<T>): String =
        results
            .asSequence()
            .map { point: T -> point as ExaminationPoint }
            .map { (average, identifier, pattern, probeNumber, position, cpm, flagged, ng) ->
                "$identifier;$pattern;$probeNumber;$cpm;$position;$flagged;$ng;$average"
            }
            .joinToString(separator = "\n", prefix = "", postfix = "")

    fun parseToPoints(data: ByteArray): List<ExaminationPoint> = Arrays
        .stream(String(data, UTF_8).split("\n").toTypedArray())
        .flatMap { Stream.of(listOf(*it.split(";").toTypedArray())) }
        .map {
            val identifier = it[0]
            val pattern = it[1]
            val probeNumber = it[2].toInt()
            val cpm = it[3].toInt()
            val position = it[4]
            val flagged = it[5].toBoolean()
            val ng = it[6]
            val average = it[7].toDouble()
            ExaminationPoint(
                average,        // var meterRead: Double,
                identifier,     // identifier: String,
                pattern,        //  pattern: String,
                probeNumber,    //  probeNumber: Int,
                position,       //  cpm: Int,
                cpm,            //  position: String,
                flagged,        // flagged: Boolean?,
                ng              // ng: String
            )
        }
        .toList()

    override fun <T : ExaminationResult?> convertToStore(results: List<T>): String =
        stringify(results)
}