package pl.examinator.service.util.proxy

import calculator.model.results.ExaminationResult

interface DatabaseProxy {
    fun <T : ExaminationResult?> convertToStore(results: List<T>): String
}