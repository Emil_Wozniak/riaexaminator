package pl.examinator.service.util.factory;

import calculator.FileAnalyzer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ModuleFactory {

    @Bean
    public FileAnalyzer fileAnalyzer() {
        return new FileAnalyzer();
    }
}
