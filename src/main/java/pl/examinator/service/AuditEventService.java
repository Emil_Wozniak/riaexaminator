package pl.examinator.service;

import io.github.jhipster.config.JHipsterProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.examinator.config.audit.AuditEventConverter;
import pl.examinator.repo.PersistenceAuditEventRepository;

import java.time.Instant;
import java.util.Optional;

import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Service for managing audit events.
 * <p>
 * This is the default implementation to support SpringBoot Actuator {@code AuditEventRepository}.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AuditEventService {

    private final JHipsterProperties properties;

    private final PersistenceAuditEventRepository repository;

    private final AuditEventConverter converter;

    /**
     * Old audit events should be automatically deleted after 30 days.
     * <p>
     * This is scheduled to get fired at 12:00 (am).
     */
    @Scheduled(cron = "0 0 12 * * ?")
    public void removeOldAuditEvents() {
        repository
                .findByAuditEventDateBefore(now().minus(properties.getAuditEvents().getRetentionPeriod(), DAYS))
                .forEach(auditEvent -> {
                    log.debug("Deleting audit data {}", auditEvent);
                    repository.delete(auditEvent);
                });
    }

    public Page<AuditEvent> findAll(Pageable pageable) {
        return repository
                .findAll(pageable)
                .map(converter::convertToAuditEvent);
    }

    public Page<AuditEvent> findByDates(Instant fromDate, Instant toDate, Pageable pageable) {
        return repository
                .findAllByAuditEventDateBetween(fromDate, toDate, pageable)
                .map(converter::convertToAuditEvent);
    }

    public Optional<AuditEvent> find(Long id) {
        return repository
                .findById(id)
                .map(converter::convertToAuditEvent);
    }
}
