package pl.examinator.service.mapper;

public interface ApiMapper<T, D> {
    D convertToDto(T entity);

    T convertToEntity(D entity);
}

