package pl.examinator.service.mapper;

import lombok.val;
import org.springframework.stereotype.Service;
import pl.examinator.model.Authority;
import pl.examinator.model.User;
import pl.examinator.service.dto.UserDTO;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * Mapper for the entity {@link User} and its DTO called {@link UserDTO}.
 * <p>
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class UserMapper {

    public List<UserDTO> usersToUserDTOs(List<User> users) {
        return users
                .stream()
                .filter(Objects::nonNull)
                .map(this::userToUserDTO)
                .collect(toList());
    }

    public UserDTO userToUserDTO(User user) {
        return new UserDTO(user);
    }

    public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
        return userDTOs
                .stream()
                .filter(Objects::nonNull)
                .map(this::userDTOToUser)
                .collect(toList());
    }

    public User userDTOToUser(UserDTO dto) {
        val authorities = dto != null ? getAuthorities(dto) : null;
        return dto == null
                ? null
                : new User() {{
            setId(dto.getId());
            setLogin(dto.getLogin());
            setFirstName(dto.getFirstName());
            setLastName(dto.getLastName());
            setEmail(dto.getEmail());
            setImageUrl(dto.getImageUrl());
            setActivated(dto.isActivated());
            setLangKey(dto.getLangKey());
            setAuthorities(authorities);
        }};
    }

    private Set<Authority> getAuthorities(UserDTO dto) {
        return this.authoritiesFromStrings(dto.getAuthorities());
    }

    private Set<Authority> authoritiesFromStrings(Set<String> authoritiesAsString) {
        return authoritiesAsString != null
                ? authoritiesAsString
                .stream()
                .map(string -> new Authority() {{
                    setName(string);
                }})
                .collect(toSet())
                : new HashSet<>();
    }

    public User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
