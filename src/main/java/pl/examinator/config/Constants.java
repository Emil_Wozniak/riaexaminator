package pl.examinator.config;

import lombok.NoArgsConstructor;

/**
 * Application constants.
 */
@NoArgsConstructor
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";
    
    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "pl";
    @SuppressWarnings("SpellCheckingInspection")
    public static final String ANONYMOUS_USER = "anonymoususer";

    public static String NEWLINE_INDENT = "\n\t";
    public static String STARTUP_LINE = "----------------------------------------------------------";
    public static String LOCALHOST_ERROR_MSG = "The host name could not be determined, using `localhost` as fallback";

    public static final String MASTER_XML = "classpath:config/liquibase/master.xml";

    public static final String SEND_MSG = "Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}";
}
