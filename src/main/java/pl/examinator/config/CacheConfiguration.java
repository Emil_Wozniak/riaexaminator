package pl.examinator.config;

import io.github.jhipster.config.JHipsterProperties;
import lombok.val;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.CacheManager;

import static java.time.Duration.ofSeconds;
import static org.ehcache.config.builders.CacheConfigurationBuilder.newCacheConfigurationBuilder;
import static org.ehcache.config.builders.ExpiryPolicyBuilder.timeToLiveExpiration;
import static org.ehcache.config.builders.ResourcePoolsBuilder.heap;
import static pl.examinator.repo.UserRepository.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> configuration;

    public CacheConfiguration(JHipsterProperties properties) {
        val ehcache = properties.getCache().getEhcache();

        configuration = Eh107Configuration.fromEhcacheCacheConfiguration(
            newCacheConfigurationBuilder(Object.class, Object.class,heap(ehcache.getMaxEntries()))
                .withExpiry(timeToLiveExpiration(ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, USERS_BY_LOGIN_CACHE);
            createCache(cm, USERS_BY_EMAIL_CACHE);
        };
    }

    private void createCache(CacheManager cm, String cacheName) {
        val cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        else cm.createCache(cacheName, configuration);
    }
}
