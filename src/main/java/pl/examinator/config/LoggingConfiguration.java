package pl.examinator.config;

import ch.qos.logback.classic.LoggerContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.config.JHipsterProperties;
import lombok.val;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static io.github.jhipster.config.JHipsterProperties.*;
import static io.github.jhipster.config.logging.LoggingUtils.*;

/*
 * Configures the console and Logstash log appender from the app properties
 */
@Configuration
public class LoggingConfiguration {

    public LoggingConfiguration(
            @Value("${spring.application.name}") String appName,
            @Value("${server.port}") String serverPort,
            JHipsterProperties properties,
            ObjectMapper mapper) throws JsonProcessingException {

        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

        val map = new HashMap<>();
        map.put("app_name", appName);
        map.put("app_port", serverPort);
        val customFields = mapper.writeValueAsString(map);

        val loggingProperties = properties.getLogging();
        val logstashProperties = loggingProperties.getLogstash();

        if (loggingProperties.isUseJsonFormat()) {
            addJsonConsoleAppender(context, customFields);
        }
        if (logstashProperties.isEnabled()) {
            addLogstashTcpSocketAppender(context, customFields, logstashProperties);
        }
        if (loggingProperties.isUseJsonFormat() || logstashProperties.isEnabled()) {
            addContextListener(context, customFields, loggingProperties);
        }
        if (properties.getMetrics().getLogs().isEnabled()) {
            setMetricsMarkerLogbackFilter(context, loggingProperties.isUseJsonFormat());
        }
    }
}
