package pl.examinator.config;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.web.filter.CachingHttpHeadersFilter;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.EnumSet;

import static io.github.jhipster.config.JHipsterConstants.SPRING_PROFILE_PRODUCTION;
import static java.net.URLDecoder.decode;
import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.servlet.DispatcherType.*;
import static org.springframework.boot.web.server.MimeMappings.DEFAULT;
import static org.springframework.core.env.Profiles.of;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
@Value
@Log4j2
@NonFinal
@Configuration
@RequiredArgsConstructor
public class WebConfigurer implements
        ServletContextInitializer,
        WebServerFactoryCustomizer<WebServerFactory> {

    Environment env;
    JHipsterProperties properties;

    @Override
    public void onStartup(ServletContext servletContext) {
        if (env.getActiveProfiles().length != 0) {
            log.info("Web application configuration, using profiles: {}", (Object[]) env.getActiveProfiles());
        }
        val dispose = EnumSet.of(REQUEST, FORWARD, ASYNC);
        if (env.acceptsProfiles(of(SPRING_PROFILE_PRODUCTION))) {
            initCachingHttpHeadersFilter(servletContext, dispose);
        }
        log.info("Web application fully configured");
    }

    /**
     * Customize the Servlet engine: Mime types, the document root, the cache.
     */
    @Override
    public void customize(WebServerFactory server) {
        setMimeMappings(server);
        // When running in an IDE or with ./gradlew bootRun, set location of the static web assets.
        setLocationForStaticAssets(server);
    }

    private void setMimeMappings(WebServerFactory server) {
        if (server instanceof ConfigurableServletWebServerFactory) {
            val mappings = new MimeMappings(DEFAULT);
            mappings.add("html", TEXT_HTML_VALUE + ";charset=" + UTF_8.name().toLowerCase());
            mappings.add("json", TEXT_HTML_VALUE + ";charset=" + UTF_8.name().toLowerCase());
            val servletWebServer = (ConfigurableServletWebServerFactory) server;
            servletWebServer.setMimeMappings(mappings);
        }
    }

    private void setLocationForStaticAssets(WebServerFactory server) {
        if (server instanceof ConfigurableServletWebServerFactory) {
            ConfigurableServletWebServerFactory servletWebServer = (ConfigurableServletWebServerFactory) server;
            File root;
            val prefixPath = resolvePathPrefix();
            root = new File(prefixPath + "build/resources/main/static/");
            if (root.exists() && root.isDirectory()) {
                servletWebServer.setDocumentRoot(root);
            }
        }
    }

    /**
     * Resolve path prefix to static resources.
     */
    private String resolvePathPrefix() {
        val fullExecutablePath = Try.of(() -> decode(this.getClass().getResource("").getPath(), UTF_8.name()))
                .recover(UnsupportedEncodingException.class, error -> this.getClass().getResource("").getPath())
                .get();
        val rootPath = Paths.get(".").toUri().normalize().getPath();
        val extractedPath = fullExecutablePath.replace(rootPath, "");
        int extractionEndIndex = extractedPath.indexOf("build/");
        if (extractionEndIndex <= 0) {
            return "";
        }
        return extractedPath.substring(0, extractionEndIndex);
    }

    /**
     * Initializes the caching HTTP Headers Filter.
     */
    private void initCachingHttpHeadersFilter(
            ServletContext servletContext,
            EnumSet<DispatcherType> dispose) {
        log.debug("Registering Caching HTTP Headers Filter");
        val registration = servletContext.addFilter(
                "cachingHttpHeadersFilter",
                new CachingHttpHeadersFilter(properties));

        registration.addMappingForUrlPatterns(dispose, true, "/i18n/*");
        registration.addMappingForUrlPatterns(dispose, true, "/content/*");
        registration.addMappingForUrlPatterns(dispose, true, "/app/*");
        registration.setAsyncSupported(true);
    }

    @Bean
    public CorsFilter corsFilter() {
        return Try
                .of(properties::getCors)
                .map(config -> config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()
                        ? new UrlBasedCorsConfigurationSource() {{
                    registerCorsConfiguration("/api/**", config);
                    registerCorsConfiguration("/routes/**", config);
                    registerCorsConfiguration("/management/**", config);
                    registerCorsConfiguration("/v2/api-docs", config);
                }} : new UrlBasedCorsConfigurationSource())
                .map(CorsFilter::new)
                .get();
    }
}
