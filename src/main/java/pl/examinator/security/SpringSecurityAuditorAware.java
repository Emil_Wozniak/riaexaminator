package pl.examinator.security;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static pl.examinator.config.Constants.SYSTEM_ACCOUNT;
import static pl.examinator.security.SecurityUtils.getCurrentUserLogin;

/**
 * Implementation of {@link AuditorAware} based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public @NotNull Optional<String> getCurrentAuditor() {
        return Optional
                .of(getCurrentUserLogin()
                        .orElse(SYSTEM_ACCOUNT));
    }
}
