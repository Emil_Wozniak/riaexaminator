/*
 * Copyright (c) 2020.
 * Emil Woźniak emil.wozniak.591986@gmail.com.
 *  This file is part of examinator
 *  examinator can be copied and/or distributed
 *  without the express permission of Emil Woźniak
 *
 */

package pl.examinator.web.user;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import pl.examinator.annotations.RestApi;
import pl.examinator.security.jwt.JWTFilter;
import pl.examinator.security.jwt.TokenProvider;
import pl.examinator.web.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static lombok.AccessLevel.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.security.core.context.SecurityContextHolder.*;
import static pl.examinator.security.jwt.JWTFilter.*;

/**
 * Controller to authenticate users.
 */
@RestApi
@RequiredArgsConstructor
public class UserJWTController {

    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {
        val authenticationToken = getAuthenticationToken(loginVM);
        val authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        getContext().setAuthentication(authentication);
        val rememberMe = loginVM.isRememberMe() != null && loginVM.isRememberMe();
        val jwt = tokenProvider.createToken(authentication, rememberMe);
        val httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, OK);
    }

    @NotNull
    private UsernamePasswordAuthenticationToken getAuthenticationToken(LoginVM loginVM) {
        return new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());
    }

    /**
     * Object to return as body in JWT Authentication.
     */

    @Setter(PACKAGE)
    @Getter(PACKAGE)
    @RequiredArgsConstructor
    static class JWTToken {

        @JsonProperty("id_token")
        private final String idToken;

    }
}
