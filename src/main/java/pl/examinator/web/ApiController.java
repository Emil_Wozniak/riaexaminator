package pl.examinator.web;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @param <T> entity type class
 */
@SuppressWarnings("MVCPathVariableInspection")
public interface ApiController<T> {

    List<T> findAll();

    ResponseEntity<T> getById(@PathVariable Long id);

    ResponseEntity<Integer> update(@RequestBody T entity, @PathVariable Long id);

    ResponseEntity<String> delete(@PathVariable String id);
}
