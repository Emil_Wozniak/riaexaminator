/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.examinator.web.vm;
