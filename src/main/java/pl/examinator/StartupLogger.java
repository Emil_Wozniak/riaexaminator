/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pl.examinator;

import io.vavr.control.Try;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import static java.net.InetAddress.getLocalHost;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static pl.examinator.config.Constants.*;

public interface StartupLogger {

    Logger log = LoggerFactory.getLogger(StartupLogger.class.getName());

    static void register(@NotNull final Environment env) {
        var protocol = env.getProperty("server.ssl.key-store") != null ? "https" : "http";
        var serverPort = env.getProperty("server.port");
        var property = env.getProperty("server.servlet.context-path");
        var contextPath = isBlank(property) ? "/" : property;
        var hostAddress = Try.of(() -> getLocalHost().getHostAddress())
                .onFailure(error -> log.warn(LOCALHOST_ERROR_MSG))
                .getOrElse("localhost");
        log.info("\n" +
                        STARTUP_LINE +
                        NEWLINE_INDENT + "Application '{}' is running! Access URLs:" +
                        NEWLINE_INDENT + "Local: \t\t{}://localhost:{}{}" + "" +
                        NEWLINE_INDENT + "External: \t{}://{}:{}{}" +
                        NEWLINE_INDENT + "Profile(s): \t{}\n" +
                        STARTUP_LINE,
                env.getProperty("spring.application.name"),
                protocol,
                serverPort,
                contextPath,
                protocol,
                hostAddress,
                serverPort,
                contextPath,
                env.getActiveProfiles());
    }
}
