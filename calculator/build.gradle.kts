#!/usr/local/products/kotlin/current/bin/kotlinc -jvm-target 1.8 -cp /usr/local/products/jetty/jetty-current.jar -script

@file:Suppress("LocalVariableName", "SpellCheckingInspection")

import org.gradle.api.JavaVersion.*

//plugins {
//    java
//    groovy
//    kotlin("jvm") version "1.3.72"
//}

group = "pl.examinator"
version = "1.4.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.zalando:problem-spring-web")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    implementation("org.springframework:spring-web:5.2.7.RELEASE")

}

configure<JavaPluginConvention> {
    sourceCompatibility = VERSION_11
    targetCompatibility = VERSION_11
}

tasks {

    compileKotlin {
        sourceCompatibility = VERSION_11.toString()
        targetCompatibility = VERSION_11.toString()
        kotlinOptions.jvmTarget = VERSION_11.toString()
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "11"
    }

    test {
        useJUnitPlatform()
    }
}

