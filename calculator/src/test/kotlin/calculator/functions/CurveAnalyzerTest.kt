package calculator.functions

import calculator.BaseCounter
import calculator.Counter
import calculator.constants.controlCurve
import calculator.functions.extractor.TxtDataExtractor
import calculator.model.hormone.CortisolPattern
import calculator.model.hormone.HormonePattern
import calculator.model.results.CurvePoint
import calculator.util.readFile
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class CurveAnalyzerTest : CurveAnalyzer(mutableListOf(), mutableListOf(), TxtDataExtractor()) {

    private lateinit var lines: MutableList<String>
    private lateinit var allLines: MutableList<String>
    private lateinit var pattern: HormonePattern
    private lateinit var counter: Counter
    private lateinit var curve: List<CurvePoint>
    private lateinit var top: List<String>

    @BeforeEach
    fun setUp() {
        lines = mutableListOf()
        top = listOf("A16_244.txt", "KORTYZOL_5_MIN" )
        lines.addAll(top)
        allLines = readFile("../samples/A16_244.txt")
        allLines.forEach { lines.add(it) }
        pattern = CortisolPattern().create()
        counter = BaseCounter(this.pattern)
        curve = analyze(lines.toMutableList(), counter, pattern)
    }

    @Test
    fun `analyze should return non empty list`() {
        assertTrue(curve.isNotEmpty())
    }

    @Test
    fun `analyze should return instances of CurvePoint`() {
        assertEquals(controlCurve, curve)
    }
}
