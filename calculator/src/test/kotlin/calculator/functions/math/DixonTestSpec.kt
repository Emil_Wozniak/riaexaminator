/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package calculator.functions.math

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class DixonTestSpec {

    private lateinit var dixonTest: DixonTest
    private lateinit var outliers: List<Double>

    @BeforeEach
    fun setUp() {
        dixonTest = DixonTest()
    }

    @Test
    fun `eliminate outliers should remove the higher value`() {
        outliers = dixonTest.eliminateOutliers(listOf(20.0, 9.7, 10.5))
        assertEquals(listOf(9.7, 10.5), outliers)
    }

    @Test
    fun `eliminate outliers should remove the lower value`() {
        outliers = dixonTest.eliminateOutliers(listOf(20.5, 19.5, 6.0))
        assertEquals(listOf(20.5, 19.5), outliers)
    }

    @Test
    fun `eliminate outliers should always return 2 values`() {
        outliers = dixonTest.eliminateOutliers(listOf(20.5, 19.5, 19.8))
        assertEquals(listOf(19.5, 19.8), outliers)
    }

    @Test
    fun `eliminate outliers should always return 3 values if all the same`() {
        outliers = dixonTest.eliminateOutliers(listOf(19.5, 19.5, 19.5))
        assertEquals(listOf(19.5, 19.5, 19.5), outliers)
    }
}