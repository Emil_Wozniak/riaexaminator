package calculator.functions.analyze

import calculator.BaseCounter
import calculator.Counter
import calculator.constants.controlCurve
import calculator.constants.examinationPoints
import calculator.functions.extractor.TxtDataExtractor
import calculator.model.hormone.CortisolPattern
import calculator.model.hormone.HormonePattern
import calculator.model.results.ExaminationPoint
import calculator.util.readFile
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class PointsAnalyzerTest : PointsAnalyzer(controlCurve.toMutableList(), TxtDataExtractor(), true) {

    private lateinit var lines: MutableList<String>
    private lateinit var allLines: MutableList<String>
    private lateinit var pattern: HormonePattern
    private lateinit var counter: Counter
    private lateinit var points: List<ExaminationPoint>
    private lateinit var top: List<String>

    @BeforeEach
    fun setUp() {
        lines = mutableListOf()
        top = listOf("A16_244.txt", "KORTYZOL_5_MIN")
        lines.addAll(top)
        allLines = readFile("../samples/A16_244.txt")
        allLines.forEach { lines.add(it) }
        pattern = CortisolPattern().create()
        counter = BaseCounter(this.pattern)
        points = analyze(lines.toMutableList(), counter, pattern)
    }

    @Test
    fun `analyze correct data should return 362 instances`() {
        assertEquals(362, points.size)
    }

    @Test
    fun `analyze correct data should return list of examination points instances`() {
        assertEquals(examinationPoints, points)
    }
}
