package calculator

import calculator.constants.*
import calculator.model.CalibrationCurveValues
import calculator.model.results.CurvePoint
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class CounterKtTest : BaseCounter(null) {

    var correlationResult: Double = 0.0
    var curve: List<CurvePoint>? = null
    var successDimensions: CalibrationCurveValues? = null
    var bindingList: List<Double>? = null
    private var realZeroResult: List<Double>? = null
    var correctZeroBind: Double? = null

    @BeforeEach
    fun setUp() {
        curve = controlCurve
        successDimensions = CalibrationCurveValues(
            total = 1979,
            nsb = 34,
            zero = 458,
            binding = 424,
            standardStart = 8,
            standardEnd = 22)
    }

    @AfterEach
    fun tearDown() {
        correlationResult = 0.0
        curve = null
        successDimensions = null
        bindingList = null
        realZeroResult = null
        correctZeroBind = null
    }

    @Test
    fun `calculateDimensions should return correct instance`() {
        val dimensions = calculateCalibrationCurve(examplePattern, controlCurve)
        assertEquals(successDimensions, dimensions)
    }

    @Test
    fun `extractStandard should return 14 elements`() {
        val standardList = extractStandard(curve!!, successDimensions!!)
        assertEquals(14, standardList.size)
    }

    @Test
    fun `countDose should return list of positive numbers`() {
        val doseResult = countDose(successDimensions!!, examplePattern, mutableListOf())
        assertEquals(exampleDose, doseResult)
    }

    @Test
    fun `binding should return list of positive doubles`() {
        bindingList = binding(standardsCPM, successDimensions!!, mutableListOf())
        assertEquals(bindingPercent, bindingList)
    }

    @Test
    fun `binding should return 14 elements`() {
        bindingList = binding(standardsCPM, successDimensions!!, mutableListOf())
        assertEquals(14, bindingList!!.size)
    }

    @Test
    fun `countRealZero should return 14 double values`() {
        realZeroResult = countRealZero(bindingPercent, mutableListOf())
        assertEquals(exampleRealZero, realZeroResult)
    }

    @Test
    fun `zeroBind should return double value `() {
        val correctZeroBind = zeroBind(successDimensions!!)
        assertEquals(exampleZeroBinding, correctZeroBind)
    }

    @Test
    fun `paramB should return negative number`() {
        val paramBResult = countB(exampleRealZero, exampleDose, mutableListOf())
        assertEquals(exampleParamB, paramBResult)
    }

    @Test
    fun `paramA should return positive number`() {
        val paramAResult = countA(exampleRealZero, exampleDose, exampleParamB, mutableListOf())
        assertEquals(exampleParamA, paramAResult)
    }

    @Test
    fun `correlation should be positive`() {
        correlationResult = findCorrelation(examplePattern, exampleDose, exampleRealZero)
        assertEquals(exampleCorrelation, correlationResult)
    }
}
