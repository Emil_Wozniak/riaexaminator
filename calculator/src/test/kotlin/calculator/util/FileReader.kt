package calculator.util

import calculator.constants.DEFAULT_TARGET_POINT
import io.vavr.control.Try
import java.nio.file.Files
import java.nio.file.Path

fun readFile(filename: String, all: Boolean = false): MutableList<String> {
    val path: Path = Path.of(filename)
    return Try.of { Files.readAllLines(path) }
        .map { lines ->
            lines.filter { line ->
                    if (all) line != ""
                    else line.contains(DEFAULT_TARGET_POINT)
            }.toMutableList()
        }
        .get()
}
