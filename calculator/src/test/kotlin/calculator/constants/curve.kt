package calculator.constants

import calculator.model.results.CurvePoint

val controlCurve = listOf(
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 0, position = "T", cpm = 1976, flagged = false, ng = ""),
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 1, position = "T", cpm = 1982, flagged = false, ng = ""),
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 2, position = "NSB", cpm = 49, flagged = true, ng = ""),
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 3, position = "NSB", cpm = 32, flagged = false, ng = ""),
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 4, position = "NSB", cpm = 36, flagged = false, ng = ""),
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 5, position = "ZERO", cpm = 458, flagged = false, ng = ""),
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 6, position = "ZERO", cpm = 459, flagged = false, ng = ""),
    CurvePoint(meterRead = 0.0, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 7, position = "ZERO", cpm = 447, flagged = true, ng = ""),
    CurvePoint(meterRead = 1.16, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 8, position = "1.25", cpm = 412, flagged = false, ng = ""),
    CurvePoint(meterRead = 1.79, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 9, position = "1.25", cpm = 390, flagged = false, ng = ""),
    CurvePoint(meterRead = 2.17, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 10, position = "2.5", cpm = 378, flagged = false, ng = ""),
    CurvePoint(meterRead = 3.07, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 11, position = "2.5", cpm = 352, flagged = false, ng = ""),
    CurvePoint(meterRead = 4.28, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 12, position = "5.0", cpm = 322, flagged = false, ng = ""),
    CurvePoint(meterRead = 4.56, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 13, position = "5.0", cpm = 316, flagged = false, ng = ""),
    CurvePoint(meterRead = 10.67, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 14, position = "10.0", cpm = 225, flagged = false, ng = ""),
    CurvePoint(meterRead = 8.58, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 15, position = "10.0", cpm = 249, flagged = false, ng = ""),
    CurvePoint(meterRead = 19.1, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 16, position = "20.0", cpm = 165, flagged = false, ng = ""),
    CurvePoint(meterRead = 17.39, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 17, position = "20.0", cpm = 174, flagged = false, ng = ""),
    CurvePoint(meterRead = 43.27, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 18, position = "40.0", cpm = 102, flagged = false, ng = ""),
    CurvePoint(meterRead = 34.77, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 19, position = "40.0", cpm = 116, flagged = false, ng = ""),
    CurvePoint(meterRead = 77.5, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 20, position = "80.0", cpm = 74, flagged = false, ng = ""),
    CurvePoint(meterRead = 101.24, identifier = "A16_244.txt", pattern = "KORTYZOL_5_MIN", probeNumber = 21, position = "80.0", cpm = 65, flagged = false, ng = "")
)

