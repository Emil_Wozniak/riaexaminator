package calculator.constants//@file:Suppress("SpellCheckingInspection")
//
//package calculator.constants
//
//import calculator.model.results.ExaminationPoint
//
//val examinationPoints: List<ExaminationPoint> = listOf(
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 22,
//        position = "22",
//        cpm = 512,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 23,
//        position = "23",
//        cpm = 536,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 24,
//        position = "24",
//        cpm = 219,
//        flagged = false,
//        ng = "11.27"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 25,
//        position = "25",
//        cpm = 224,
//        flagged = false,
//        ng = "10.77"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 26,
//        position = "26",
//        cpm = 164,
//        flagged = false,
//        ng = "19.31"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 27,
//        position = "27",
//        cpm = 191,
//        flagged = false,
//        ng = "14.68"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 28,
//        position = "28",
//        cpm = 226,
//        flagged = false,
//        ng = "10.57"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 29,
//        position = "29",
//        cpm = 230,
//        flagged = false,
//        ng = "10.19"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 30,
//        position = "30",
//        cpm = 293,
//        flagged = false,
//        ng = "5.71"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 31,
//        position = "31",
//        cpm = 279,
//        flagged = false,
//        ng = "6.52"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 32,
//        position = "32",
//        cpm = 325,
//        flagged = false,
//        ng = "4.15"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 33,
//        position = "33",
//        cpm = 347,
//        flagged = false,
//        ng = "3.25"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 34,
//        position = "34",
//        cpm = 386,
//        flagged = false,
//        ng = "1.92"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 35,
//        position = "35",
//        cpm = 388,
//        flagged = false,
//        ng = "1.86"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 36,
//        position = "36",
//        cpm = 372,
//        flagged = false,
//        ng = "2.36"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 37,
//        position = "37",
//        cpm = 379,
//        flagged = false,
//        ng = "2.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 38,
//        position = "38",
//        cpm = 293,
//        flagged = false,
//        ng = "5.71"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 39,
//        position = "39",
//        cpm = 330,
//        flagged = false,
//        ng = "3.94"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 40,
//        position = "40",
//        cpm = 252,
//        flagged = false,
//        ng = "8.35"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 41,
//        position = "41",
//        cpm = 237,
//        flagged = false,
//        ng = "9.56"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 42,
//        position = "42",
//        cpm = 225,
//        flagged = false,
//        ng = "10.67"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 43,
//        position = "43",
//        cpm = 247,
//        flagged = false,
//        ng = "8.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 44,
//        position = "44",
//        cpm = 211,
//        flagged = false,
//        ng = "12.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 45,
//        position = "45",
//        cpm = 220,
//        flagged = false,
//        ng = "11.17"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 46,
//        position = "46",
//        cpm = 313,
//        flagged = false,
//        ng = "4.7"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 47,
//        position = "47",
//        cpm = 295,
//        flagged = false,
//        ng = "5.6"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 48,
//        position = "48",
//        cpm = 338,
//        flagged = false,
//        ng = "3.61"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 49,
//        position = "49",
//        cpm = 341,
//        flagged = false,
//        ng = "3.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 50,
//        position = "50",
//        cpm = 335,
//        flagged = false,
//        ng = "3.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 51,
//        position = "51",
//        cpm = 362,
//        flagged = false,
//        ng = "2.7"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 52,
//        position = "52",
//        cpm = 368,
//        flagged = false,
//        ng = "2.5"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 53,
//        position = "53",
//        cpm = 392,
//        flagged = false,
//        ng = "1.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 54,
//        position = "54",
//        cpm = 402,
//        flagged = false,
//        ng = "1.44"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 55,
//        position = "55",
//        cpm = 402,
//        flagged = false,
//        ng = "1.44"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 56,
//        position = "56",
//        cpm = 237,
//        flagged = false,
//        ng = "9.56"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 57,
//        position = "57",
//        cpm = 220,
//        flagged = false,
//        ng = "11.17"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 58,
//        position = "58",
//        cpm = 264,
//        flagged = false,
//        ng = "7.48"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 59,
//        position = "59",
//        cpm = 295,
//        flagged = false,
//        ng = "5.6"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 60,
//        position = "60",
//        cpm = 359,
//        flagged = false,
//        ng = "2.81"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 61,
//        position = "61",
//        cpm = 352,
//        flagged = false,
//        ng = "3.07"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 62,
//        position = "62",
//        cpm = 400,
//        flagged = false,
//        ng = "1.5"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 63,
//        position = "63",
//        cpm = 403,
//        flagged = false,
//        ng = "1.41"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 64,
//        position = "64",
//        cpm = 411,
//        flagged = false,
//        ng = "1.19"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 65,
//        position = "65",
//        cpm = 437,
//        flagged = false,
//        ng = "0.51"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 66,
//        position = "66",
//        cpm = 477,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 67,
//        position = "67",
//        cpm = 469,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 68,
//        position = "68",
//        cpm = 454,
//        flagged = true,
//        ng = "0.1"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 69,
//        position = "69",
//        cpm = 474,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 70,
//        position = "70",
//        cpm = 483,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 71,
//        position = "71",
//        cpm = 450,
//        flagged = true,
//        ng = "0.2"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 72,
//        position = "72",
//        cpm = 412,
//        flagged = false,
//        ng = "1.16"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 73,
//        position = "73",
//        cpm = 440,
//        flagged = false,
//        ng = "0.44"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 74,
//        position = "74",
//        cpm = 262,
//        flagged = false,
//        ng = "7.62"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 75,
//        position = "75",
//        cpm = 201,
//        flagged = false,
//        ng = "13.34"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 76,
//        position = "76",
//        cpm = 250,
//        flagged = false,
//        ng = "8.5"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 77,
//        position = "77",
//        cpm = 242,
//        flagged = false,
//        ng = "9.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 78,
//        position = "78",
//        cpm = 227,
//        flagged = false,
//        ng = "10.47"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 79,
//        position = "79",
//        cpm = 270,
//        flagged = false,
//        ng = "7.08"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 80,
//        position = "80",
//        cpm = 298,
//        flagged = false,
//        ng = "5.45"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 81,
//        position = "81",
//        cpm = 299,
//        flagged = false,
//        ng = "5.39"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 82,
//        position = "82",
//        cpm = 341,
//        flagged = false,
//        ng = "3.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 83,
//        position = "83",
//        cpm = 300,
//        flagged = false,
//        ng = "5.34"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 84,
//        position = "84",
//        cpm = 392,
//        flagged = false,
//        ng = "1.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 85,
//        position = "85",
//        cpm = 410,
//        flagged = false,
//        ng = "1.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 86,
//        position = "86",
//        cpm = 442,
//        flagged = false,
//        ng = "0.39"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 87,
//        position = "87",
//        cpm = 413,
//        flagged = false,
//        ng = "1.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 88,
//        position = "88",
//        cpm = 453,
//        flagged = true,
//        ng = "0.12"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 89,
//        position = "89",
//        cpm = 449,
//        flagged = true,
//        ng = "0.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 90,
//        position = "90",
//        cpm = 440,
//        flagged = false,
//        ng = "0.44"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 91,
//        position = "91",
//        cpm = 477,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 92,
//        position = "92",
//        cpm = 465,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 93,
//        position = "93",
//        cpm = 469,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 94,
//        position = "94",
//        cpm = 417,
//        flagged = false,
//        ng = "1.03"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 95,
//        position = "95",
//        cpm = 373,
//        flagged = false,
//        ng = "2.33"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 96,
//        position = "96",
//        cpm = 297,
//        flagged = false,
//        ng = "5.5"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 97,
//        position = "97",
//        cpm = 286,
//        flagged = false,
//        ng = "6.1"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 98,
//        position = "98",
//        cpm = 341,
//        flagged = false,
//        ng = "3.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 99,
//        position = "99",
//        cpm = 342,
//        flagged = false,
//        ng = "3.45"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 100,
//        position = "100",
//        cpm = 375,
//        flagged = false,
//        ng = "2.27"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 101,
//        position = "101",
//        cpm = 437,
//        flagged = false,
//        ng = "0.51"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 102,
//        position = "102",
//        cpm = 392,
//        flagged = false,
//        ng = "1.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 103,
//        position = "103",
//        cpm = 399,
//        flagged = false,
//        ng = "1.53"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 104,
//        position = "104",
//        cpm = 391,
//        flagged = false,
//        ng = "1.76"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 105,
//        position = "105",
//        cpm = 421,
//        flagged = false,
//        ng = "0.92"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 106,
//        position = "106",
//        cpm = 452,
//        flagged = true,
//        ng = "0.15"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 107,
//        position = "107",
//        cpm = 421,
//        flagged = false,
//        ng = "0.92"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 108,
//        position = "108",
//        cpm = 468,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 109,
//        position = "109",
//        cpm = 494,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 110,
//        position = "110",
//        cpm = 473,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 111,
//        position = "111",
//        cpm = 470,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 112,
//        position = "112",
//        cpm = 395,
//        flagged = false,
//        ng = "1.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 113,
//        position = "113",
//        cpm = 404,
//        flagged = false,
//        ng = "1.39"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 114,
//        position = "114",
//        cpm = 358,
//        flagged = false,
//        ng = "2.85"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 115,
//        position = "115",
//        cpm = 355,
//        flagged = false,
//        ng = "2.95"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 116,
//        position = "116",
//        cpm = 352,
//        flagged = false,
//        ng = "3.07"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 117,
//        position = "117",
//        cpm = 373,
//        flagged = false,
//        ng = "2.33"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 118,
//        position = "118",
//        cpm = 431,
//        flagged = false,
//        ng = "0.67"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 119,
//        position = "119",
//        cpm = 387,
//        flagged = false,
//        ng = "1.89"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 120,
//        position = "120",
//        cpm = 441,
//        flagged = false,
//        ng = "0.42"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 121,
//        position = "121",
//        cpm = 456,
//        flagged = true,
//        ng = "0.05"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 122,
//        position = "122",
//        cpm = 417,
//        flagged = false,
//        ng = "1.03"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 123,
//        position = "123",
//        cpm = 460,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 124,
//        position = "124",
//        cpm = 186,
//        flagged = false,
//        ng = "15.42"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 125,
//        position = "125",
//        cpm = 176,
//        flagged = false,
//        ng = "17.04"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 126,
//        position = "126",
//        cpm = 157,
//        flagged = false,
//        ng = "20.83"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 127,
//        position = "127",
//        cpm = 158,
//        flagged = false,
//        ng = "20.6"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 128,
//        position = "128",
//        cpm = 161,
//        flagged = false,
//        ng = "19.94"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 129,
//        position = "129",
//        cpm = 138,
//        flagged = false,
//        ng = "25.96"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 130,
//        position = "130",
//        cpm = 141,
//        flagged = false,
//        ng = "25.03"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 131,
//        position = "131",
//        cpm = 150,
//        flagged = false,
//        ng = "22.53"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 132,
//        position = "132",
//        cpm = 194,
//        flagged = false,
//        ng = "14.26"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 133,
//        position = "133",
//        cpm = 177,
//        flagged = false,
//        ng = "16.87"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 134,
//        position = "134",
//        cpm = 17,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 135,
//        position = "135",
//        cpm = 20,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 136,
//        position = "136",
//        cpm = 235,
//        flagged = false,
//        ng = "9.74"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 137,
//        position = "137",
//        cpm = 243,
//        flagged = false,
//        ng = "9.05"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 138,
//        position = "138",
//        cpm = 309,
//        flagged = false,
//        ng = "4.89"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 139,
//        position = "139",
//        cpm = 290,
//        flagged = false,
//        ng = "5.88"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 140,
//        position = "140",
//        cpm = 335,
//        flagged = false,
//        ng = "3.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 141,
//        position = "141",
//        cpm = 341,
//        flagged = false,
//        ng = "3.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 142,
//        position = "142",
//        cpm = 396,
//        flagged = false,
//        ng = "1.62"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 143,
//        position = "143",
//        cpm = 371,
//        flagged = false,
//        ng = "2.4"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 144,
//        position = "144",
//        cpm = 400,
//        flagged = false,
//        ng = "1.5"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 145,
//        position = "145",
//        cpm = 407,
//        flagged = false,
//        ng = "1.3"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 146,
//        position = "146",
//        cpm = 412,
//        flagged = false,
//        ng = "1.16"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 147,
//        position = "147",
//        cpm = 423,
//        flagged = false,
//        ng = "0.87"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 148,
//        position = "148",
//        cpm = 375,
//        flagged = false,
//        ng = "2.27"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 149,
//        position = "149",
//        cpm = 403,
//        flagged = false,
//        ng = "1.41"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 150,
//        position = "150",
//        cpm = 372,
//        flagged = false,
//        ng = "2.36"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 151,
//        position = "151",
//        cpm = 365,
//        flagged = false,
//        ng = "2.6"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 152,
//        position = "152",
//        cpm = 412,
//        flagged = false,
//        ng = "1.16"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 153,
//        position = "153",
//        cpm = 358,
//        flagged = false,
//        ng = "2.85"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 154,
//        position = "154",
//        cpm = 389,
//        flagged = false,
//        ng = "1.82"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 155,
//        position = "155",
//        cpm = 432,
//        flagged = false,
//        ng = "0.64"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 156,
//        position = "156",
//        cpm = 442,
//        flagged = false,
//        ng = "0.39"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 157,
//        position = "157",
//        cpm = 446,
//        flagged = false,
//        ng = "0.29"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 158,
//        position = "158",
//        cpm = 472,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 159,
//        position = "159",
//        cpm = 435,
//        flagged = false,
//        ng = "0.56"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 160,
//        position = "160",
//        cpm = 473,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 161,
//        position = "161",
//        cpm = 456,
//        flagged = true,
//        ng = "0.05"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 162,
//        position = "162",
//        cpm = 441,
//        flagged = false,
//        ng = "0.42"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 163,
//        position = "163",
//        cpm = 440,
//        flagged = false,
//        ng = "0.44"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 164,
//        position = "164",
//        cpm = 438,
//        flagged = false,
//        ng = "0.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 165,
//        position = "165",
//        cpm = 472,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 166,
//        position = "166",
//        cpm = 419,
//        flagged = false,
//        ng = "0.98"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 167,
//        position = "167",
//        cpm = 444,
//        flagged = false,
//        ng = "0.34"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 168,
//        position = "168",
//        cpm = 382,
//        flagged = false,
//        ng = "2.04"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 169,
//        position = "169",
//        cpm = 415,
//        flagged = false,
//        ng = "1.08"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 170,
//        position = "170",
//        cpm = 433,
//        flagged = false,
//        ng = "0.61"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 171,
//        position = "171",
//        cpm = 414,
//        flagged = false,
//        ng = "1.11"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 172,
//        position = "172",
//        cpm = 455,
//        flagged = true,
//        ng = "0.08"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 173,
//        position = "173",
//        cpm = 426,
//        flagged = false,
//        ng = "0.79"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 174,
//        position = "174",
//        cpm = 482,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 175,
//        position = "175",
//        cpm = 488,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 176,
//        position = "176",
//        cpm = 248,
//        flagged = false,
//        ng = "8.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 177,
//        position = "177",
//        cpm = 245,
//        flagged = false,
//        ng = "8.89"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 178,
//        position = "178",
//        cpm = 235,
//        flagged = false,
//        ng = "9.74"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 179,
//        position = "179",
//        cpm = 246,
//        flagged = false,
//        ng = "8.81"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 180,
//        position = "180",
//        cpm = 187,
//        flagged = false,
//        ng = "15.27"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 181,
//        position = "181",
//        cpm = 169,
//        flagged = false,
//        ng = "18.32"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 182,
//        position = "182",
//        cpm = 199,
//        flagged = false,
//        ng = "13.59"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 183,
//        position = "183",
//        cpm = 182,
//        flagged = false,
//        ng = "16.04"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 184,
//        position = "184",
//        cpm = 228,
//        flagged = false,
//        ng = "10.38"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 185,
//        position = "185",
//        cpm = 242,
//        flagged = false,
//        ng = "9.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 186,
//        position = "186",
//        cpm = 266,
//        flagged = false,
//        ng = "7.35"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 187,
//        position = "187",
//        cpm = 293,
//        flagged = false,
//        ng = "5.71"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 188,
//        position = "188",
//        cpm = 341,
//        flagged = false,
//        ng = "3.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 189,
//        position = "189",
//        cpm = 358,
//        flagged = false,
//        ng = "2.85"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 190,
//        position = "190",
//        cpm = 330,
//        flagged = false,
//        ng = "3.94"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 191,
//        position = "191",
//        cpm = 296,
//        flagged = false,
//        ng = "5.55"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 192,
//        position = "192",
//        cpm = 245,
//        flagged = false,
//        ng = "8.89"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 193,
//        position = "193",
//        cpm = 259,
//        flagged = false,
//        ng = "7.83"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 194,
//        position = "194",
//        cpm = 236,
//        flagged = false,
//        ng = "9.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 195,
//        position = "195",
//        cpm = 262,
//        flagged = false,
//        ng = "7.62"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 196,
//        position = "196",
//        cpm = 287,
//        flagged = false,
//        ng = "6.05"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 197,
//        position = "197",
//        cpm = 284,
//        flagged = false,
//        ng = "6.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 198,
//        position = "198",
//        cpm = 252,
//        flagged = false,
//        ng = "8.35"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 199,
//        position = "199",
//        cpm = 265,
//        flagged = false,
//        ng = "7.41"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 200,
//        position = "200",
//        cpm = 237,
//        flagged = false,
//        ng = "9.56"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 201,
//        position = "201",
//        cpm = 252,
//        flagged = false,
//        ng = "8.35"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 202,
//        position = "202",
//        cpm = 291,
//        flagged = false,
//        ng = "5.82"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 203,
//        position = "203",
//        cpm = 296,
//        flagged = false,
//        ng = "5.55"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 204,
//        position = "204",
//        cpm = 233,
//        flagged = false,
//        ng = "9.92"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 205,
//        position = "205",
//        cpm = 243,
//        flagged = false,
//        ng = "9.05"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 206,
//        position = "206",
//        cpm = 229,
//        flagged = false,
//        ng = "10.28"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 207,
//        position = "207",
//        cpm = 249,
//        flagged = false,
//        ng = "8.58"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 208,
//        position = "208",
//        cpm = 259,
//        flagged = false,
//        ng = "7.83"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 209,
//        position = "209",
//        cpm = 234,
//        flagged = false,
//        ng = "9.83"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 210,
//        position = "210",
//        cpm = 240,
//        flagged = false,
//        ng = "9.3"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 211,
//        position = "211",
//        cpm = 244,
//        flagged = false,
//        ng = "8.97"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 212,
//        position = "212",
//        cpm = 279,
//        flagged = false,
//        ng = "6.52"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 213,
//        position = "213",
//        cpm = 294,
//        flagged = false,
//        ng = "5.66"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 214,
//        position = "214",
//        cpm = 312,
//        flagged = false,
//        ng = "4.74"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 215,
//        position = "215",
//        cpm = 310,
//        flagged = false,
//        ng = "4.84"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 216,
//        position = "216",
//        cpm = 326,
//        flagged = false,
//        ng = "4.11"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 217,
//        position = "217",
//        cpm = 354,
//        flagged = false,
//        ng = "2.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 218,
//        position = "218",
//        cpm = 321,
//        flagged = false,
//        ng = "4.33"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 219,
//        position = "219",
//        cpm = 358,
//        flagged = false,
//        ng = "2.85"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 220,
//        position = "220",
//        cpm = 229,
//        flagged = false,
//        ng = "10.28"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 221,
//        position = "221",
//        cpm = 225,
//        flagged = false,
//        ng = "10.67"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 222,
//        position = "222",
//        cpm = 215,
//        flagged = false,
//        ng = "11.7"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 223,
//        position = "223",
//        cpm = 220,
//        flagged = false,
//        ng = "11.17"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 224,
//        position = "224",
//        cpm = 262,
//        flagged = false,
//        ng = "7.62"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 225,
//        position = "225",
//        cpm = 271,
//        flagged = false,
//        ng = "7.02"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 226,
//        position = "226",
//        cpm = 221,
//        flagged = false,
//        ng = "11.07"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 227,
//        position = "227",
//        cpm = 236,
//        flagged = false,
//        ng = "9.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 228,
//        position = "228",
//        cpm = 220,
//        flagged = false,
//        ng = "11.17"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 229,
//        position = "229",
//        cpm = 226,
//        flagged = false,
//        ng = "10.57"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 230,
//        position = "230",
//        cpm = 240,
//        flagged = false,
//        ng = "9.3"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 231,
//        position = "231",
//        cpm = 258,
//        flagged = false,
//        ng = "7.9"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 232,
//        position = "232",
//        cpm = 308,
//        flagged = false,
//        ng = "4.94"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 233,
//        position = "233",
//        cpm = 297,
//        flagged = false,
//        ng = "5.5"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 234,
//        position = "234",
//        cpm = 307,
//        flagged = false,
//        ng = "4.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 235,
//        position = "235",
//        cpm = 299,
//        flagged = false,
//        ng = "5.39"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 236,
//        position = "236",
//        cpm = 259,
//        flagged = false,
//        ng = "7.83"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 237,
//        position = "237",
//        cpm = 255,
//        flagged = false,
//        ng = "8.12"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 238,
//        position = "238",
//        cpm = 222,
//        flagged = false,
//        ng = "10.96"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 239,
//        position = "239",
//        cpm = 212,
//        flagged = false,
//        ng = "12.03"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 240,
//        position = "240",
//        cpm = 192,
//        flagged = false,
//        ng = "14.54"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 241,
//        position = "241",
//        cpm = 196,
//        flagged = false,
//        ng = "13.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 242,
//        position = "242",
//        cpm = 199,
//        flagged = false,
//        ng = "13.59"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 243,
//        position = "243",
//        cpm = 166,
//        flagged = false,
//        ng = "18.9"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 244,
//        position = "244",
//        cpm = 196,
//        flagged = false,
//        ng = "13.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 245,
//        position = "245",
//        cpm = 182,
//        flagged = false,
//        ng = "16.04"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 246,
//        position = "246",
//        cpm = 305,
//        flagged = false,
//        ng = "5.09"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 247,
//        position = "247",
//        cpm = 307,
//        flagged = false,
//        ng = "4.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 248,
//        position = "248",
//        cpm = 341,
//        flagged = false,
//        ng = "3.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 249,
//        position = "249",
//        cpm = 340,
//        flagged = false,
//        ng = "3.53"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 250,
//        position = "250",
//        cpm = 361,
//        flagged = false,
//        ng = "2.74"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 251,
//        position = "251",
//        cpm = 370,
//        flagged = false,
//        ng = "2.43"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 252,
//        position = "252",
//        cpm = 246,
//        flagged = false,
//        ng = "8.81"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 253,
//        position = "253",
//        cpm = 246,
//        flagged = false,
//        ng = "8.81"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 254,
//        position = "254",
//        cpm = 204,
//        flagged = false,
//        ng = "12.96"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 255,
//        position = "255",
//        cpm = 211,
//        flagged = false,
//        ng = "12.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 256,
//        position = "256",
//        cpm = 194,
//        flagged = false,
//        ng = "14.26"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 257,
//        position = "257",
//        cpm = 167,
//        flagged = false,
//        ng = "18.7"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 258,
//        position = "258",
//        cpm = 166,
//        flagged = false,
//        ng = "18.9"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 259,
//        position = "259",
//        cpm = 181,
//        flagged = false,
//        ng = "16.2"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 260,
//        position = "260",
//        cpm = 196,
//        flagged = false,
//        ng = "13.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 261,
//        position = "261",
//        cpm = 174,
//        flagged = false,
//        ng = "17.39"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 262,
//        position = "262",
//        cpm = 191,
//        flagged = false,
//        ng = "14.68"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 263,
//        position = "263",
//        cpm = 205,
//        flagged = false,
//        ng = "12.84"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 264,
//        position = "264",
//        cpm = 181,
//        flagged = false,
//        ng = "16.2"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 265,
//        position = "265",
//        cpm = 207,
//        flagged = false,
//        ng = "12.6"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 266,
//        position = "266",
//        cpm = 220,
//        flagged = false,
//        ng = "11.17"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 267,
//        position = "267",
//        cpm = 232,
//        flagged = false,
//        ng = "10.01"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 268,
//        position = "268",
//        cpm = 194,
//        flagged = false,
//        ng = "14.26"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 269,
//        position = "269",
//        cpm = 183,
//        flagged = false,
//        ng = "15.88"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 270,
//        position = "270",
//        cpm = 170,
//        flagged = false,
//        ng = "18.13"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 271,
//        position = "271",
//        cpm = 177,
//        flagged = false,
//        ng = "16.87"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 272,
//        position = "272",
//        cpm = 185,
//        flagged = false,
//        ng = "15.57"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 273,
//        position = "273",
//        cpm = 216,
//        flagged = false,
//        ng = "11.59"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 274,
//        position = "274",
//        cpm = 206,
//        flagged = false,
//        ng = "12.72"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 275,
//        position = "275",
//        cpm = 209,
//        flagged = false,
//        ng = "12.37"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 276,
//        position = "276",
//        cpm = 197,
//        flagged = false,
//        ng = "13.85"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 277,
//        position = "277",
//        cpm = 200,
//        flagged = false,
//        ng = "13.46"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 278,
//        position = "278",
//        cpm = 226,
//        flagged = false,
//        ng = "10.57"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 279,
//        position = "279",
//        cpm = 242,
//        flagged = false,
//        ng = "9.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 280,
//        position = "280",
//        cpm = 288,
//        flagged = false,
//        ng = "5.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 281,
//        position = "281",
//        cpm = 276,
//        flagged = false,
//        ng = "6.7"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 282,
//        position = "282",
//        cpm = 338,
//        flagged = false,
//        ng = "3.61"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 283,
//        position = "283",
//        cpm = 307,
//        flagged = false,
//        ng = "4.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 284,
//        position = "284",
//        cpm = 381,
//        flagged = false,
//        ng = "2.07"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 285,
//        position = "285",
//        cpm = 359,
//        flagged = false,
//        ng = "2.81"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 286,
//        position = "286",
//        cpm = 241,
//        flagged = false,
//        ng = "9.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 287,
//        position = "287",
//        cpm = 241,
//        flagged = false,
//        ng = "9.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 288,
//        position = "288",
//        cpm = 175,
//        flagged = false,
//        ng = "17.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 289,
//        position = "289",
//        cpm = 195,
//        flagged = false,
//        ng = "14.12"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 290,
//        position = "290",
//        cpm = 162,
//        flagged = false,
//        ng = "19.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 291,
//        position = "291",
//        cpm = 177,
//        flagged = false,
//        ng = "16.87"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 292,
//        position = "292",
//        cpm = 181,
//        flagged = false,
//        ng = "16.2"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 293,
//        position = "293",
//        cpm = 162,
//        flagged = false,
//        ng = "19.73"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 294,
//        position = "294",
//        cpm = 231,
//        flagged = false,
//        ng = "10.1"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 295,
//        position = "295",
//        cpm = 256,
//        flagged = false,
//        ng = "8.05"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 296,
//        position = "296",
//        cpm = 248,
//        flagged = false,
//        ng = "8.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 297,
//        position = "297",
//        cpm = 267,
//        flagged = false,
//        ng = "7.28"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 298,
//        position = "298",
//        cpm = 283,
//        flagged = false,
//        ng = "6.28"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 299,
//        position = "299",
//        cpm = 296,
//        flagged = false,
//        ng = "5.55"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 300,
//        position = "300",
//        cpm = 333,
//        flagged = false,
//        ng = "3.81"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 301,
//        position = "301",
//        cpm = 395,
//        flagged = false,
//        ng = "1.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 302,
//        position = "302",
//        cpm = 336,
//        flagged = false,
//        ng = "3.69"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 303,
//        position = "303",
//        cpm = 339,
//        flagged = false,
//        ng = "3.57"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 304,
//        position = "304",
//        cpm = 189,
//        flagged = false,
//        ng = "14.97"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 305,
//        position = "305",
//        cpm = 183,
//        flagged = false,
//        ng = "15.88"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 306,
//        position = "306",
//        cpm = 215,
//        flagged = false,
//        ng = "11.7"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 307,
//        position = "307",
//        cpm = 248,
//        flagged = false,
//        ng = "8.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 308,
//        position = "308",
//        cpm = 236,
//        flagged = false,
//        ng = "9.65"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 309,
//        position = "309",
//        cpm = 239,
//        flagged = false,
//        ng = "9.39"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 310,
//        position = "310",
//        cpm = 289,
//        flagged = false,
//        ng = "5.93"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 311,
//        position = "311",
//        cpm = 304,
//        flagged = false,
//        ng = "5.14"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 312,
//        position = "312",
//        cpm = 306,
//        flagged = false,
//        ng = "5.04"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 313,
//        position = "313",
//        cpm = 293,
//        flagged = false,
//        ng = "5.71"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 314,
//        position = "314",
//        cpm = 368,
//        flagged = false,
//        ng = "2.5"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 315,
//        position = "315",
//        cpm = 340,
//        flagged = false,
//        ng = "3.53"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 316,
//        position = "316",
//        cpm = 383,
//        flagged = false,
//        ng = "2.01"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 317,
//        position = "317",
//        cpm = 394,
//        flagged = false,
//        ng = "1.67"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 318,
//        position = "318",
//        cpm = 348,
//        flagged = false,
//        ng = "3.21"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 319,
//        position = "319",
//        cpm = 370,
//        flagged = false,
//        ng = "2.43"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 320,
//        position = "320",
//        cpm = 265,
//        flagged = false,
//        ng = "7.41"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 321,
//        position = "321",
//        cpm = 255,
//        flagged = false,
//        ng = "8.12"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 322,
//        position = "322",
//        cpm = 208,
//        flagged = false,
//        ng = "12.48"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 323,
//        position = "323",
//        cpm = 227,
//        flagged = false,
//        ng = "10.47"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 324,
//        position = "324",
//        cpm = 216,
//        flagged = false,
//        ng = "11.59"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 325,
//        position = "325",
//        cpm = 199,
//        flagged = false,
//        ng = "13.59"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 326,
//        position = "326",
//        cpm = 249,
//        flagged = false,
//        ng = "8.58"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 327,
//        position = "327",
//        cpm = 265,
//        flagged = false,
//        ng = "7.41"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 328,
//        position = "328",
//        cpm = 302,
//        flagged = false,
//        ng = "5.24"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 329,
//        position = "329",
//        cpm = 325,
//        flagged = false,
//        ng = "4.15"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 330,
//        position = "330",
//        cpm = 358,
//        flagged = false,
//        ng = "2.85"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 331,
//        position = "331",
//        cpm = 358,
//        flagged = false,
//        ng = "2.85"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 332,
//        position = "332",
//        cpm = 386,
//        flagged = false,
//        ng = "1.92"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 333,
//        position = "333",
//        cpm = 377,
//        flagged = false,
//        ng = "2.2"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 334,
//        position = "334",
//        cpm = 370,
//        flagged = false,
//        ng = "2.43"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 335,
//        position = "335",
//        cpm = 397,
//        flagged = false,
//        ng = "1.59"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 336,
//        position = "336",
//        cpm = 385,
//        flagged = false,
//        ng = "1.95"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 337,
//        position = "337",
//        cpm = 416,
//        flagged = false,
//        ng = "1.06"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 338,
//        position = "338",
//        cpm = 435,
//        flagged = false,
//        ng = "0.56"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 339,
//        position = "339",
//        cpm = 438,
//        flagged = false,
//        ng = "0.49"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 340,
//        position = "340",
//        cpm = 429,
//        flagged = false,
//        ng = "0.72"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 341,
//        position = "341",
//        cpm = 388,
//        flagged = false,
//        ng = "1.86"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 342,
//        position = "342",
//        cpm = 356,
//        flagged = false,
//        ng = "2.92"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 343,
//        position = "343",
//        cpm = 283,
//        flagged = false,
//        ng = "6.28"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 344,
//        position = "344",
//        cpm = 284,
//        flagged = false,
//        ng = "6.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 345,
//        position = "345",
//        cpm = 281,
//        flagged = false,
//        ng = "6.4"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 346,
//        position = "346",
//        cpm = 307,
//        flagged = false,
//        ng = "4.99"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 347,
//        position = "347",
//        cpm = 305,
//        flagged = false,
//        ng = "5.09"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 348,
//        position = "348",
//        cpm = 311,
//        flagged = false,
//        ng = "4.79"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 349,
//        position = "349",
//        cpm = 320,
//        flagged = false,
//        ng = "4.37"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 350,
//        position = "350",
//        cpm = 386,
//        flagged = false,
//        ng = "1.92"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 351,
//        position = "351",
//        cpm = 403,
//        flagged = false,
//        ng = "1.41"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 352,
//        position = "352",
//        cpm = 411,
//        flagged = false,
//        ng = "1.19"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 353,
//        position = "353",
//        cpm = 410,
//        flagged = false,
//        ng = "1.22"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 354,
//        position = "354",
//        cpm = 471,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 355,
//        position = "355",
//        cpm = 451,
//        flagged = true,
//        ng = "0.17"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 356,
//        position = "356",
//        cpm = 468,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 357,
//        position = "357",
//        cpm = 501,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 358,
//        position = "358",
//        cpm = 465,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 359,
//        position = "359",
//        cpm = 463,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 360,
//        position = "360",
//        cpm = 456,
//        flagged = true,
//        ng = "0.05"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 361,
//        position = "361",
//        cpm = 451,
//        flagged = true,
//        ng = "0.17"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 362,
//        position = "362",
//        cpm = 487,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 363,
//        position = "363",
//        cpm = 481,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 364,
//        position = "364",
//        cpm = 477,
//        flagged = true,
//        ng = "0.0"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 365,
//        position = "365",
//        cpm = 431,
//        flagged = false,
//        ng = "0.67"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 366,
//        position = "366",
//        cpm = 343,
//        flagged = false,
//        ng = "3.41"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 367,
//        position = "367",
//        cpm = 359,
//        flagged = false,
//        ng = "2.81"
//    ),
//    ExaminationPoint(
//        average = 0.0,
//        identifier = "A16_244.txt",
//        pattern = "KORTYZOL_5_MIN",
//        probeNumber = 368,
//        position = "368",
//        cpm = 418,
//        flagged = false,
//        ng = "1.0"
//    )
//)
