package calculator.constants

import calculator.model.results.BaseGraph
import calculator.model.results.Coordinates

var coordinates = listOf(
    Coordinates(x = 0.1, y = 0.908),
    Coordinates(x = 0.1, y = 0.72),
    Coordinates(x = 0.4, y = 0.63),
    Coordinates(x = 0.4, y = 0.477),
    Coordinates(x = 0.7, y = 0.327),
    Coordinates(x = 0.7, y = 0.308),
    Coordinates(x = 1.0, y = 0.017),
    Coordinates(x = 1.0, y = -0.087),
    Coordinates(x = 1.3, y = -0.308),
    Coordinates(x = 1.3, y = -0.347),
    Coordinates(x = 1.6, y = -0.63),
    Coordinates(x = 1.6, y = -0.72),
    Coordinates(x = 1.9, y = -1.005),
    Coordinates(x = 1.9, y = -1.123))

val graphExample = BaseGraph(
    coordinates,
    r = 0.985921,
    zeroBindingPercent = 21.42)
