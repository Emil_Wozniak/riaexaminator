package calculator.constants

import calculator.model.hormone.CortisolPattern
import calculator.model.hormone.Point

val examplePattern = CortisolPattern(
    totals = 2,
    NSBs = 3,
    Zeros = 3,
    length = 7,
    repeats = 2,
    startValue = 1.25,
    points = listOf(
        Point(name = "T", value = 0.0),
        Point(name = "T", value = 0.0),
        Point(name = "NSB", value = 0.0),
        Point(name = "NSB", value = 0.0),
        Point(name = "NSB", value = 0.0),
        Point(name = "ZERO", value = 0.0),
        Point(name = "ZERO", value = 0.0),
        Point(name = "ZERO", value = 0.0),
        Point(name = "1.25", value = 1.25),
        Point(name = "1.25", value = 1.25),
        Point(name = "2.5", value = 2.5),
        Point(name = "2.5", value = 2.5),
        Point(name = "5.0", value = 5.0),
        Point(name = "5.0", value = 5.0),
        Point(name = "10.0", value = 10.0),
        Point(name = "10.0", value = 10.0),
        Point(name = "20.0", value = 20.0),
        Point(name = "20.0", value = 20.0),
        Point(name = "40.0", value = 40.0),
        Point(name = "40.0", value = 40.0),
        Point(name = "80.0", value = 80.0),
        Point(name = "80.0", value = 80.0)),
    controlPoints = 0)

val exampleDose = listOf(
    0.1, 0.1, 0.4, 0.4,
    0.7, 0.7, 1.0, 1.0,
    1.3, 1.3, 1.6, 1.6,
    1.9, 1.9)

val standardsCPM = listOf(412, 390, 378, 352, 322, 316, 225, 249, 165, 174, 102, 116, 74, 65)

val bindingPercent = listOf(
    89.0, 84.0, 81.0, 75.0, 68.0, 67.0, 51.0,
    45.0, 33.0, 31.0, 19.0, 16.0, 9.0, 7.0)

val exampleRealZero = listOf(
    0.908, 0.72, 0.63, 0.477, 0.327, 0.308, 0.017, -0.087,
    -0.308, -0.347, -0.63, -0.72, -1.005, -1.123)

const val exampleZeroBinding = 21.42

const val exampleParamB = -1.0403

const val exampleParamA = 0.9831571428571428

val exampleCorrelation = 0.985921
