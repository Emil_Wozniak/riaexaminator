package calculator.strategy

import calculator.constants.DEFAULT_TARGET_POINT
import calculator.constants.EMPTY
import calculator.factory.ResultsFactory
import calculator.functions.extractor.TxtDataExtractor
import calculator.model.FileSettings
import calculator.model.hormone.CortisolPattern
import calculator.model.hormone.CustomPattern
import calculator.model.hormone.HormonePattern
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class PatternResolverTest : PatternResolver(TxtDataExtractor()) {

    private var fileSettings: FileSettings? = null
    private var factory: ResultsFactory? = null
    private var hormonePattern: HormonePattern? = null
    private var pattern: String? = null
    private var startValue: String? = null

    @BeforeEach
    fun setUp() {
        fileSettings = FileSettings(DEFAULT_TARGET_POINT, true, "", "1.25")
        pattern = "2;3;3;7;2;0"
        startValue = "2.5"
    }

    @AfterEach
    fun tearDown() {
        fileSettings = null
        factory = null
        hormonePattern = null
        pattern = null
        startValue = null
    }

    @Test
    fun `resolve should contains cortisol pattern`() {
        factory = resolve(fileSettings!!)
        hormonePattern = factory!!.patternType()
        assertTrue(hormonePattern is CortisolPattern)
    }

    @Test
    fun `resolve should contains custom pattern`() {
        fileSettings = FileSettings(EMPTY, false, pattern!!, startValue!!)
        factory = resolve(fileSettings!!)
        hormonePattern = factory!!.patternType()
        assertTrue(hormonePattern is CustomPattern)
    }

    @Test
    fun `resolve should throw exception on no pattern value and defaults false`() {
        assertThrows<NumberFormatException> {
            fileSettings = FileSettings(EMPTY, false,/* will cause error */ "", startValue!!)
            factory = resolve(fileSettings!!)
        }
    }

    @Test
    fun `resolve should throw exception on defaults false and startValue is empty`() {
        assertThrows<NumberFormatException> {
            fileSettings = FileSettings(DEFAULT_TARGET_POINT, false, pattern!!, /* will cause error */"")
            factory = resolve(fileSettings!!)
        }
    }
}
