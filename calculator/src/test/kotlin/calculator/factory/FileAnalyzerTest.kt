package calculator.factory

import calculator.FileAnalyzer
import calculator.constants.controlCurve
import calculator.constants.examinationPoints
import calculator.constants.graphExample
import calculator.model.ExaminationResults
import calculator.model.FileSettings
import calculator.model.results.ExaminationPoint
import calculator.util.readFile
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE
import org.springframework.mock.web.MockMultipartFile
import java.io.ByteArrayInputStream

internal class FileAnalyzerTest : FileAnalyzer() {

    private lateinit var firstFile: MockMultipartFile
    private lateinit var lines: MutableList<String>
    private lateinit var readFile: ByteArrayInputStream
    private lateinit var name: String
    private lateinit var type: String
    private lateinit var results: ExaminationResults
    private lateinit var settings: FileSettings

    @BeforeEach
    fun setUp() {
        lines = mutableListOf()
        lines.addAll(readFile("../samples/A16_244.txt", true))
        readFile = lines.joinToString(separator = "\n").byteInputStream()
        type = "A16_244.txt"
        name = "A16_244"
        firstFile = MockMultipartFile(name, type, MULTIPART_FORM_DATA_VALUE, readFile)
        settings = FileSettings("", true, "", "")
        results = analyze(firstFile, settings)
    }

    @Test
    fun `analyze results should not be null`() =
        assertTrue(results.title != null)

    @Test
    fun `results should return curve points`() =
        assertEquals(controlCurve, results.controlCurve)

    @Test
    fun `results should contain examination points`() =
        assertEquals(examinationPoints, results.resultPoints)

    @Test
    fun `results should contain errors`() =
        assertEquals(listOf<ExaminationPoint>(), results.errors)

    @Test
    fun `results should contain graph on success`() =
        assertEquals(graphExample, results.graph)
}
