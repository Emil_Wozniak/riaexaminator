/*
 *  Copyright 2001-2020 the original author or authors.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package calculator.counter

import cyclops.data.LazySeq.range
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class SpreadCounterTest {

    private lateinit var spreadCounter: SpreadCounter
    private lateinit var spread: List<Boolean>
    private lateinit var result: List<Boolean>
    private lateinit var test: List<Int>

    @BeforeEach
    fun setUp() {
        spreadCounter = SpreadCounter()
        result = range(0, 11).map { false }.toList()
        test = range(0, 11).toList()
    }

    @Test
    fun `isSpread should return list of false if list size is length of 2`() {
        spread = spreadCounter.isSpread(listOf(2, 2))
        assertEquals(listOf(false, false), spread)
    }

    @Test
    fun `isSpread should return list of false if list size is greater than 10`() {
        spread = spreadCounter.isSpread(test)
        assertEquals(result, spread)
    }

    @Test
    fun `isSpread should return list of false if list has 3 equals values`() {
        spread = spreadCounter.isSpread(test.take(3).map { 5 })
        assertEquals(result.take(3), spread)
    }

    @Test
    fun `isSpread should return list of false if list has 10 equals values`() {
        spread = spreadCounter.isSpread(test.take(10).map { 5 })
        assertEquals(result.take(10), spread)
    }

    @Test
    fun `isSpread should return one true value`() {
        val transform: (Int) -> Int = { if (it < 2) it + 5 else it }
        spread = spreadCounter.isSpread(test.take(3).map(transform))
        assertEquals(listOf(false, false, true), spread)
    }
}