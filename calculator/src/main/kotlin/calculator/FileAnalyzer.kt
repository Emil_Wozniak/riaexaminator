package calculator

import calculator.constants.FILE_NAME
import calculator.factory.AnalysisExecutor
import calculator.factory.ResultsFactory
import calculator.functions.convertFile
import calculator.functions.extractor.DataExtractor
import calculator.functions.extractor.TxtDataExtractor
import calculator.model.ExaminationError
import calculator.model.ExaminationFile
import calculator.model.ExaminationResults
import calculator.model.FileSettings
import calculator.model.results.BaseGraph
import calculator.model.results.CurvePoint
import calculator.model.results.ExaminationPoint
import calculator.strategy.PatternResolver
import calculator.strategy.PatternStrategy
import calculator.util.FileReader
import calculator.util.TxtFileReader
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.multipart.MultipartFile

/**
 * Prepares all interfaces and classes for further calculation on examine data.
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
open class FileAnalyzer : AnalysisExecutor {

    private val log: Logger = LoggerFactory.getLogger(FileAnalyzer::class.java.name)

    /**
     * Setups objects required for the analysis and performs logic.
     *
     * @param multipart analysed file containing data
     * @param settings analyze parameters
     */
    override fun analyze(
        multipart: MultipartFile,
        settings: FileSettings
    ): ExaminationResults {
        val examinationFile: ExaminationFile = convertFile(multipart, settings)
        val (file) = examinationFile
        log.info(FILE_NAME, file.originalFilename)
        val reader: FileReader = TxtFileReader()
        val extractor: DataExtractor = TxtDataExtractor()
        val strategy: PatternStrategy = PatternResolver(extractor)
        val factory: ResultsFactory = strategy.resolve(settings)
        val data = reader.read(examinationFile).toMutableList()
        return createResults(factory, examinationFile, data)
    }

    private fun createResults(
        factory: ResultsFactory,
        examinationFile: ExaminationFile,
        data: MutableList<String>
    ): ExaminationResults =
        factory.run {
            val (file, _, patternTitle) = examinationFile
            val curve: List<CurvePoint?> = createCurve(data)
            val points: List<ExaminationPoint?> = createPoints(data)
            val errors: List<ExaminationError?> = getErrors()
            val graph: BaseGraph = createGraph()
            ExaminationResults(
                null,
                file.name,
                patternTitle,
                curve,
                points,
                graph,
                errors
            )
        }
}
