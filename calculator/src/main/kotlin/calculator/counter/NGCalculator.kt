package calculator.counter

import calculator.constants.NG_FAILED
import calculator.constants.NG_INFINITE_OR_NAN
import calculator.constants.NG_SUCCESS
import calculator.functions.ValueValidator
import calculator.functions.math.Algorithms
import calculator.functions.math.powerBy10
import calculator.model.CalibrationCurveValues
import io.vavr.control.Try
import org.apache.commons.math3.util.Precision.round
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.math.log10

@Suppress("SpellCheckingInspection")
class NGCalculator(
    private val cpms: List<Double>,
    private val calibrationCurveValues: CalibrationCurveValues,
    private val parameterA: Double,
    private val parameterB: Double
) : Algorithms, ValueValidator {

    companion object {
        val log: Logger = LoggerFactory.getLogger(NGCalculator::class.java.name)
        var wrongs = 0
    }

    /**
     * Excel version:
     * = 10^ * (( LOG((B44-$I$16) *100 /$J$18 / (100-(B44-$I$16) *100 /$J$18)) -$R$19) /$R$20)
     *
     *
     * calculates the value of hormone nanograms by formula:
     * { 10^(( LOG((cmp-zero) *100 / binding / (100-(cmp-zero) *100 / binding)) - component.getRegressionParameterA()) / component.getRegressionParameterB())}
     *
     * @return the value of hormone nanograms in the sample
     */
    fun calculate(): List<Double> = Try
        .of {
            cpms.asSequence()
                .map { processCPM(it) }
                .map { log10(it) }
                .map { it - parameterA }
                .map { it / parameterB }
                .map { powerBy10(it) }
                .map {
                    if (it.isNaN() || it.isInfinite()) {
                        wrongs + 1
                        0.0
                    } else it
                }
                .map { round(it, 2) }
                .toList()
        }
        .onSuccess { log.info(NG_SUCCESS, it.size) }
        .onFailure { log.error(NG_FAILED, it.cause, it.message) }
        .map {
            if (wrongs > 0) log.warn(NG_INFINITE_OR_NAN, wrongs)
            it
        }
        .getOrElse(cpms)

    private fun processCPM(CPM: Double): Double = CPM.let {
        val (_, nsbs, _, binding) = calibrationCurveValues
        val first = (it - nsbs.toDouble()) * 100 / binding.toDouble()
        val second = 100 - (it - nsbs.toDouble()) * 100 / binding.toDouble()
        first / second
    }
}
