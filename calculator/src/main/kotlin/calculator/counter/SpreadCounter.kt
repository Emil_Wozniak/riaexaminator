package calculator.counter

import calculator.constants.DIXON_TEST_BORDER_VALUE
import calculator.constants.SPREAD_COUNTER
import calculator.functions.math.DixonTest
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.streams.toList

class SpreadCounter {
    companion object {
        val log: Logger = LoggerFactory.getLogger(SpreadCounter::class.java.name)
    }

    /**
     * if cpms can be tested it will perform dixon test [.dixonTest].
     *
     * @param cpms examine values from upload file.
     * @return when pass [.isTestable] it will use [.dixonTest]
     * in other case [.skipTest]
     */
    fun isSpread(cpms: List<Int>): List<Boolean> = Try
        .of {
            if (isTestable(cpms)) dixonTest(cpms)
            else skipTest(cpms)
        }
        .onFailure { log.error(SPREAD_COUNTER + it.cause) }
        .getOrElse(skipTest(cpms))

    private fun isTestable(cpms: List<Int>): Boolean =
        cpms.size in 3 until DIXON_TEST_BORDER_VALUE

    /**
     * @param cpms values to check
     * @return false values for each element of CPMs list,
     * if and only if CPMs can't be checked by [.dixonTest]
     * @see SpreadCounter.dixonTest
     */
    private fun skipTest(cpms: List<Int>): List<Boolean> =
        cpms.map { false }.toList()

    private fun dixonTest(values: List<Int>): List<Boolean> {
        val examinePoints = values.map { it.toDouble() }.toList()
        val eliminateOutliers: List<Double> = DixonTest().eliminateOutliers(examinePoints)
        return getResult(examinePoints, eliminateOutliers)
    }

    private fun getResult(values: List<Double>, outliers: List<Double>): List<Boolean> =
        values
            .stream()
            .map { !outliers.contains(it) }
            .toList()

}
