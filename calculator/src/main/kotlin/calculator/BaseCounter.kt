package calculator

import calculator.counter.NGCalculator
import calculator.functions.StandardExtractor
import calculator.functions.counter.*
import calculator.model.CalibrationCurveValues
import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import calculator.model.results.CurvePoint
import org.slf4j.Logger
import org.slf4j.LoggerFactory

open class BaseCounter(val pattern: HormonePattern?) :
        Counter, ZeroBindingPercentage, RealZero,
        BindingPercentage, CalibrationCurveCounter,
        ParameterB, ParameterA, Correlation,
        MeterRead, Dose, StandardExtractor, PointAverage {
    override var dose: List<Double> = listOf()
    override var correlation: Double = 0.0
    override var zeroBinding: Double = 0.0
    override var realZero: List<Double> = listOf()
    override var calibrationCurveValues: CalibrationCurveValues = CalibrationCurveValues()
    override val errors: MutableList<ExaminationError> = mutableListOf()
    override var standards: MutableList<Int> = mutableListOf()
    override var paramB: Double = 0.0
    override var paramA: Double = 0.0
    override var bindingPercentage: List<Double> = listOf()

    override fun countMeters(curve: List<CurvePoint>): List<Double> {
        this.calibrationCurveValues = calculateCalibrationCurve(this.pattern!!, curve)
        this.standards = extractStandard(curve, this.calibrationCurveValues)
        this.bindingPercentage = binding(this.standards, this.calibrationCurveValues, this.errors)
        this.realZero = countRealZero(this.bindingPercentage, this.errors)
        this.zeroBinding = zeroBind(this.calibrationCurveValues)
        this.dose = countDose(this.calibrationCurveValues, this.pattern, this.errors)
        this.paramB = countB(this.realZero, this.dose, this.errors)
        this.paramA = countA(this.realZero, this.dose, this.paramB, this.errors)
        this.correlation = findCorrelation(this.pattern, this.dose, this.realZero)
        return countReadMeter(this.standards, this.calibrationCurveValues, this.paramA, this.paramB)
    }

    fun calculateCalibrationCurve(pattern: HormonePattern, curve: List<CurvePoint>):
            CalibrationCurveValues = calibrate(pattern, curve.toMutableList(), mutableListOf())

    override fun calculateDoseLogarithm(pattern: HormonePattern) =
            calculateCalibrationCurve(pattern, listOf())

    override fun countNGs(cpms: List<Double>): List<Double> =
            NGCalculator(cpms, this.calibrationCurveValues, this.paramA, this.paramB)
                .calculate()
}
