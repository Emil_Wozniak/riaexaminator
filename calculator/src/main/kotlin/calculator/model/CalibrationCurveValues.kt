package calculator.model

/**
 * Represents Calibration Curve standard values, it is instantiating by
 * CalibrationCurveCounter.
 * @property total average value of all highest points in the standard
 * @property nsb non specific binding average value of antibody binding response
 * @property zero average value of all lowest points in standard
 * @property binding rest of subtraction from zero and nsb
 * @property standardStart sum of number of the totals, zeros and nsbs
 * @property standardEnd amount of all standard points subtracts control points
 * @constructor default instance of calibration curve if
 * CalibrationCurveCounter failed.
 *
 * @see calculator.functions.counter.CalibrationCurveCounter.calibrate
 * @link https://en.wikipedia.org/wiki/Calibration_curve
 */
data class CalibrationCurveValues(
    val total: Int,
    val nsb: Int,
    val zero: Int,
    val binding: Int,
    val standardStart: Int,
    val standardEnd: Int) {

    constructor(): this(0,0,0,0,0,0)
}


