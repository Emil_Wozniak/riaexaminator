package calculator.model.results

/**
 * Represents Calibration Curve point instance.
 *
 * @see ExaminationResult
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class CurvePoint(
    var meterRead: Double,
    override var identifier: String,
    override val pattern: String,
    override val probeNumber: Int,
    override val position: String,
    override val cpm: Int,
    override var flagged: Boolean?,
    override var ng: String)
    : ExaminationResult(
    identifier,
    pattern,
    probeNumber,
    position,
    cpm,
    flagged,
    ng)
