package calculator.model.results

/**
 * @property coordinates points on graph
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class BaseGraph(
    var coordinates: List<Coordinates> = listOf(),
    var r: Double = 0.0,
    var zeroBindingPercent: Double = 0.0)

data class Coordinates(
    var x: Double,
    var y: Double
)
