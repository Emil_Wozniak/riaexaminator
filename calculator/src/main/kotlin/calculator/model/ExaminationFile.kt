package calculator.model

import calculator.constants.SUCCESS_MSG
import cyclops.control.Eval
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.multipart.MultipartFile
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.function.Supplier

/**
 * Container for storing uploaded file metadata.
 */
data class ExaminationFile(
    val file: MultipartFile,
    val target: String,
    val patternTitle: String,
    val defaults: Boolean? = false
) {

    private val log: Logger = LoggerFactory.getLogger(ExaminationFile::class.java.name)

    /**
     * Stores content of uploaded file.
     */
    val contents: Supplier<List<String>> = Eval.later { loadContents() }

    private fun loadContents(): List<String> = Try
        .of { readLines().toList() }
        .onFailure { log.error(it.message) }
        .getOrElse { listOf() }

    /**
     * @return uploaded file lines on success or empty array list on failed.
     */
    private fun readLines(): MutableList<String> = Try
        .withResources { BufferedReader(InputStreamReader(file.inputStream)) }
        .of {
            val metadata: MutableList<String> = mutableListOf()
            it.forEachLine { line -> metadata.add(line) }
            if (metadata.size == 1) metadata[0].split("\n").toMutableList()
            else metadata
        }
        .onSuccess { log.info(SUCCESS_MSG, it.size) }
        .onFailure { log.warn(it.message) }
        .getOrElse { mutableListOf() }
}
