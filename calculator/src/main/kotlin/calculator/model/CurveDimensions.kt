package calculator.model

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class CurveDimensions(
    val T: Int,
    val N: Int,
    val O: Int) {

    fun all(): Int = T + N + O

    fun zeroStart(): Int = T

    fun zeroEnd(): Int = N + T

    fun nsbsEnd(): Int = O + N + T

}
