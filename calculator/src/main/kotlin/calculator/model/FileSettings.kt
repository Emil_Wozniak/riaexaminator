package calculator.model

import calculator.constants.DEFAULT_TARGET_POINT

/**
 * @property target DEFAULT_TARGET_POINT by default, target
 * value represents string which should precede results in file
 * @property defaults when true it will cause create Cortisol
 * pattern, when false and rest of property is set properly
 * it will cause Custom pattern
 * @property pattern is "2;3;3;7;2;0" by default but when
 * defaults property is false should contains 6 digits
 * separated by semicolons
 * @property startValue "1.25" by default, it should contain
 * string representation of double value
 * @see DEFAULT_TARGET_POINT
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class FileSettings(
    val target: String,
    val defaults: Boolean,
    val pattern: String,
    val startValue: String
)
