package calculator.model

/**
 * Errors data information.
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class ExaminationError(
    val key: String,
    val value: String
    )
