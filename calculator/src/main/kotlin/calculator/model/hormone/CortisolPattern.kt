package calculator.model.hormone

import calculator.functions.counter.Pattern
import java.util.function.Supplier

/**
 * @property pattern "2;3;3;7;2;0"
 */
class CortisolPattern(
    totals: Int,
    NSBs: Int,
    Zeros: Int,
    length: Int,
    repeats: Int,
    startValue: Double,
    points: List<Point>,
    controlPoints: Int)
    : HormonePattern(
    totals,
    NSBs,
    Zeros,
    length,
    repeats,
    startValue,
    points,
    controlPoints), Pattern {
    constructor(): this(2,3,3,7,2,1.25, listOf(),0) {
        create()
    }

    fun create(): CortisolPattern = make(
        Supplier { mutableListOf(2, 3, 3, 7, 2, 0) },
        Supplier { 1.25 }) as CortisolPattern

}
