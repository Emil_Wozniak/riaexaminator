package calculator.model.hormone

enum class PointType {
    T, NSB, ZERO, K
}
