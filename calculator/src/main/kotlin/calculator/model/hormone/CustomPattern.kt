package calculator.model.hormone

import calculator.constants.ROW_SEPARATOR
import calculator.functions.counter.Pattern
import java.util.*
import java.util.function.Supplier
import kotlin.streams.toList

/**
 * @property pattern for example "2;3;3;7;2;0"
 */
class CustomPattern(
    totals: Int,
    NSBs: Int,
    Zeros: Int,
    length: Int,
    repeats: Int,
    startValue: Double,
    points: List<Point>,
    controlPoints: Int)
    : HormonePattern(
    totals,
    NSBs,
    Zeros,
    length,
    repeats,
    startValue,
    points,
    controlPoints), Pattern {
    constructor(): this(0,0,0,0,0,0.0, listOf(),0)

    fun create(pattern: String, startValue: Double): CustomPattern {
        val patternValues = Arrays
            .stream(pattern.split(ROW_SEPARATOR.toRegex()).toTypedArray())
            .map { Integer.valueOf(it) }
            .toList()
            .toMutableList()
        return make(Supplier { patternValues }, Supplier { startValue }) as CustomPattern
    }
}
