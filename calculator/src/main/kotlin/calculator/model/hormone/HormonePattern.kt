package calculator.model.hormone

/**
 * @author emil.wozniak.591986@gmail.com
 * @property totals
 * @property NSBs non specific binding values
 * @property Zeros amount of blind points on standard
 * @property length amount of expected standard points
 * @property repeats amount of repeats of standard point
 * @property startValue value of first point of the standard pattern
 * @property points list of generated points
 * @since 15.06.2020
 * example standard pattern:
 * [1.25, 1.25, 2.5, 2.5, 5.0, 5.0, 10.0, 10.0, 20.0, 20.0, 40.0, 40.0, 80.0, 80.0]
 * length = 7
 * repeats = 2
 * startValue = 1.25
 */
abstract class HormonePattern(
    var totals: Int,
    var NSBs: Int,
    var Zeros: Int,
    var length: Int,
    var repeats: Int,
    var startValue: Double,
    var points: List<Point>,
    var controlPoints: Int
)

/**
 * @property name PointType name
 * @property value
 * @see PointType
 */
class Point(
    val name: String,
    val value: Double
)
