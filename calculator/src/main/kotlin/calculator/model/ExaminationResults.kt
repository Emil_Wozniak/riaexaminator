package calculator.model

import calculator.model.results.BaseGraph
import calculator.model.results.CurvePoint
import calculator.model.results.ExaminationPoint

/**
 * All types of results container.
 *
 * @property title filename
 * @property patternTitle default or provided by [FileSettings] pattern name
 * @property controlCurve Calibration Curve points
 * @property resultPoints results of calculation of the samples
 * @property graph properties required to draw graph
 * @property errors information about failed fields
 *
 * @since 16.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class ExaminationResults(
        var id: Long? = null,
        var title: String,
        val patternTitle: String,
        val controlCurve: List<CurvePoint?>,
        val resultPoints: List<ExaminationPoint?>,
        val graph: BaseGraph?,
        val errors: List<ExaminationError?>)
