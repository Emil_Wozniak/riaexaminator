package calculator.constants

const val SHORT_FILE = "Uploaded file is too short"
const val CURVE_POINT_SUCCESS = "Curve Flagged: {}"
const val RESULT_POINT_SUCCESS = "Result points NGs set successfully"
const val CURVE_POINT_FAILED = "Curve flagging failed with: "
const val RESULT_POINT_FAILED = "Couldn't set Result points NGs with cause: "
const val DEFAULT_COLUMN = 3
const val DEFAULT_TOTAL = 2
const val DEFAULT_NSB = 3
const val DEFAULT_ZERO = 3
