@file:Suppress("SpellCheckingInspection")

package calculator.constants

import calculator.constants.Ansi.*

const val ONE_ARG = COLOR.GREEN + " {} " + Ansi.RESET
const val ERROR_ONE_ARG = COLOR.RED + " {} " + Ansi.RESET

const val VISITOR_ERROR_MSG = "encourage error on "
const val DIMENSION_SUCCESS = "T: $ONE_ARG | Zero: $ONE_ARG | NSB: $ONE_ARG | N - O: $ONE_ARG "
const val TO_PERCENT = 100.0
const val DIXON_TEST_BORDER_VALUE = 10
const val SPREAD_COUNTER = "SpreadCounter failed with cause: "

const val SUCCESS = " finished with:"
const val FAILED = " failed with:"

const val NG = "Count nanogram values"
const val READ_METER = "Reading Meter"
const val CORRELATION = "Correlation"
const val LOG_DOSE = "Logarithm Dose"
const val REGRESSION_A = "Regression Parameter A"
const val REGRESSION_B = "Regression Parameter B"
const val LOG_REAL_ZERO = "Logarithm Real Zero"
const val BINDING_PERCENT = "Binding percentage"
const val ZERO_BINDING_PERCENT = "Zero binding percentage"

const val POINTS = "points"
const val READ_METER_SUCCESS = READ_METER + SUCCESS + ONE_ARG + POINTS
const val NG_SUCCESS = NG + SUCCESS + ONE_ARG + "results"
const val REGRESSION_A_SUCCESS = REGRESSION_A + SUCCESS + ONE_ARG
const val CORRELATION_SUCCESS = CORRELATION + SUCCESS + ONE_ARG
const val LOG_DOSE_SUCCESS = LOG_DOSE + SUCCESS + ONE_ARG
const val LOG_REAL_ZERO_SUCCESS = LOG_REAL_ZERO + ONE_ARG
const val BINDING_PERCENT_SUCCESS = BINDING_PERCENT + SUCCESS + ONE_ARG + POINTS
const val ZERO_BINDING_PERCENT_SUCCESS = ZERO_BINDING_PERCENT + SUCCESS + ONE_ARG
const val ZERO_BINDING_PERCENT_WARN = ZERO_BINDING_PERCENT + SUCCESS + ERROR_ONE_ARG
const val REGRESSION_B_SUCCESS = REGRESSION_B + SUCCESS + ONE_ARG

const val NG_FAILED = NG + FAILED + ONE_ARG + ONE_ARG
const val CORRELATION_FAILED = CORRELATION + FAILED
const val REGRESSION_B_FAILED = REGRESSION_B + FAILED
const val REGRESSION_A_FAILED = REGRESSION_A + FAILED
const val LOG_REAL_ZERO_FAILED = LOG_REAL_ZERO + FAILED
const val BINDING_PERCENT_FAILED = BINDING_PERCENT + FAILED
const val LOG_DOSE_FAILED = LOG_DOSE + FAILED
const val READ_METER_FAILED = READ_METER + FAILED
const val NG_INFINITE_OR_NAN = "Found ∞ or NaN points: ${COLOR.YELLOW}[{}]${Ansi.RESET}"

const val EMPTY = ""
const val PATTERN_PART = "Name: COPY_OF_H-3_"
const val COLUMN_SPLICER = "\t"
const val ROW_SEPARATOR = ";"
const val TXT = "txt"
const val NEGATIVE_ONE = "-1"
const val DEFAULT_TARGET_POINT = " \tUnk"
const val FILENAME_PART = "C:\\mbw\\results\\"
const val SUCCESS_MSG = "Finished with: $ONE_ARG lines"
const val FILE_NAME = "Detect file:$ONE_ARG"

const val TXT_EXTRACTOR_ERROR = "${COLOR.RED}Row does not contain enough amount of column:${Ansi.RESET} {} "

