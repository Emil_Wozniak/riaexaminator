package calculator.strategy

import calculator.BaseCounter
import calculator.Counter
import calculator.factory.ResultsFactory
import calculator.factory.ResultsFactoryImpl
import calculator.functions.extractor.DataExtractor
import calculator.model.FileSettings
import calculator.model.hormone.CortisolPattern
import calculator.model.hormone.CustomPattern
import calculator.model.hormone.HormonePattern
import java.util.function.Function

/**
 * Provides methods to resolve used type of [calculator.model.hormone.HormonePattern]
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
open class PatternResolver(private val extractor: DataExtractor) : PatternStrategy {

    override fun resolve(settings: FileSettings): ResultsFactory = analysisFactory.apply(settings)

    /**
     * Takes [FileSettings.defaults] value, if it's true then returns value of
     * [PatternResolver.isCortisolPattern] else returns value of
     * [PatternResolver.isCustomPattern].
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private val analysisFactory: Function<FileSettings, ResultsFactory> =
        Function { settings: FileSettings ->
            if (settings.defaults) isCortisolPattern(settings)
            else isCustomPattern(settings)
        }

    /**
     * instantiates [CortisolPattern] and [Counter] with instantiated type of
     * [HormonePattern] then instantiates and returns [ResultsFactoryImpl].
     *
     * @param settings object received from ExaminationService
     * @return instance of [ResultsFactoryImpl]
     * @see CortisolPattern
     * @see Counter
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    override fun isCortisolPattern(settings: FileSettings): ResultsFactory {
        val pattern: CortisolPattern = CortisolPattern().create()
        val cortisolCounter: Counter = BaseCounter(pattern)
        return ResultsFactoryImpl(pattern, cortisolCounter, extractor)
    }

    /**
     * instantiates [CustomPattern] and [Counter]  with instantiated type of
     * [HormonePattern] then instantiates and returns [ResultsFactoryImpl].
     *
     * @param settings object received from ExaminationService
     * @return instance of [ResultsFactoryImpl]
     * @see CustomPattern
     * @see Counter
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    override fun isCustomPattern(settings: FileSettings): ResultsFactory {
        val (_, _, pattern, startValue) = settings
        val hormone: HormonePattern = CustomPattern().create(pattern, startValue.toDouble())
        val customCounter: Counter = BaseCounter(hormone)
        return ResultsFactoryImpl(hormone, customCounter, extractor)
    }
}
