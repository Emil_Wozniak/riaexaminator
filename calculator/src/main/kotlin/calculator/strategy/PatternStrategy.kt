package calculator.strategy

import calculator.factory.ResultsFactory
import calculator.model.FileSettings

/**
 * Provides methods to resolve [ResultsFactory]
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface PatternStrategy {
    /**
     * @param settings object received from ExaminationService
     * @return instance of [ResultsFactory]
     * @see calculator.factory.FileAnalyzer
     */
    fun resolve(settings: FileSettings): ResultsFactory

    /**
     * @param settings object received from ExaminationService
     * @return instance of CortisolPatternFactory
     */
    fun isCortisolPattern(settings: FileSettings): ResultsFactory

    /**
     * @param settings object received from ExaminationService
     * @return instance of CustomPatternFactory
     */
    fun isCustomPattern(settings: FileSettings): ResultsFactory
}
