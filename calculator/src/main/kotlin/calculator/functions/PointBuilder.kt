package calculator.functions

import calculator.model.hormone.Point
import calculator.model.hormone.PointType
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.IntStream
import kotlin.streams.toList

interface PointBuilder {
    /**
     * @param patternValues each return entity values
     * @return list of [Point] in range of
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    fun build(patternValues: List<Int>, startValue: Double): List<Point> {
        val points: MutableList<Point> = mutableListOf()
        points.addAll(createPoint(patternValues[0], PointType.T))
        points.addAll(createPoint(patternValues[1], PointType.NSB))
        points.addAll(createPoint(patternValues[2], PointType.ZERO))
        points.addAll(createStandardPattern(patternValues, startValue))
        points.addAll(createPoint(patternValues[5], PointType.K))
        return points
    }

    private fun createPoint(amount: Int, type: PointType): List<Point> {
        return IntStream
            .range(0, amount)
            .mapToObj { Point(type.name, 0.0) }
            .toList()
    }

    private fun createStandardPattern(patternValues: List<Int>, startValue: Double): List<Point> {
        val points = populatePoints(patternValues[3], startValue)
        return handleDuplicates(patternValues[4], points)
            .map { Point(it.toString(), it) }
            .toList()
    }

    /**
     * @param length     amount of standard points
     * @param startValue the lowest value of standard pattern
     * @return list of standard hormone values
     *
     *
     * takes defined [calculator.model.hormone.HormonePattern.length] and
     * [calculator.model.hormone.HormonePattern.startValue],
     * then generates each next value for standard in order that each next should be `2x startValue`
     * @author emil.wozniak.591986@gmail.com
     * @see calculator.model.hormone.HormonePattern.length
     * @see calculator.model.hormone.HormonePattern.startValue
     * @receiver calculator.model.hormone.HormonePattern
     *
     * @since 14.06.2020
     * @return Hormone pattern points
     */
    private fun populatePoints(length: Int?, startValue: Double): List<Double> {
        val atomic = AtomicInteger()
        atomic.set(1)
        return IntStream
            .range(0, length!!)
            .mapToObj { if (it == 0) 1 else atomic.get() + atomic.get() }
            .peek { atomic.set(it!!) }
            .map { it * startValue }
            .toList()
    }

    /**
     * @param repeats amount of repeats of the standard point
     * @param points  list of points
     * @return list of points which each Point will have requested amount of duplicates
     * Process will be skipped if repeats equals 1 or less
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    private fun handleDuplicates(repeats: Int, points: List<Double>): List<Double> {
        return if (repeats > 1) points
            .stream()
            .map { duplicate(repeats, it) }
            .flatMap { it.stream() }
            .toList()
            .toMutableList()
        else points
    }

    /**
     * @param repeats amount of repeats of the standard point
     * @param value   current duplicated value
     * @return list of same values
     * @author emil.wozniak.591986@gmail.com
     * @since 15.01.2020
     */
    private fun duplicate(repeats: Int, value: Double): List<Double> {
        return IntStream
            .range(0, repeats)
            .mapToObj { value }
            .toList()
    }
}
