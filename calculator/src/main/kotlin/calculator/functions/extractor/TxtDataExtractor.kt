package calculator.functions.extractor

import calculator.constants.COLUMN_SPLICER
import calculator.constants.NEGATIVE_ONE
import calculator.constants.TXT_EXTRACTOR_ERROR
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
class TxtDataExtractor : DataExtractor {
    companion object {
        val log: Logger = LoggerFactory.getLogger(this::class.java.name)
    }

    /**
     * Extracts [calculator.model.results.ExaminationResult.cpm] from txt file.
     * It will split row to the list of string and try find
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     * @return list of string, where each
     */
    override fun getColumn(line: String, column: Int): String =
        line.split(COLUMN_SPLICER).toList().run {
            Try
                .of { get(column) }
                .onFailure { log.error(TXT_EXTRACTOR_ERROR, it) }
                .getOrElse { NEGATIVE_ONE }
        }
}
