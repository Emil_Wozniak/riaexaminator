package calculator.functions.extractor

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface DataExtractor {
    /**
     * Extracts [calculator.model.results.ExaminationResult.cpm] from txt file.
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun getColumn(line: String, column: Int): String
}
