package calculator.functions

interface ValueValidator {
    /**
     * @param point      value that will be checked
     * @param methodName name of the method that checks value
     * @return while point value is NaN or Infinite point value will be changed to 0.0, in other way returns original value
     * @author emil.wozniak.591986@gmail.com
     * @since 22.01.2020
     */
    fun avoidNaNsOrInfinite(point: Double, methodName: String): Double =
        if (point.isNaN() || point.isInfinite()) 0.0
        else point
}
