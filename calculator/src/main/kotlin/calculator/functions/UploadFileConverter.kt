package calculator.functions

import calculator.constants.CORTISOL_STANDARD
import calculator.constants.DEFAULT_TARGET_POINT
import calculator.constants.EMPTY
import calculator.model.ExaminationFile
import calculator.model.FileSettings
import org.springframework.web.multipart.MultipartFile
import java.util.function.BiFunction
import javax.validation.Valid
import javax.validation.constraints.NotNull

/**
 * @param file     uploaded file wrapped on ExaminationFile
 * @param settings values to determine HormonePattern
 * @return instance of [ExaminationFile]
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
fun convertFile(
    file: @Valid @NotNull MultipartFile,
    settings: @Valid FileSettings
): ExaminationFile = settings.run {
    val target = extractValue.apply(target, DEFAULT_TARGET_POINT)
    val pattern = extractValue.apply(pattern, CORTISOL_STANDARD)
    ExaminationFile(file, target, pattern, defaults)
}

/**
 * in other way a target value.
 */
private val extractValue = BiFunction { target: String?, defaultValue: String ->
    if (target == null || target == EMPTY) defaultValue
    else target
}
