package calculator.functions.analyze

import calculator.Counter
import calculator.constants.DEFAULT_COLUMN
import calculator.factory.ResultsFactory
import calculator.functions.counter.PointAverage
import calculator.functions.extractor.DataExtractor
import calculator.model.CurveDimensions
import calculator.model.hormone.HormonePattern
import calculator.model.results.CurvePoint
import calculator.model.results.ExaminationPoint

/**
 * Is responsible for analyze data and creates instances of ExaminationPoint.
 *
 * @param controlCurve instance containing List<CurvePoint>] is provided for calculation
 * @param withCurve false by default, while true it will perform create Curve Points
 * @see ResultsFactory
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
internal open class PointsAnalyzer(
    private val controlCurve: MutableList<CurvePoint>,
    private val extractor: DataExtractor,
    private val withCurve: Boolean = false
) :
    Analyzer<ExaminationPoint>, Dimensions, FileName,
    PatternData, Metadata, MatchCpm, PointsFlag,
    MeterReader, NanogramCounter, PointsCreator, PointAverage {
    /**
     * @param counter instance provided by PatternFactory.
     * @param pattern instance of [HormonePattern], which is provided from PatternFactory.
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    override fun analyze(
        data: List<String>,
        counter: Counter,
        pattern: HormonePattern,
        columnNumber: Int?
    ): List<ExaminationPoint> = kotlin.run {
        val dimensions = dimension(pattern)
        recreateCurve(counter, dimensions)
        val metadata = metadata(data)
        val filename = filename(data)
        val patternData = patternData(data)
        val cpms = cpms(extractor, pattern, metadata, columnNumber ?: DEFAULT_COLUMN)
        val length = cpms.size
        val flags = flags(cpms, dimensions, controlCurve)
        val ng = nanograms(counter, cpms)
        val avg = avg(ng)
        points(
            dimensions,
            counter,
            pattern,
            length,
            filename,
            patternData,
            cpms,
            flags,
            ng,
            avg
        )
    }

    private fun recreateCurve(counter: Counter, dimensions: CurveDimensions) {
        if (this.withCurve) assignMeterToCurve(
            controlCurve,
            mutableListOf(),
            mutableListOf(),
            counter,
            dimensions
        )
    }
}
