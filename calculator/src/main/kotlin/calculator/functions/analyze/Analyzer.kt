package calculator.functions.analyze

import calculator.Counter
import calculator.model.hormone.HormonePattern
import calculator.model.results.ExaminationResult

interface Analyzer<ER : ExaminationResult> {
    fun analyze(data: List<String>, counter: Counter, pattern: HormonePattern, columnNumber: Int? = null): List<ER>
}
