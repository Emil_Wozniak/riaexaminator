package calculator.functions.analyze

interface FileName {
    fun filename(data: List<String>): String =
        data[0].trim { it <= ' ' }
}
