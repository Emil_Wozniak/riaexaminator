package calculator.functions.analyze

import calculator.model.CurveDimensions
import calculator.model.results.CurvePoint
import java.util.stream.IntStream
import kotlin.streams.toList

interface PointsFlag {

    /**
     * [calculator.model.CurveDimensions.zeroStart] to [CurveDimensions.nsbsEnd], then
     * checks if each value from [cpms] should be flagged. Finally assigns return list.
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun flags(cpms: List<Int>, dimensions: CurveDimensions, controlCurve: List<CurvePoint>): List<Boolean> =
        cpms
            .map {
                val separatePart = separatePart(controlCurve, dimensions)
                flagCondition(it, separatePart)
            }
            .toList()

    /**
     * @param cpm           examination point cpm
     * @param controlCurves control points
     * @return true if cpm value is greater than curve first point
     * or less than last curve point, or false in the other case
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun flagCondition(
        cpm: Int,
        controlCurves: List<CurvePoint>): Boolean =
        cpm > controlCurves[controlCurves.size - 1].cpm || cpm < controlCurves[0].cpm

    private fun separatePart(curve: List<CurvePoint>, dimensions: CurveDimensions):
        List<CurvePoint> =
        IntStream
            .range(dimensions.zeroStart(), dimensions.nsbsEnd())
            .boxed()
            .map { curve[it] }
            .toList()
}
