package calculator.functions.analyze

interface PatternData {

    fun patternData(data: List<String>): String =
        data[1].trim { it <= ' ' }
}
