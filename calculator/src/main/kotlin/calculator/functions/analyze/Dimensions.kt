package calculator.functions.analyze

import calculator.model.CurveDimensions
import calculator.model.hormone.HormonePattern

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface Dimensions {

    fun dimension(pattern: HormonePattern): CurveDimensions =
        CurveDimensions(pattern.totals, pattern.NSBs,pattern.Zeros)
}
