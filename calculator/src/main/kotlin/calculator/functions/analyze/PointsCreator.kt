package calculator.functions.analyze

import calculator.Counter
import calculator.model.CurveDimensions
import calculator.model.hormone.HormonePattern
import calculator.model.results.ExaminationPoint
import java.util.stream.IntStream
import kotlin.streams.toList

internal interface PointsCreator {
    fun points(
            dimensions: CurveDimensions,
            counter: Counter,
            pattern: HormonePattern,
            length: Int,
            filename: String,
            patternData: String,
            cpms: List<Int>,
            flags: List<Boolean>,
            ng: List<Double>,
            avg: List<Double>
    ): List<ExaminationPoint> = IntStream
            .range(pattern.points.size, length)
            .boxed()
            .map {
                ExaminationPoint(
                        average = avg[it],
                        identifier = filename,
                        pattern = patternData,
                        probeNumber = it,
                        position = "$it",
                        cpm = cpms[it],
                        flagged = flags[it],
                        ng = "${ng[it]}")
            }
            .toList()
}
