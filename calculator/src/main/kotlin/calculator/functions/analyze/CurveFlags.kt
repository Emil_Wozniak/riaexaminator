package calculator.functions.analyze

import calculator.constants.Ansi
import calculator.constants.Ansi.COLOR
import calculator.constants.CURVE_POINT_FAILED
import calculator.constants.CURVE_POINT_SUCCESS
import calculator.counter.SpreadCounter
import calculator.model.CurveDimensions
import calculator.model.hormone.HormonePattern
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.stream.IntStream
import kotlin.streams.toList

val log: Logger = LoggerFactory.getLogger(CurveFlags::javaClass.name)

interface CurveFlags {
    fun flags(dimensions: CurveDimensions, pattern: HormonePattern, cpms: List<Int>): List<Boolean> =
        Try
            .of {
                IntStream
                    .range(0, pattern.points.size)
                    .boxed()
                    .map { getFlag(it, dimensions, cpms) }
                    .toList()
            }
            .onSuccess {
                val logs = it.map(this::collectResults).toList()
                log.info(CURVE_POINT_SUCCESS, logs)
            }
            .onFailure { log.error(CURVE_POINT_FAILED + it.message) }
            .getOrElse { ArrayList() }

    fun collectResults(it: Boolean) = if (it) "${COLOR.RED}${it}${Ansi.RESET}"
    else "${COLOR.GREEN}${it}${Ansi.RESET}"

    @Suppress("SpellCheckingInspection")
    fun getFlag(i: Int, dimensions: CurveDimensions, cpms: List<Int>): Boolean =
        dimensions.run {
            val zerosFlags = countFlags(cpms, zeroStart(), O)
            val nsbsFlags = countFlags(cpms, zeroEnd(), N)
            return if (i < zeroStart()) false
            else if (i == zeroStart() || i < zeroEnd()) zerosFlags[i - zeroStart()]
            else if (i < nsbsEnd()) nsbsFlags[i - zeroEnd()]
            else false
        }

    fun countFlags(cpms: List<Int>, from: Int, to: Int): List<Boolean> =
        SpreadCounter().isSpread(cpms.drop(from).take(to).toList())
}
