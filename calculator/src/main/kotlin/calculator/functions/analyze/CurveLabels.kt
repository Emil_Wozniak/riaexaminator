package calculator.functions.analyze

import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import io.vavr.control.Try

interface CurveLabels {
    fun labels(
        pattern: HormonePattern,
        errors: MutableList<ExaminationError>): List<String> =
        Try.of {
            pattern.points
                .map { it.name }
                .toList()
        }.onFailure {
            errors.add(ExaminationError(
                CurveLabels::javaClass.name,
                it.message!!))
        }.getOrElse { listOf() }
}
