package calculator.functions.analyze

import calculator.model.results.CurvePoint
import java.util.stream.IntStream
import kotlin.streams.toList

class ControlCurveCreator(
    private val size: Int,
    private val fileName: String,
    private val patternData: String,
    private val labels: List<String>,
    private val cpms: List<Int>,
    private val flags: List<Boolean>) {

    fun createPoints(): List<CurvePoint> = IntStream
        .range(0, size)
        .boxed()
        .map {
            CurvePoint(
                identifier = fileName,
                pattern = patternData,
                probeNumber = it,
                position = labels[it],
                cpm = cpms[it],
                flagged = flags[it],
                meterRead = 0.0,
                ng = "")
        }
        .toList()
}
