package calculator.functions.analyze

import calculator.Counter
import calculator.model.CurveDimensions
import calculator.model.ExaminationError
import calculator.model.results.CurvePoint
import java.util.stream.IntStream
import kotlin.streams.toList

interface MeterReader {

    fun assignMeterToCurve(
        controlCurve: MutableList<CurvePoint>,
        errors: MutableList<ExaminationError>,
        meterReads: MutableList<Double>,
        counter: Counter,
        dimensions: CurveDimensions
    ): List<CurvePoint> {

        setMeterRead(controlCurve, counter, meterReads, errors)
        return IntStream
            .range(0, controlCurve.size)
            .boxed()
            .map { isPointInScope(dimensions.all(), it, controlCurve, meterReads) }
            .toList()
    }

    /**
     * performs [CurvePoint.ng] on each elements of
     * {@param controlCurves}
     * and assign value of [Counter.errors]
     * if [Counter.errors] is not null and is not empty.
     */
    private fun setMeterRead(
        curve: List<CurvePoint>,
        counter: Counter,
        meterReads: MutableList<Double>,
        errors: MutableList<ExaminationError>
    ) {
        meterReads.addAll(counter.countMeters(curve))
        if (counter.errors.isNotEmpty()) {
            errors.addAll(counter.errors)
        }
    }

    /**
     * gets [CurvePoint] as point from [.controlCurve]
     * and if value of {@param i} is greater of equals than
     * {@param from} it will assign [.meterReads].
     */
    private fun isPointInScope(
        from: Int, i: Int,
        controlCurve: MutableList<CurvePoint>,
        meterReads: MutableList<Double>
    ): CurvePoint {
        val point: CurvePoint = controlCurve[i]
        if (i >= from) point.meterRead = (meterReads[i - from])
        return point
    }
}
