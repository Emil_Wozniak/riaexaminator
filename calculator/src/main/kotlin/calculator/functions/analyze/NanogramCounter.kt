package calculator.functions.analyze

import calculator.Counter
import calculator.constants.RESULT_POINT_FAILED
import calculator.constants.RESULT_POINT_SUCCESS
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.stream.IntStream
import kotlin.streams.toList

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface NanogramCounter {
    companion object {
        val log: Logger = LoggerFactory.getLogger(NanogramCounter::class.java.name)
    }

    /**
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    fun nanograms(counter: Counter, cpms: List<Int>): List<Double> = counter.run {
        Try.of { countNGs(cpms.map { it.toDouble() }.toList()) }
            .onSuccess { log.info(RESULT_POINT_SUCCESS) }
            .onFailure { log.error(RESULT_POINT_FAILED + it.cause) }
            .getOrElse {
                IntStream
                    .range(0, cpms.size)
                    .boxed()
                    .map { it.toDouble() }
                    .toList()
            }
    }
}
