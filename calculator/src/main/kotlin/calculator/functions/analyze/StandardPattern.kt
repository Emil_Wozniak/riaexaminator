package calculator.functions.analyze

import calculator.Counter
import calculator.model.hormone.HormonePattern

interface StandardPattern {
    fun standardze(counter: Counter, pattern: HormonePattern) {
        counter.calculateDoseLogarithm(pattern)
    }
}
