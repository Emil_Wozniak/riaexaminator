package calculator.functions.analyze

import calculator.constants.SHORT_FILE
import calculator.errors.ControlCurveException
import calculator.functions.extractor.DataExtractor
import calculator.model.hormone.HormonePattern
import io.vavr.control.Try
import java.util.*
import kotlin.streams.toList

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 * @exception ControlCurveException if return values is less than pattern size
 * @return list of count per minute values extracted from file
 */
interface MatchCpm {
    fun cpms(
        extractor: DataExtractor,
        pattern: HormonePattern,
        metadata: List<String>,
        columnNr: Int
    ): List<Int> = Try
        .of {
            metadata
                .stream()
                .skip(1)
                .takeWhile { it.startsWith(" \tUnk") }
                .map { extractor.getColumn(it, columnNr) }
                .map { Integer.valueOf(it) }
                .toList()
        }
        .map { it -> it }
        .getOrElse { ArrayList<Int>() }
}
