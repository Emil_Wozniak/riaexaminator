package calculator.functions.analyze

interface Metadata {
    fun metadata(data: List<String>): List<String> =
        data.drop(2).toList()
}
