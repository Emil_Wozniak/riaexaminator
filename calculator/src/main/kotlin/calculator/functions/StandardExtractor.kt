package calculator.functions

import calculator.model.CalibrationCurveValues
import calculator.model.results.CurvePoint

interface StandardExtractor {
    /**
     * Standard storing CMP and Flag
     * tableC && tableG -> Control Curve CMP
     */
    fun extractStandard(curve: List<CurvePoint>, dimensions: CalibrationCurveValues): MutableList<Int> {
        return curve
            .drop(dimensions.standardStart)
            .take(dimensions.standardEnd)
            .map { it.cpm }
            .toMutableList()
    }
}
