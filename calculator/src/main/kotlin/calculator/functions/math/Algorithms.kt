package calculator.functions.math

import org.apache.commons.math3.util.Pair
import org.apache.commons.math3.util.Precision.round
import kotlin.math.pow
import kotlin.streams.toList

interface Algorithms

fun count(values: List<Double>): Double = values.size.toDouble()

fun multiplyPairElements(pair: Pair<Double, Double>): Double =
    pair.run { first * second }

fun listRoundTo1(table: List<Double>): List<Double> = table
    .stream()
    .map { round(it, 3) }
    .toList()

fun sum(values: List<Double?>): Double = values
    .stream()
    .mapToDouble { round(it!!, 2) }
    .sum()
    .let { round(it, 2) }

fun sumRound1(values: List<Double>): Double = values
    .stream()
    .mapToDouble { limitPrecision(it.toString(), 1) }
    .sum()

fun limitPrecision(dblAsString: String, digitsAfter: Int): Double =
    10.0.pow(digitsAfter.toDouble()).toInt().let {
        (dblAsString.toDouble() * it).toLong().toDouble() / it
    }

@Suppress("SpellCheckingInspection")
fun sumsq(values: List<Double?>): Double = values
    .map { it!!.toDouble().pow(2.0) }
    .sum()

fun powerBy10(it: Double?): Double = 10.0.pow(it!!)

