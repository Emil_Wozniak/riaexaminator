package calculator.functions

import calculator.Counter
import calculator.constants.DEFAULT_COLUMN
import calculator.factory.ResultsFactory
import calculator.functions.analyze.*
import calculator.functions.extractor.DataExtractor
import calculator.functions.extractor.TxtDataExtractor
import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import calculator.model.results.CurvePoint

/**
 * @param errors container for [Counter] errors
 * @param curvePoints container for list of the results
 * @see ResultsFactory
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
open class CurveAnalyzer(
    private val errors: MutableList<ExaminationError>,
    private val curvePoints: MutableList<CurvePoint>,
    private val extractor: DataExtractor
) :
    Analyzer<CurvePoint>,
    Dimensions, FileName,
    PatternData, Metadata,
    CurveLabels, MatchCpm,
    CurveFlags, MeterReader {

    /**
     * Setup params required to instantiate [CurvePoint].
     *
     * @param counter instance provided by [ResultsFactory].
     * @param pattern instance of [HormonePattern], which is provided from [ResultsFactory].
     * @param data extracted strings from uploaded file
     * @param columnNumber when null [TxtDataExtractor] will point column 3 of data results
     * as default in other way it will override default.
     *
     * @return list of CurvePoint instances.
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    override fun analyze(data: List<String>, counter: Counter, pattern: HormonePattern, columnNumber: Int?):
            List<CurvePoint> {
        val dimensions = dimension(pattern)
        val metadata = metadata(data)
        val cpms = cpms(extractor, pattern, metadata, columnNumber ?: DEFAULT_COLUMN)
        val fileName = filename(data)
        val patternData = patternData(data)
        val labels = labels(pattern, errors)
        val flags = flags(dimensions, pattern, cpms)
        val controlCurve = ControlCurveCreator(
            pattern.points.size,
            fileName,
            patternData,
            labels,
            cpms,
            flags
        )
            .createPoints()
            .toMutableList()

        val curveWithMeters = assignMeterToCurve(
            controlCurve,
            errors,
            mutableListOf(),
            counter,
            dimensions
        )

        /* add result points to higher level container */
        curvePoints.addAll(curveWithMeters)

        return curveWithMeters
    }
}
