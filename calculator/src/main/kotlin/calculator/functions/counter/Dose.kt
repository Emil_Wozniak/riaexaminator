package calculator.functions.counter

import calculator.constants.LOG_DOSE
import calculator.constants.LOG_DOSE_FAILED
import calculator.constants.LOG_DOSE_SUCCESS
import calculator.functions.ValueValidator
import calculator.functions.math.Algorithms
import calculator.model.CalibrationCurveValues
import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import io.vavr.control.Try
import org.apache.commons.math3.util.Precision.round
import org.slf4j.LoggerFactory
import kotlin.math.log10

interface Dose : ValueValidator, Algorithms {
    companion object {
        val log = LoggerFactory.getLogger(Dose::class.java)
    }

    /**
     * take double array which contains standardized pattern and
     * performs a logarithmic function for each elements of the array
     */
    fun countDose(
        dimensions: CalibrationCurveValues,
        pattern: HormonePattern,
        errors: MutableList<ExaminationError>
    ): List<Double> =
        Try
            .of {
                pattern
                    .points
                    .asSequence()
                    .drop(dimensions.standardStart)
                    .take(dimensions.standardEnd)
                    .map { it.value }
                    .map { log10(it) }
                    .map { round(it, 3) }
                    .map { round(it, 2) }
                    .map { avoidNaNsOrInfinite(it, LOG_DOSE) }
                    .toList()
            }
            .onSuccess { log.info(LOG_DOSE_SUCCESS, it) }
            .onFailure {
                log.error(LOG_DOSE_FAILED + it.cause)
                errors.add(
                    ExaminationError(
                        Dose::javaClass.name,
                        LOG_DOSE_FAILED + it.message
                    )
                )
            }
            .getOrElse { ArrayList() }
}


