package calculator.functions.counter

import calculator.constants.REGRESSION_A_FAILED
import calculator.constants.REGRESSION_A_SUCCESS
import calculator.functions.math.Algorithms
import calculator.functions.math.sum
import calculator.functions.math.sumRound1
import calculator.model.ExaminationError
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory

interface ParameterA : Algorithms {
    companion object {
        val log: Logger = LoggerFactory.getLogger(ParameterA::class.java)
    }

    /**
     * Excel version:
     * R19 == N20
     * var M25:M40 => logDose
     * var N25:N40 => logarithmRealZero
     * = sum(N25:N40) / count(M25:M40) - N19 => component.getRegressionParameterB() * sum(M25:M40) / count(M25:M40)
     */
    fun countA(
        realZero: List<Double>,
        dose: List<Double>,
        parameterB: Double,
        errors: MutableList<ExaminationError>
    ): Double = Try
        .of { sumBySize(realZero, dose) - paramBTimesDose(parameterB, dose) }
        .onSuccess { log.info(REGRESSION_A_SUCCESS, it) }
        .onFailure {
            log.error(REGRESSION_A_FAILED + it)
            errors.add(
                ExaminationError(
                    ParameterA::javaClass.name,
                    REGRESSION_A_FAILED + it.message
                )
            )
        }
        .getOrElse { 0.0 }

    private fun doseSumByDoseSize(dose: List<Double>): Double = sum(dose) / dose.size

    private fun sumBySize(realZero: List<Double>, dose: List<Double>): Double =
        sumRound1(realZero) / dose.size

    private fun paramBTimesDose(parameterB: Double, dose: List<Double>): Double =
        parameterB * doseSumByDoseSize(dose)

}
