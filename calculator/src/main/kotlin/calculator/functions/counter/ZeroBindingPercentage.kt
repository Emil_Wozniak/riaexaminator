package calculator.functions.counter

import calculator.constants.TO_PERCENT
import calculator.constants.ZERO_BINDING_PERCENT_SUCCESS
import calculator.constants.ZERO_BINDING_PERCENT_WARN
import calculator.functions.math.Algorithms
import calculator.model.CalibrationCurveValues
import io.vavr.control.Try
import org.apache.commons.math3.util.Precision.round
import org.slf4j.Logger
import org.slf4j.LoggerFactory

interface ZeroBindingPercentage : Algorithms {
    companion object {
        val log: Logger = LoggerFactory.getLogger(ZeroBindingPercentage::class.java)
    }

    private fun upTo100(dimensions: CalibrationCurveValues): Double =
        (dimensions.binding.toDouble() * TO_PERCENT)

    private fun byTotal(dimensions: CalibrationCurveValues): Double =
        dimensions.total.toDouble()

    fun zeroBind(dimensions: CalibrationCurveValues): Double = Try
        .of { round(upTo100(dimensions) / byTotal(dimensions), 2) }
        .onSuccess {
            if (it > 20.0) log.info(ZERO_BINDING_PERCENT_SUCCESS, it)
            else log.info(ZERO_BINDING_PERCENT_WARN, it)
        }
        .getOrElse(0.0)
}


