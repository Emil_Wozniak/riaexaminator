package calculator.functions.counter

import calculator.constants.CORRELATION_FAILED
import calculator.constants.CORRELATION_SUCCESS
import calculator.model.hormone.HormonePattern
import io.vavr.concurrent.Future
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation
import org.apache.commons.math3.util.Precision.round
import org.slf4j.LoggerFactory
import java.util.stream.IntStream
import kotlin.math.pow

interface Correlation {
    companion object {
        val log = LoggerFactory.getLogger(Correlation::class.java)
    }

    private fun getControlCurveSize(pattern: HormonePattern): Int =
        pattern.run {
            points.size - totals - NSBs - Zeros - controlPoints
        }

    private fun createCorrelationArrays(
        dose: List<Double>,
        realZero: List<Double>,
        size: Int
    ): Array<DoubleArray> = Array(2) { DoubleArray(size) }
        .let {
            IntStream
                .range(0, size)
                .boxed()
                .forEach { point ->
                    it[0][point] = dose[point]
                    it[1][point] = realZero[point]
                }
            it
        }

    fun findCorrelation(
        pattern: HormonePattern,
        dose: List<Double>, realZero:
        List<Double>
    ): Double {
        val controlCurveSize = getControlCurveSize(pattern)
        return Future
            .of { createCorrelationArrays(dose, realZero, controlCurveSize) }
            .map { PearsonsCorrelation().correlation(it[0], it[1]) }
            .map { (it!!).pow(2.0) }
            .map { round(it!!, 6) }
            .onSuccess { log.info(CORRELATION_SUCCESS, it) }
            .onFailure { log.error(CORRELATION_FAILED + it.cause) }
            .getOrElse(0.0)
    }
}
