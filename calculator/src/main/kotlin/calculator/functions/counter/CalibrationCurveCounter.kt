package calculator.functions.counter

import calculator.constants.DIMENSION_SUCCESS
import calculator.constants.VISITOR_ERROR_MSG
import calculator.model.CalibrationCurveValues
import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import calculator.model.results.CurvePoint
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.Integer.sum
import java.util.stream.IntStream
import kotlin.streams.toList



interface CalibrationCurveCounter {
    companion object {
        val log: Logger = LoggerFactory.getLogger(CalibrationCurveCounter::class.java)
    }

    fun calibrate(
        hormone: HormonePattern,
        curve: MutableList<CurvePoint>,
        errors: MutableList<ExaminationError>
    ): CalibrationCurveValues = Try
        .of {
            val totalsSize: Int = hormone.totals
            val nsbSize: Int = hormone.NSBs + totalsSize
            val zeroSize: Int = hormone.Zeros + nsbSize
            val total = calculateTotals(forPoints(curve, 0, totalsSize))
            val nsbs = calculatePoint(forPoints(curve, totalsSize, nsbSize))
            val zero = calculatePoint(forPoints(curve, nsbSize, zeroSize))
            val binding = zero - nsbs
            val start = standardStart(hormone)
            val end = standardEnd(hormone)
            CalibrationCurveValues(total, nsbs, zero, binding, start, end)
        }
        .onSuccess { it.run { log.info(DIMENSION_SUCCESS, total, zero, nsb, binding) } }
        .onFailure {
            errors.add(
                ExaminationError(
                    CalibrationCurveValues::javaClass.name,
                    VISITOR_ERROR_MSG + it.message
                )
            )
        }
        .getOrElse { CalibrationCurveValues() }

    /**
     * @param totals list of [CurvePoint.cpm]
     * @return sum of each TOTALS values divided by totalsSize
     */
    private fun calculateTotals(totals: List<CurvePoint>): Int =
        totals
            .stream()
            .map { it.cpm }
            .reduce(0) { a: Int, b: Int -> sum(a, b) } / totals.size


    /**
     * controlCurve sublist from [calculator.functions.counter.CalibrationCurveCounter.calibrate]
     * controlCurve, separated by [.forPoints] value calculated by removing flagged elements from
     * controlCurve, then converts to [CurvePoint.cpm] after that sum those values and divides by
     * number of remain elements
     */
    private fun calculatePoint(curve: MutableList<CurvePoint>): Int {
        val flaggedPointCpm = notFlaggedPointCpm(curve)
        val count = pointsCount(curve)
        return flaggedPointCpm / count
    }

    private fun notFlaggedPointCpm(curve: MutableList<CurvePoint>): Int = curve
        .filter { isNotFlagged(it) }
        .map { it.cpm }
        .toList()
        .stream()
        .reduce(0) { a: Int, b: Int -> sum(a, b) }


    private fun pointsCount(curve: MutableList<CurvePoint>): Int =
        curve.filter { isNotFlagged(it) }.count()

    private fun isNotFlagged(point: CurvePoint): Boolean = !point.flagged!!

    private fun forPoints(
        curve: MutableList<CurvePoint>,
        from: Int,
        to: Int
    ): MutableList<CurvePoint> =
        IntStream
            .range(from, to)
            .boxed()
            .map { curve[it] }
            .toList()
            .toMutableList()

    private fun standardStart(pattern: HormonePattern): Int =
        (pattern.totals + pattern.Zeros + pattern.NSBs)

    private fun standardEnd(pattern: HormonePattern): Int =
        (pattern.points.size - pattern.controlPoints)

}

