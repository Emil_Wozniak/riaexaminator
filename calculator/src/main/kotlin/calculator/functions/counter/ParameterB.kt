@file:Suppress("SpellCheckingInspection")

package calculator.functions.counter

import calculator.constants.REGRESSION_B_FAILED
import calculator.constants.REGRESSION_B_SUCCESS
import calculator.functions.math.*
import calculator.model.ExaminationError
import io.vavr.control.Try
import org.apache.commons.math3.util.Pair
import org.apache.commons.math3.util.Precision.round
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.stream.IntStream
import kotlin.math.pow

interface ParameterB : Algorithms {
    companion object {
        val log: Logger = LoggerFactory.getLogger(BindingPercentage::class.java)
    }

    private fun getDoseMultiplyBySumsq(countDose: Double, sumsqSecondFactor: Double): Double =
        (countDose * sumsqSecondFactor)

    private fun getFirstBySecond(firstFactor: Double, secondFactor: Double): Double = firstFactor / secondFactor

    private fun sumProduct(realZero: List<Double>, doseList: List<Double>): Double = IntStream
        .range(0, realZero.size)
        .boxed()
        .map { Pair(doseList[it], listRoundTo1(realZero)[it]) }
        .mapToDouble { multiplyPairElements(it) }
        .map { round(it, 3) }
        .sum()

    private fun multiplyDoseByRealZero(sumDose: Double, sumRealZero: Double): Double =
        sumDose * sumRealZero

    private fun multiplyDoseBySumProduct(countDose: Double, sumProduct: Double): Double =
        countDose * sumProduct

    @Suppress("UnnecessaryVariable")
    private fun createVariables(realZero: List<Double>, doseList: List<Double>): Double =
        kotlin.run {
            val sumProduct = sumProduct(realZero, doseList)
            val countDose = count(doseList)
            val sumDose = sum(doseList)
            val sumRealZero = sum(realZero)
            val sumsqSecondFactor = sumsq(doseList)
            val dosePow2 = (sumDose).pow(2.0)
            val firstFactor =
                multiplyDoseBySumProduct(countDose, sumProduct) - multiplyDoseByRealZero(sumDose, sumRealZero)
            val secondFactor = getDoseMultiplyBySumsq(countDose, sumsqSecondFactor) - dosePow2
            val firstBySecond = getFirstBySecond(firstFactor, secondFactor)
            firstBySecond
        }

    /**
     * Excel version:
     * N19
     * var M25:M40 => logDose
     * var N25:N40 => logarithmRealZero
     *
     *
     * =(COUNT(M25:M40) * SUMPRODUCT(M25:M40;N25:N40) -SUM(M25:M40)*SUM(N25:N40))/(COUNT(M25:M40) * SUMSQ(M25:M40)-(SUM(M25:M40))^2)
     */
    fun countB(
        realZero: List<Double>,
        doseList: List<Double>,
        errors: MutableList<ExaminationError>
    ): Double = Try
        .of { round(createVariables(realZero, doseList), 4) }
        .onSuccess { log.info(REGRESSION_B_SUCCESS, it) }
        .onFailure {
            log.error(REGRESSION_B_FAILED + it.message)
            errors.add(
                ExaminationError(
                    ParameterB::javaClass.name,
                    REGRESSION_B_FAILED + it.message
                )
            )
        }
        .getOrElse(0.0)
}
