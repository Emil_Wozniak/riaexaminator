package calculator.functions.counter

import calculator.functions.PointBuilder
import calculator.model.hormone.CortisolPattern
import calculator.model.hormone.CustomPattern
import calculator.model.hormone.HormonePattern
import java.util.function.Supplier

interface Pattern : PointBuilder {
    fun make(values: Supplier<MutableList<Int>>, value: Supplier<Double>): HormonePattern {
        return pattern(values.get(), value.get())
    }

    private fun pattern(patternValues: List<Int>, startValue: Double): HormonePattern {
        val isCortisol = (startValue) == 1.25 && patternValues == listOf(2, 3, 3, 7, 2, 0)
        return if (isCortisol) cortisol(patternValues, startValue)
        else custom(patternValues, startValue)
    }

    private fun custom(patternValues: List<Int>, startValue: Double): HormonePattern {
        return CustomPattern(
                totals = patternValues[0],
                NSBs = patternValues[1],
                Zeros = patternValues[2],
                length = patternValues[3],
                repeats = patternValues[4],
                startValue = startValue,
                points = build(patternValues, startValue),
                controlPoints = patternValues[5])
    }

    private fun cortisol(patternValues: List<Int>, startValue: Double): HormonePattern {
        return CortisolPattern(
                totals = patternValues[0],
                NSBs = patternValues[1],
                Zeros = patternValues[2],
                length = patternValues[3],
                repeats = patternValues[4],
                startValue = startValue,
                points = build(patternValues, startValue),
                controlPoints = patternValues[5])
    }
}

