package calculator.functions.counter

import calculator.constants.READ_METER
import calculator.constants.READ_METER_FAILED
import calculator.constants.READ_METER_SUCCESS
import calculator.functions.ValueValidator
import calculator.functions.math.Algorithms
import calculator.functions.math.powerBy10
import calculator.model.CalibrationCurveValues
import io.vavr.control.Try
import org.apache.commons.math3.util.Precision.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.function.BiFunction
import kotlin.math.log10

interface MeterRead : ValueValidator, Algorithms {
    companion object {
        val log: Logger = LoggerFactory.getLogger(MeterRead::class.java)
    }

    private fun getReadPoint(): BiFunction<Double, CalibrationCurveValues, Double> =
        BiFunction { point: Double, calibrations: CalibrationCurveValues ->
            val nsbs = calibrations.nsb.toDouble()
            val binding = calibrations.binding.toDouble()
            val realPoint = point - nsbs
            // first part
            val realMultiplyTo100 = realPoint * 100.0
            val first = realMultiplyTo100 / binding
            // second part
            val realMinus100 = realPoint * 100
            val realMinus100ByBinding = realMinus100 / binding
            val second = 100 - realMinus100ByBinding
            first / second
        }

    /**
     * Excel version:
     * LOG() == decimal logarithm | Briggsian logarithm
     * var real_point = (G23 - $I$16)
     * var first_part = (G23 - $I$16) *100 / $J$18
     * var second_part = 100- ((G23 - $I$16) *100)
     * ################ [        first_part        ] / [          second_part            ] ###################
     * result =10^((LOG((G23 - $I$16) * 100 / $J$18 / (100 - (G23 - $I$16) * 100 / $J$18))) - $R$19) / $R$20).
     *
     * @return meter read in picograms for List of Point entities
     * @see Point
     */
    fun countReadMeter(
        standards: List<Int>,
        dimensions: CalibrationCurveValues,
        parameterA: Double,
        parameterB: Double
    ): List<Double> = Try
        .of {
            standards
                .asSequence()
                .map { it.toDouble() }
                .map { getReadPoint().apply(it, dimensions) }
                .map { log10(it) }
                .map { it - parameterA }
                .map { it / parameterB }
                .map { powerBy10(it) }
                .map { avoidNaNsOrInfinite(it, READ_METER) }
                .map { round(it, 2) }
                .toList()
        }
        .onSuccess { log.info(READ_METER_SUCCESS, it.size) }
        .onFailure { log.error(READ_METER_FAILED + it.cause) }
        .getOrElse { listOf() }
}
