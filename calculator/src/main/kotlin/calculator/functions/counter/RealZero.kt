package calculator.functions.counter

import calculator.constants.LOG_REAL_ZERO
import calculator.constants.LOG_REAL_ZERO_FAILED
import calculator.constants.LOG_REAL_ZERO_SUCCESS
import calculator.functions.ValueValidator
import calculator.functions.math.Algorithms
import calculator.model.ExaminationError
import io.vavr.control.Try
import org.apache.commons.math3.util.Precision.round
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import java.util.stream.IntStream
import kotlin.math.log10
import kotlin.streams.toList

interface RealZero : ValueValidator, Algorithms {
    companion object {
        val log: Logger = LoggerFactory.getLogger(RealZero::class.java)
    }

    fun processBindingPercent(i: Int, bindingPercent: List<Double>): Double {
        val point = bindingPercent[i]
        val subtract = 100.0 - point
        return point / subtract
    }

    /**
     * Excel version:
     *
     *
     * Table J = LOG(H23/(100-H23))
     */
    fun countRealZero(
        bindingPercent: List<Double>,
        errors: MutableList<ExaminationError>
    ): List<Double> = Try
        .of {
            IntStream
                .range(0, bindingPercent.size)
                .boxed()
                .map { processBindingPercent(it, bindingPercent) }
                .map { log10(it) }
                .map { round(it, 3) }
                .map { avoidNaNsOrInfinite(it, LOG_REAL_ZERO) }
                .sorted(Comparator.reverseOrder())
                .toList()
        }
        .onSuccess { log.info(LOG_REAL_ZERO_SUCCESS, it) }
        .onFailure {
            log.error(LOG_REAL_ZERO_FAILED + it.message)
            errors.add(
                ExaminationError(
                    key = RealZero::javaClass.name,
                    value = LOG_REAL_ZERO_FAILED + it.message
                )
            )
        }
        .getOrElse(ArrayList())
}

