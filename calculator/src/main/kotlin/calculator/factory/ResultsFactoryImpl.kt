package calculator.factory

import calculator.Counter
import calculator.functions.CurveAnalyzer
import calculator.functions.analyze.PointsAnalyzer
import calculator.functions.extractor.DataExtractor
import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import calculator.model.results.BaseGraph
import calculator.model.results.Coordinates
import calculator.model.results.CurvePoint
import calculator.model.results.ExaminationPoint
import java.util.stream.IntStream
import kotlin.streams.toList

/**
 * @param pattern instance of [HormonePattern]
 * @param counter instance of [Counter] to perform all points calculation
 * @param columnNumber number of column containing metadata for [Counter] calculations
 * @property curvePoints list to store [CurvePoint] instances if Calibration Curve calculations end successfully
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
data class ResultsFactoryImpl(
    val pattern: HormonePattern,
    val counter: Counter,
    val extractor: DataExtractor,
    val columnNumber: Int? = null
) : ResultsFactory {

    private val curvePoints: MutableList<CurvePoint> = mutableListOf()

    /**
     * if any instance of ResultAnalyzer in [ResultsFactoryImpl.createCurve] or
     * [ResultsFactoryImpl.createPoints] all failures will be added to this list.
     */
    private val errors: MutableList<ExaminationError> = mutableListOf()

    override fun createCurve(data: MutableList<String>): List<CurvePoint?> =
        CurveAnalyzer(errors, curvePoints, extractor)
            .analyze(data, counter, pattern, columnNumber)

    /**
     * @param data separated from uploaded file by [calculator.util.FileReader.read]
     * @return instance of {@link PointAnalyzer}
     * @see calculator.util.FileReader
     * @see calculator.strategy.PatternStrategy
     */
    override fun createPoints(data: List<String>): List<ExaminationPoint?> =
        PointsAnalyzer(curvePoints, extractor)
            .analyze(data, counter, pattern, columnNumber)

    /**
     * This method should be call after [ResultsFactoryImpl.createCurve]
     * in other case [Counter]  will not be setup previously and method
     * will case an Exception.
     *
     * @return instance of [BaseGraph]
     */
    override fun createGraph(): BaseGraph = counter.run {
        val coordinates: List<Coordinates> = createCoordinates()
        val r: Double = correlation
        val zeroBindingPercent: Double = zeroBinding
        BaseGraph(coordinates, r, zeroBindingPercent)
    }

    override fun getErrors(): List<ExaminationError> = errors

    override fun patternType(): HormonePattern = pattern

    private fun createCoordinates(): List<Coordinates> = IntStream
        .range(0, defineLimit())
        .mapToObj {
            counter.run {
                val x: Double = dose[it]
                val y: Double = realZero[it]
                Coordinates(x, y)
            }
        }
        .toList()

    private fun defineLimit(): Int = pattern.run {
        (points.size - controlPoints - defineStart())
    }


    private fun defineStart(): Int = pattern.run {
        (totals + Zeros + NSBs)
    }
}
