package calculator.factory

import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import calculator.model.results.BaseGraph
import calculator.model.results.CurvePoint
import calculator.model.results.ExaminationPoint

/**
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface ResultsFactory {

    fun createCurve(data: MutableList<String>): List<CurvePoint?>

    fun createPoints(data: List<String>): List<ExaminationPoint?>

    fun createGraph(): BaseGraph

    fun getErrors(): List<ExaminationError?>

    fun patternType(): HormonePattern
}
