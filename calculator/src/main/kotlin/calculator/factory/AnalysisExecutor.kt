package calculator.factory

import calculator.model.ExaminationResults
import calculator.model.FileSettings
import org.springframework.web.multipart.MultipartFile

interface AnalysisExecutor {
    fun analyze(multipart: MultipartFile, settings: FileSettings): ExaminationResults
}
