package calculator.errors

import java.net.URI

const val CONTROL_CURVE_MSG = "Control Curve failed"
const val GRAPH_MSG = "Can't create graph"

const val ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure"
const val ERR_VALIDATION = "error.validation"
const val PROBLEM_BASE_URL = "https://www.jhipster.tech/problem"
val DEFAULT_TYPE = URI.create("$PROBLEM_BASE_URL/problem-with-message")
val CONSTRAINT_VIOLATION_TYPE = URI.create("$PROBLEM_BASE_URL/constraint-violation")
val INVALID_PASSWORD_TYPE = URI.create("$PROBLEM_BASE_URL/invalid-password")
val EMAIL_ALREADY_USED_TYPE = URI.create("$PROBLEM_BASE_URL/email-already-used")
val LOGIN_ALREADY_USED_TYPE = URI.create("$PROBLEM_BASE_URL/login-already-used")
val EMAIL_NOT_FOUND_TYPE = URI.create("$PROBLEM_BASE_URL/email-not-found")

const val NOT_SUPPORT_TYPE = "File format is not supported"
const val READ_METER_FAIL = "Can't Read Meter"
