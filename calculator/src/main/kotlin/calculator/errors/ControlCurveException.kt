package calculator.errors

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status.CONFLICT
import java.util.*

class ControlCurveException(
    entityName: String,
    errorKey: String
) : AbstractThrowableProblem(
    null,
    CONTROL_CURVE_MSG,
    CONFLICT,
    READ_METER_FAIL,
    null,
    null,
    getAlertParameters(errorKey, entityName)
) {
    override fun getCause(): Exceptional = this
}

private fun getAlertParameters(entityName: String, errorKey: String): Map<String, Any>? {
    val parameters: MutableMap<String, Any> = HashMap()
    parameters["message"] = "error.$errorKey"
    parameters["params"] = entityName
    return parameters
}
