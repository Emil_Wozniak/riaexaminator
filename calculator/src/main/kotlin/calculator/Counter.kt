package calculator

import calculator.model.CalibrationCurveValues
import calculator.model.ExaminationError
import calculator.model.hormone.HormonePattern
import calculator.model.results.CurvePoint

/**
 * Provides methods to perform calculations and get return values.
 * @since 13.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
interface Counter  {

    var dose: List<Double>
    var realZero: List<Double>
    var zeroBinding: Double
    var correlation: Double
    var calibrationCurveValues: CalibrationCurveValues
    val errors: MutableList<ExaminationError>
    var standards: MutableList<Int>
    var paramB: Double
    var paramA: Double
    var bindingPercentage: List<Double>

    fun countMeters(curve: List<CurvePoint>): List<Double>

    fun calculateDoseLogarithm(pattern: HormonePattern): CalibrationCurveValues

    fun countNGs(cpms: List<Double>): List<Double>

}
