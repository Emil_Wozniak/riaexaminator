package calculator.util

import calculator.constants.EMPTY
import calculator.constants.FILENAME_PART
import calculator.constants.PATTERN_PART
import calculator.model.ExaminationFile
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Is responsible for read Txt file content.
 *
 * @since 15.06.2020
 * @author emil.wozniak.591986@gmail.com
 */
class TxtFileReader : FileReader {

    val log: Logger = LoggerFactory.getLogger(TxtFileReader::class.java.name)

    /**
     * Takes instance of {@link ExaminationFile},
     * creates stream from its content,
     * then removes all empty and nulls,
     * and finally appends required metadata,
     * removes empty and returns list.
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    override fun read(data: ExaminationFile): List<String> =
        data.contents.get().run {
            filter { it.isNotBlank() }
                .map { appendFileInfo(it, data) }
                .filter { it.isNotBlank() }
                .toList()
        }

    /**
     * @param line   line of data matched to target start value
     * @param target characteristic point from which data will be received
     * @return if line contains target returns line if not empty string
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun isDataResult(line: String, target: String): String =
        if (line.startsWith(target)) line
        else EMPTY

    /**
     * @param data       defaults field determine behavior of method
     * @param streamData list of data from uploaded file
     * @return if defaults true list with uploaded filename and
     * information about what hormone pattern
     * have been used in examination the samples,
     * if false empty ArrayList
     * @see calculator.constants.CalculatorConstants.DEFAULT_TARGET_POINT
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun appendFileInfo(streamData: String, data: ExaminationFile): String =
        when {
            streamData.contains(PATTERN_PART) -> PATTERN_PART
            streamData.contains(FILENAME_PART) -> FILENAME_PART
            else -> streamData
        }.let {
            create(streamData, data, it)
        }

    private fun create(streamData: String, data: ExaminationFile, checker: String): String =
        when (checker) {
            PATTERN_PART -> getPatternFromData(streamData)
            FILENAME_PART -> getFileName(streamData)
            else -> isDataResult(streamData, data.target)
        }

    /**
     * @param pattern list of lines from the upload file;
     * @return the fifth line of streamData if starts from [PATTERN_PART]
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun getPatternFromData(pattern: String): String = pattern.let {
        if (it.startsWith(PATTERN_PART)) {
            log.warn("Not default pattern: $it")
            it.replace(PATTERN_PART, EMPTY)
        } else it
    }

    /**
     * @param startPath list of lines from the upload file;
     * @return the first line of streamData if starts from [PATTERN_PART]
     *
     * @since 15.06.2020
     * @author emil.wozniak.591986@gmail.com
     */
    private fun getFileName(startPath: String): String = startPath.let {
         if (it.startsWith(FILENAME_PART)) {
             log.warn("Not default: $it")
             it.replace(FILENAME_PART, EMPTY)
         } else it
     }
}
