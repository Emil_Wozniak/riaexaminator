package calculator

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Executor {
    val log : Logger = LoggerFactory.getLogger(Executor::class.java.name)
    fun execute() {
        log.info("Start :calculator module component")
    }
}
